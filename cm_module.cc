
#include "cm_module.hh"


reach_module *controllable_module::single_reach(int type)
{
  if (_controller)
    {
      _module->_loc.print_lineno(stderr);
      fprintf (stderr,"Module expected direct processor, but controller in the middle.\n");
      return NULL;
    }
  else if (!_processor)
    {
      _module->_loc.print_lineno(stderr);
      fprintf (stderr,"Module expected direct processor, but has no processor (at all).\n");
      return NULL;
    }
  else if (_reach._type != type)
    {
      _module->_loc.print_lineno(stderr);
      fprintf (stderr,"Module expect direct processor, with wrong-type reach list.\n");
      return NULL;
    }
  else
    return &_reach;
}

void reach_module::reach_fprintf(FILE* out)
{
  switch (_type)
    {
    case RM_CAMAC:
      fprintf(out,"CAMACc%ds%d",_access._camac._crate,_access._camac._slot);
      break;
    case RM_VME:
      fprintf(out,"VMEb0x%08x/*s%d*/",_access._vme._address,_access._vme._slot);
      break;
    case RM_FASTBUS:
      fprintf(out,"FASTBUSs%d",_access._fastbus._slot);
      break;
    case RM_MAINFRAME:
      fprintf(out,"MFRc%ds%d",_access._mfr._crate,_access._mfr._slot);
      break;
    case RM_SERIAL:
      fprintf(out,"SERIAL");
      break;
    default:
      fprintf(out,"SMOKE_SIGNALS");
      break;
    }
}

void controllable_module::backwards_reach_fprintf(FILE* out)
{
  // if we are behind a controllable, first print his way of getting
  // reached...

  if (_controller)
    {
      assert (!_processor);

      _controller->backwards_reach_fprintf(out);
      fprintf(out,"/");
    }
  else 
    {
      if (!_processor)
	{
	  _module->_loc.print_lineno(stderr);
	  fprintf (stderr,"Module has neither upstream controller or processor, cannot be reached.\n");
	  fprintf (out,"UNREACHABLE/NONE/");
	}
      else
	{
	  // Print the network name of the processor

	  const char *hostname = _processor->get_setting("HOSTNAME");

	  if (!hostname)
	    {
	      _processor->_loc.print_lineno(stderr);
	      fprintf (stderr,"No hostname for processor.\n");
	      _module->_loc.print_lineno(stderr);
	      fprintf (stderr,"This module is thus unreachable.");
	      hostname = "NOHOSTNAME";
	    }
	  
	  fprintf (out,"%s/UNKNOWN/",hostname);
  	}
    }
  
  // And then, tell how to reach this module

  _reach.reach_fprintf(out);
}

