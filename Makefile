GENDIR = gen

GENOBJS = parser.o lexer.o
OBJS    = ytcon.o rcsu.o cable_node.o hardware_def.o hardware_handler.o \
	hw_module.o daq_code.o enumerate.o trace.o str_set.o cm_module.o \
	util.o setup_param.o name_map.o

ALL_OBJS = $(addprefix $(GENDIR)/,$(GENOBJS) $(OBJS))

AUTO_DEPS = $(ALL_OBJS:%.o=%.d)

CXXLINK = $(CXX)

CXXFLAGS += -g -I. -pedantic-errors -Wall -Werror -Wextra

CXX_O=mkdir -p $(@D) && $(CXX) $(CXXFLAGS) $(OPTFLAGS) -o $@ -c $<
CXX_D=mkdir -p $(@D) && $(CXX) $(CXXFLAGS) -MM -MG $< | \
      sed -e 's,\($(*F)\)\.o[ :]*,$*.o $@ : ,g' > $@
LEX_CC=mkdir -p $(@D) && \
       $(LEX) -8 -t $< > $@ && \
       sed -i 's/yyl < yyleng/(size_t)yyl < (size_t)yyleng/;s/i < _yybytes_len/(size_t)i < (size_t)_yybytes_len/' $@
LINK_EXE=$(CXXLINK) $(CXXLINKFLAGS) $(ALL_OBJS) $(CXXLIBS) -o $@
YACC_CC=mkdir -p $(@D) && $(YACC) -d -o $@ $<
ifeq (1,$(V))
	CXX_O_V=$(CXX_O)
	CXX_D_V=$(CXX_D)
	LEX_V=$(LEX_CC)
	LINK_V=$(LINK_EXE)
	YACC_V=$(YACC_CC)
else
	CXX_O_V=@echo "  CC     $@" && $(CXX_O)
	CXX_D_V=@echo "  DEPS   $@" && $(CXX_D)
	LEX_V=@echo "  LEX    $@" && $(LEX_CC)
	LINK_V=@echo "  LINK   $@" && $(LINK_EXE)
	YACC_V=@echo "  YACC   $@" && $(YACC_CC)
endif

all: ytcon

clean:
	rm -rf $(GENDIR)
	rm -f ytcon

$(GENDIR)/y.tab.c: parser.y
	$(YACC_V)
	@touch $(GENDIR)/y.tab.h

$(GENDIR)/y.tab.h: $(GENDIR)/y.tab.c

$(GENDIR)/parser.cc: $(GENDIR)/y.tab.c
	@echo "   CP    $@"
	@cp $< $@

$(GENDIR)/parser.hh: $(GENDIR)/y.tab.h
	@echo "   CP    $@"
	@cp $< $@

$(GENDIR)/lexer.cc: lexer.lex
	$(LEX_V)

$(GENDIR)/lexer.o: $(GENDIR)/parser.hh

%.o: %.cc %.d
	$(CXX_O_V)

$(GENDIR)/%.o: %.cc $(GENDIR)/%.d
	$(CXX_O_V)

%.d: %.cc
	$(CXX_D_V)

$(GENDIR)/%.d: %.cc
	$(CXX_D_V)

ytcon: $(ALL_OBJS)
	$(LINK_V)

ifneq (clean,$(MAKECMDGOALS))
-include $(AUTO_DEPS)
endif
