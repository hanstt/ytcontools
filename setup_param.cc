
#include "setup_param.hh"

#include "daq_code.hh"
#include "enumerate.hh"
#include "hw_module.hh"
#include "trace.hh"
#include "str_set.hh"
#include "util.hh"

#include <stdlib.h>

#define RS_RANGE  1

struct range_spec
{
  int   _type;
  char *_unit;

  struct
  {
    double _min;
    double _max;

    char *_min_str;
    char *_max_str;
    
  } _range;
};

bool parse_range_unit(const char *range,
		      range_spec *dest)
{
  // range is on the form <min><unit>-<max><unit>
  //
  // Where min and max are floating point values, and unit gives the unit

  memset(dest,0,sizeof(*dest));

  if (!range)
    return false;

  // dest->_type = 0;
  // dest->_unit = NULL;

  char *endptr;
  const char *dash;
  const char *p = range;

  // Get the min value
  dest->_range._min = strtod(p,&endptr);
  if (endptr == p)
    goto parse_error;
  dest->_range._min_str = strndup(p,endptr-p);
  p = endptr;
  // Find the end of the unit
  dash = strchr(p,'-');
  if (!dash)
    goto parse_error;
  dest->_unit = strndup(p,dash-p);
  p = dash+1;
  // Get the max value
  dest->_range._max = strtod(p,&endptr);
  if (endptr == p)
    goto parse_error;
  dest->_range._max_str = strndup(p,endptr-p);
  p = endptr;
  // Check that the unit is the same
  if (strcmp(dest->_unit,p) != 0)
    goto parse_error;
  // We're done...

  dest->_type = RS_RANGE;

  return true;
 parse_error:
  free(dest->_unit);
  free(dest->_range._min_str);
  free(dest->_range._max_str);
  return false;
}


void generate_params(controllable_module *cm,const char *param)
{  
  /*
  fprintf (stderr,"Module: %s  Param: %s\n",
	   cm->_module->_location.to_string().c_str(),
	   param);
  */

  // The param string has the form
  //
  // <name>:<channels>,range
  //
  // where channels may be on the form in1_8 or so, specifying that
  // it actually is many parameters, each applying to one signal
  //
  // range is on the form <min><unit>-<max><unit>
  //
  // Where min and max are floating point values, and unit gives the unit

  const char *colon = strchr(param,':');
  const char *comma = strchr(param,',');
  char *name = NULL;
  char *conn = NULL;
  char *range = NULL;
  
  std::vector<std::string> *signals = NULL;

  if (comma)
    {
      range = strdup(comma+1);
    }
  else
    comma = param+strlen(param); // point to end of channel specification

  if (colon)
    {
      name = strndup(param,colon-param);

      conn = strndup(colon+1,comma-(colon+1));

      signals = expand_string_one(conn);
    }

  range_spec model_range;
  memset(&model_range,0,sizeof(model_range));

  if (!parse_range_unit(range,
			&model_range))
    {
      cm->_module->_model->_loc.print_lineno(stderr);
      fprintf (stderr,"Model range parse error: %s in %s\n",range,param);
    }

  const char *first_channel = 
    cm->_module->_model->get_control("FIRST_CHANNEL");

  std::vector<std::string>::const_iterator si;

  for (si = signals->begin(); si != signals->end(); ++si)
    {
      /*
      fprintf (stderr,"Module: %s/%s  Param: %s  Range: %s\n",
	       cm->_module->_location.to_string().c_str(),
	       si->c_str(),
	       name,
	       range);
      */

      // parameter_type '(' channel_ids ',' hw_id ',' unit ',' range comment setup_modifiers ')' ';' { }

      int channel = get_channel_index(cm->_module,NULL,si->c_str(),first_channel);

      // find the signal(s) for this channel...

      connection_parts_vector *channel_ids = cm->_signals->get_signals_for_channel(channel);

      fprintf (stdout,"/*%s/%s*/ ",
	       cm->_module->_location.to_string().c_str(),
	       si->c_str());

      printf ("%s(",name); // parameter_type
      // channel_ids
      if (channel_ids->size())
	{
	  connection_parts_vector::iterator ci;

	  for (ci = channel_ids->begin(); ci != channel_ids->end(); ++ci)
	    printf (" %s",(*ci)->_label);
	}
      else
	printf (" UNCONNECTED");
      printf (", ");
      // hw_id
      // find out how to reach us...
      cm->backwards_reach_fprintf(stdout);
      // and which channel we are occupying

      if (channel != -1)
	printf ("/CHn%d",channel);
      else
	printf ("/*global*/");

      if (model_range._unit)
	printf (", %s",strlen(model_range._unit) == 0 ? "1" : model_range._unit);
      else
	printf (", -"); // no unit?

      switch (model_range._type)
	{
	case RS_RANGE:
	  printf (", RANGE(%s %s,%s %s)",
		  model_range._range._min_str,
		  model_range._unit,
		  model_range._range._max_str,
		  model_range._unit);
	  break;
	default:
	  printf (", BAD_RANGE/**/");
	  break;
	}
      printf (");\n");

      delete channel_ids;
    }  

  // parameter_type '(' channel_ids ',' hw_id ',' unit ',' range comment setup_modifiers ')' ';' { }
  // PATH '(' connectors_or_ch_id ',' connectors_or_ch_id ')' { }
  // MULTIPLEXER '(' hw_id ',' connectors ',' connectors ')' { }

  // std::vector<std::string>* expand_string(const char *label)
	      
  free(name);
  free(conn);
  free(range);
  delete signals;
  free(model_range._unit);
  free(model_range._range._min_str);
  free(model_range._range._max_str);
}

char *format_module_conn(hw_module *mod,const char *conn)
{
  return make_message("%s(%s)/%s",
		      mod->_location.to_string().c_str(),
		      mod->_model->_name,
		      conn);
}

char *format_module_conn(hw_module *mod,hw_connection *conn)
{
  return format_module_conn(mod,conn->_attr->_label);
}

void generate_multiplexer(controllable_module *cm,const char *mux)
{
  // A multiplexer specification looks like
  //
  // OUTPUTS=INPUTS,NAME

  // in order to find what outputs are connected to what inputs,
  // the usual handler functionality is used...
  //
  // I.e., the inputs are

  const char *equals = strchr(mux,'=');
  const char *comma = strchr(mux,',');

  char *name = NULL;
  char *outputs = NULL;
  char *inputs = NULL;

  if (!equals)
    {
      fprintf (stderr,"Multiplexer parse error, no '=' sign: %s\n",mux);
      return;
    }

  if (comma)
    name = strdup(comma+1);
  else
    comma = mux+strlen(mux); // point to end of channel specification

  outputs = strndup(mux,equals-mux);
  inputs = strndup(equals+1,comma-(equals+1));

  std::vector<int> *outlevels = new std::vector<int>;
  std::vector<int> *inlevels = new std::vector<int>;

  std::vector<std::string> *outsignals = NULL;
  std::vector<std::string> *insignals = NULL;

  outsignals = expand_string_one(outputs,false,outlevels);
  insignals = expand_string_one(inputs,false,inlevels);

  // fprintf (stderr,"Mux: %s -> %s  as  %s\n",outputs,inputs,name);

  // now, it is necessary that the input has one more level than the output,
  // and that, if there are more levels, all the first indices match

  if (outlevels->size()+1 != inlevels->size())
    {
      fprintf (stderr,"Multiplexer output levels + 1 != input levels, (%d,%d): %s\n",
	       (int)outlevels->size(),
	       (int)inlevels->size(),
	       mux);
      if (NULL != name) {
        free(name);
      }
      return;
    }

  int lastlevel = inlevels->back();

  // Now, when we have several output levels, we have several multiplexers, so first of
  // all loop over the outputs

  const char *first_channel = 
    cm->_module->_model->get_control("FIRST_CHANNEL");

  std::vector<std::string>::const_iterator osi;

  std::vector<std::string>::const_iterator isi;
  isi = insignals->begin();
      
  for (osi = outsignals->begin(); osi != outsignals->end(); ++osi)
    {
      /*
      fprintf (stderr,"Module: %s/%s  Name: %s\n",
	       cm->_module->_location.to_string().c_str(),
	       osi->c_str(),
	       name);
      */
      fprintf (stdout,"/*%s/%s*/ ",
	       cm->_module->_location.to_string().c_str(),
	       osi->c_str());

      int channel = get_channel_index(cm->_module,NULL,
				      osi->c_str(),first_channel,true);

      printf ("MULTIPLEXER(");

      cm->backwards_reach_fprintf(stdout);

      if (channel != -1)
	printf ("/CHn%d",channel);
      else
	printf ("/*global*/");

      char *out = format_module_conn(cm->_module,osi->c_str());
      printf (", \"%s\",",out);
      free(out);

      // Then loop over out inputs

      for (int i = 0; i < lastlevel; i++, ++isi)
	{
	  if (isi == insignals->end())
	    {
	      fprintf (stderr,"Multiplexer ran out of input signals (expand level mismatch): %s\n",mux);
	      break;
	    }
	  char *in = format_module_conn(cm->_module,isi->c_str());
	  printf (" \"%s\"",in);
	  free(in);
	}

      printf (");\n");
    }

  if (isi != insignals->end())
    {
      fprintf (stderr,"Multiplexer has left-over input signals (expand level mismatch): %s\n",mux);
    }
  if (NULL != name) {
    free(name);
  }
}

int strcmp_num(const char *s1, const char *s2)
{
  // This routine compare two strings, essentially as
  // strcmp would do it.  But, if the difference occurs while
  // digits are being compared, their values are compared, such
  // that we get a sequence 1,2,3,10,11,23 and not 1,10,11,2,23,3

  const char *p1 = s1;
  const char *p2 = s2;

  while (*p1 == *p2 && *p1)
    {
      p1++;
      p2++;
    }

  // We've now reached a point where the two strings diverge
  
  // First, if we had digits before us, back up

  // But, it makes no sense to back up if what we currently
  // have both are non-digit, since then the digits compared
  // equal

  if (isdigit(*p1) || isdigit(*p2))
    {
      while (p1 > s1 && isdigit(*(p1-1)))
	{
	  p1--;
	  p2--;
	}

      // Now, if only one of them has a digit, we compare as normal
      // strings (with digits being part of the ascii table), but if both
      // are numbered, we'll interpret the numbers

      if (isdigit(*p1) && isdigit(*p2))
	{
	  // Now, actually, if there are very many digits, then we
	  // have a problem, since we wont convert correctly...
	  // This is solved by realizing that sort order is only
	  // different from strcmp if the numbers have different length
	  // so simply count the length.  If different, sort on that, 
	  // otherwise, revert to strcmp (fall-through)

	  // Which is not true, if the numbers begin with zeros...
	  // But this I do not care about here...  (solving that,
	  // would possibly make the comparison return equal even when
	  // non-equal, since we then have to basically ignore those
	  // zeros...)

	  const char *e1 = p1+1;
	  const char *e2 = p2+1;

	  while (isdigit(*e1))
	    e1++;
	  while (isdigit(*e2))
	    e2++;

	  int diff = (e1-p1) - (e2-p2);

	  if (diff)
	    return diff;
	}      
    }
  
  return strcmp(p1,p2);  
}

struct signal_path
{
public:
  signal_path(char *ins,char *in,char *outs,char *out)
  {
    _insignal = ins;
    _outsignal = outs;
    _input = in;
    _output = out;
  }

public:
  char *_insignal;
  char *_outsignal;
  char *_input;
  char *_output;

public:
  int input_compare(const signal_path &rhs) const
  {
    int cmp;

    if (_insignal || rhs._insignal)
      {
	if (_insignal && rhs._insignal)
	  cmp = strcmp_num(_insignal,rhs._insignal);
	else
	  {
	    if (_insignal)
	      return -1;
	    else
	      return 1;
	  }
      }
    else
      cmp = strcmp_num(_input,rhs._input);

    return (cmp);
  }

public:
  bool operator<(const signal_path &rhs) const
  {
    int cmp;

    if (_insignal || rhs._insignal)
      {
	if (_insignal && rhs._insignal)
	  cmp = strcmp_num(_insignal,rhs._insignal);
	else
	  {
	    if (_insignal)
	      return 1;
	    else
	      return 0;
	  }
      }
    else
      cmp = strcmp_num(_input,rhs._input);

    if (cmp)
      return cmp < 0;

    if (_outsignal || rhs._outsignal)
      {
	if (_outsignal && rhs._outsignal)
	  cmp = strcmp_num(_outsignal,rhs._outsignal);
	else
	  {
	    if (_outsignal)
	      return 1;
	    else
	      return 0;
	  }
      }
    else
      cmp = strcmp_num(_output,rhs._output);

    return cmp < 0;
  }
};

struct signal_path_compare
{
  bool operator()(const signal_path &p1, 
		  const signal_path &p2) const
  {
    return (p1 < p2);
  }
};


typedef std::set<signal_path,signal_path_compare> signal_path_set;

signal_path_set signal_paths;

void add_signal_path(const char *in_signal,
		     hw_module *in_mod,hw_connection *in_conn,
		     const char *out_signal,
		     hw_module *out_mod,hw_connection *out_conn)
{
  char *input = NULL;
  char *output = NULL;
  char *ins = NULL;
  char *outs = NULL;

  if (in_signal)
    ins = strdup(in_signal);
  else
    input = format_module_conn(in_mod,in_conn);
    /*
    input = make_message("%s(%s)/%s",
			 in_mod->_location.to_string().c_str(),
			 in_mod->_model->_name,
			 in_conn->_attr->_label);
    */
  if (out_signal)
    outs = strdup(out_signal);
  else
    output = format_module_conn(out_mod,out_conn);
  /*
    output = make_message("%s(%s)/%s",
			  out_mod->_location.to_string().c_str(),
			  out_mod->_model->_name,
			  out_conn->_attr->_label);;
  */

  signal_path path(ins,input,
		   outs,output);

  if (!signal_paths.insert(path).second)
    {
      // The thing already existed, not inserted.
      // kill the strings
      free(ins);
      free(outs);
      free(input);
      free(output);
    }
}

void dump_signal_paths()
{
  // typedef std::set<signal_path,signal_path_compare> signal_path_set;
  // signal_path_set signal_paths;

  signal_path_set::iterator spi;
  signal_path_set::iterator spi_next;

  for (spi = signal_paths.begin(); spi != signal_paths.end(); )
    {
      printf ("SIGNAL_PATH(");
      if (spi->_insignal)
	printf ("%s, ",spi->_insignal);
      else
	printf ("\"%s\", ",spi->_input);
      
      // Try to merge inputs

      spi_next = spi;
      ++spi_next;

      if (spi->_outsignal)
	printf ("%s);\n",spi->_outsignal);
      else
	{
	  while (spi_next != signal_paths.end() &&
		 spi->input_compare(*spi_next) == 0)
	    {
	      printf ("\"%s\" ",spi->_output);
	      spi = spi_next;
	      ++spi_next;
	    }
	  
	  printf ("\"%s\");\n",spi->_output);
	}
      spi = spi_next;
    }
}

void add_signal_paths(connection_parts *parts)
{
  connection_part_vect::iterator pi;
  connection_part_vect::iterator pi_prev = parts->_trace.end();
  /*
  fprintf(stderr,"%d : ",parts->_forward);
  for (pi = parts->_trace.begin(); pi != parts->_trace.end(); ++pi)
    {
      fprintf (stderr," %s/%s(%s)/%s ",
	       pi->_conn[0] ? pi->_conn[0]->_attr->_label : "--",
	       pi->_module->_location.to_string().c_str(),
	       pi->_module->_model->_name,
	       pi->_conn[1] ? pi->_conn[1]->_attr->_label : "--");
    }
  fprintf(stderr,"\n");
  */
  for (pi = parts->_trace.begin(); pi != parts->_trace.end(); ++pi)
    {
      /*
      fprintf (stderr," %s/%s(%s)/%s ",
	       pi->_conn[0] ? pi->_conn[0]->_attr->_label : "--",
	       pi->_module->_location.to_string().c_str(),
	       pi->_module->_model->_name,
	       pi->_conn[1] ? pi->_conn[1]->_attr->_label : "--");
      */

      if (pi_prev != parts->_trace.end())
	{
	  // Connection from last module
	  // if (pi->)

	  if (pi_prev->_conn[1] && pi->_conn[0])
	    {
	      if (parts->_forward)
		add_signal_path(NULL,
				pi_prev->_module,pi_prev->_conn[1],
				NULL,
				pi->_module,pi->_conn[0]);
	      else
		add_signal_path(NULL,
				pi->_module,pi->_conn[0],
				NULL,
				pi_prev->_module,pi_prev->_conn[1]);
	    }
	}
      // Connection within this module
      
      if (pi->_conn[1] && pi->_conn[0])
	{
	  if (parts->_forward)
	    add_signal_path(NULL,
			    pi->_module,pi->_conn[0],
			    NULL,
			    pi->_module,pi->_conn[1]);
	  else
	    add_signal_path(NULL,
			    pi->_module,pi->_conn[1],
			    NULL,
			    pi->_module,pi->_conn[0]);
	}
      
      pi_prev = pi;
    }

  if (pi_prev != parts->_trace.end())
    {
      // Connection from last module
      // if (pi->)
      
      if (pi_prev->_conn[0])
	{
	  if (parts->_forward)
	    add_signal_path(NULL,
			    pi_prev->_module,pi_prev->_conn[0],
			    parts->_label,
			    NULL,NULL);
	  else
	    {
	      // The special signal MULTIPLEXED is not added to the lists!

	      if (strcmp(parts->_label,"MULTIPLEXED") != 0)
		add_signal_path(parts->_label,
				NULL,NULL,
				NULL,
				pi_prev->_module,pi_prev->_conn[0]);
	    }
	}
    }
 
}

void generate_setup_table()
{
  // Generate a listing of all setup parameters, on the form
  //
  // param(signal_ids,hw_id,unit,range,"comment",modifiers);
  //
  // An empty setup parameter would look like:
  //
  // param(signal_ids,?(value) unit,time,hw_id,"comment",modifiers);

  char marker[64];
  char comment[64];

  sprintf (marker,"SETUP_DEFINITION");
  sprintf (comment,"Land slow control setup listing");

  print_header(marker,comment);
  
  printf ("\n");

  // We'll run though all the modules, looking for parameters that can
  // be set.  For each module (which is the controllable item), we
  // need to figure out what channels are connected.  And for the
  // module, we need to figure out how to reach it, i.e. we need to
  // find the path to the processor

  // For an item to be added as something which can be controlled we
  // also like to find the corresponding signal name (or we'll eject
  // the word UNCONNECTED)

  for (cm_vector::iterator i = cm_modules.begin();
       i != cm_modules.end(); ++i)
    {
      controllable_module *cm = *i;

      // Get the control parameters

      std::vector<const char*> *params = cm->_module->_model->get_controls("SETUP_PARAMETER");

      if (params)
	{
	  // We have a set of parameters

	  std::vector<const char*>::const_iterator pi;

	  for (pi = params->begin(); pi != params->end(); ++pi)
	    {
	      // Now, for every parameter, find out a) which connector(s) it applies to,
	      // and if it applies to a connector, find out what signal is connected there!

	      generate_params(cm,*pi);
	    }

	  delete params;
	}

      // Find all signals, and remember their paths

      connection_parts_vector::iterator si;

      for (si = cm->_signals->_signals.begin(); si != cm->_signals->_signals.end(); ++si)
	{
	  // fprintf (stderr,"SIGNAL: %s\n",(*si)->_label);

	  add_signal_paths(*si);
	  //fprintf (stderr,"\n");
	}

      // Find any multiplexers

      std::vector<const char*> *muxs = cm->_module->_model->get_controls("MULTIPLEXER");

      if (muxs)
	{
	  // We have a set of parameters

	  std::vector<const char*>::const_iterator mi;

	  for (mi = muxs->begin(); mi != muxs->end(); ++mi)
	    {
	      generate_multiplexer(cm,*mi);
	      //fprintf (stderr,"Mux: %s\n",*mi);
	    }

	  delete muxs;
	}
    }  

  printf ("\n");

  dump_signal_paths();

  printf ("\n");

  print_footer(marker);
}


