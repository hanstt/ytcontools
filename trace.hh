
#ifndef __TRACE_H__
#define __TRACE_H__

#include "hw_module.hh"
#include "cm_module.hh"

#include <vector>

struct connection_part
{
public:
  connection_part()
  {
    _conn[0] = NULL;
    _conn[1] = NULL;
    _module  = NULL;
  }

  connection_part(hw_module *module)
  {
    _conn[0] = NULL;
    _conn[1] = NULL;
    _module  = module;
  }

  connection_part(hw_module *module,hw_connection *end)
  {
    _conn[0] = NULL;
    _conn[1] = end;
    _module  = module;
  }

  connection_part(hw_connection *start,hw_module *module)
  {
    _conn[0] = start;
    _conn[1] = NULL;
    _module  = module;
  }

  connection_part(hw_connection *start,hw_module *module,hw_connection *end)
  {
    _conn[0] = start;
    _conn[1] = end;
    _module  = module;
  }


public:
  hw_connection *_conn[2];
  hw_module     *_module;

  // Order is _conn[0] -> _module -> _conn[1]
  // (if we traced backwards, 0 will be module output, and 1 the input,
  // if we traced forward (as signals travel), 0 will be an input, and 1 an output)
};

typedef std::vector<connection_part> connection_part_vect;
typedef std::set<hw_connection*> connection_set;

struct connection_parts
{
public:
  connection_parts(int forward)
  {
    _channel = -1;
    _label = NULL;
    _forward = forward;
  }
  
public:
  connection_part_vect _trace;
  connection_set       _connectors; // to detect loops

public:
  int         _forward;
  int         _channel; // Zero-based index of signal in module (-1 == invalid)
  const char *_label;   // Name of physical channel

public:
  bool has_continuation(hw_connection *conn)
  {
    connection_set::iterator iter;
    iter = _connectors.find(conn);
    return iter != _connectors.end();
  }
  void insert_continuation(hw_connection *conn)
  {
    _connectors.insert(conn);
  }

public:
  void push_back(const connection_part &part) { _trace.push_back(part); }
  connection_part &back() { return _trace.back(); }

public:
  void print();

};

typedef std::vector<connection_parts*> connection_parts_vector;

struct physical_signals
{

public:
  connection_parts_vector _signals;

public:
  int num_signals();
  int one_after_last();

public:
  void insert(const connection_parts_vector* src)
  {
    _signals.insert(_signals.end(),
		    src->begin(),src->end());
  }

  connection_parts_vector *get_signals_for_channel(int channel);
};

// For connection_parts, the first [0] will be empty, and the last [1] as well!
// Since we always begin end at modules, and not in a dangling cable.

void trace_signal(connection_parts_vector *traces,
		  connection_parts *trace,
		  int info,
		  bool backwards);

void find_signals(controllable_module *cm_module);

int get_channel_index(const hw_module *module,
		      const hw_connection *conn,
		      const char *name,
		      const char *first_channel,
		      bool allow_failure = false);

#endif// __TRACE_H__


