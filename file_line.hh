
#ifndef __FILE_LINE_H__
#define __FILE_LINE_H__

#include <stdio.h>

void print_lineno(FILE* fid,int internal,bool end_colon = true);

class file_line
{
public:
  file_line(int internal)
  {
    _internal = internal;
  }

  file_line()
  {
    _internal = 0;
  }

public:
  void print_lineno(FILE* fid,bool end_colon = true) const
  {
    ::print_lineno(fid,_internal,end_colon);
  }

public:
  int _internal;
};

#endif//__FILE_LINE_H__
