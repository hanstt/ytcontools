/* This file contains the lexical analyser for
 * cabling documentation files.
 *
 * With the help of flex(lex), a C lexer is produced.
 *
 * The perser.y contain the grammar which will read
 * the tokens that we produce.
 *
 * Don't be afraid.  Just look below and you will figure
 * out what is going on.
 */

%{
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "cable_node.hh"
#include "str_set.hh"
#include "file_line.hh"

struct md_ident_fl;

#include "gen/parser.hh"

struct lineno_map
{
  int   _internal;

  int   _line;
  char* _file;

  lineno_map *_prev;
};

lineno_map *_last_lineno_map = NULL;

void yyerror(const char *);

ssize_t lexer_read(char* buf,size_t max_size);

#define YY_INPUT(buf,result,max_size) \
{ \
  result = lexer_read(buf,max_size); \
  if (!result) result = YY_NULL; \
}

%}

%option nounput
%option yylineno

%%

[0-9]+      {
                yylval.iValue = atoi(yytext);
                return INTEGER;
            }

((r|rack)([0-9]+|[_A-Z][_A-Z0-9]*)|d([_A-Z][_A-Z0-9]*))(c[0-9]+(s[0-9]+(u[0-9]+)?)?)? {
		yylval.rcsuValue = create_rcsu(yytext);
		return RCSU;
            }

((r|rack)([0-9]+|[_A-Z][_A-Z0-9]*)|d([_A-Z][_A-Z0-9]*))c[0-9]+(s[0-9]+(u[0-9]+)?)?_[0-9]+ {
		yylval.rcsuRangeValue = create_rcsu_range(yytext);
		return RCSU_RANGE;
            }

"."(c[0-9]+)?s[0-9]+(u[0-9]+)? {
		yylval.rcsuValue = create_partial_rcsu(yytext);
		return PARTIAL_RCSU;
            }

"."c[0-9]+    {
		yylval.rcsuValue = create_partial_rcsu(yytext);
		return PARTIAL_RCSU;
            }

"."u[0-9]+    {
		yylval.rcsuValue = create_partial_rcsu(yytext);
		return PARTIAL_RCSU;
            }

"."[csu]      {
		yylval.rcsuValue = create_partial_rcsu_same(yytext);
		return PARTIAL_RCSU;
            }

"."(c[0-9]+)?s[0-9]+(u[0-9]+)?_[0-9]+ {
		yylval.rcsuRangeValue = create_partial_rcsu_range(yytext);
		return PARTIAL_RCSU_RANGE;
            }

[Nn][Aa][Nn]|[Ii][Nn][Ff] {
		yylval.fValue = atof(yytext);
                return DOUBLE;
            }
                

[+-]?[0-9]+"."[0-9]*([eE][+-]?[0-9]+)? { 
		yylval.fValue = atof(yytext);
                return DOUBLE;
            }

"RACK"      { return RACK; }
"DET"       { return DET; }
"CRATE"     { return CRATE; }
"MODULE"    { return MODULE; }
"UNIT"      { return UNIT; }
"DEVICE"    { return DEVICE; }
"SETTING"   { return SETTING; }
"HAS_SETTING" { return HAS_SETTING; }
"ECL_INPUT"  { return ECL_INPUT; }
"ECL_OUTPUT" { return ECL_OUTPUT; }
"LEMO_INPUT"  { return LEMO_INPUT; }
"LEMO_OUTPUT" { return LEMO_OUTPUT; }
"LEMO8_INPUT"  { return LEMO_INPUT; } // TODO, handle LEMO8 separately?
"LEMO8_OUTPUT" { return LEMO_OUTPUT; }
"LEMO12_INPUT"  { return LEMO_INPUT; } // TODO, handle LEMO12 separately?
"LEMO12_OUTPUT" { return LEMO_OUTPUT; }
"LVDS_INPUT"  { return ECL_INPUT; } // TODO, handle LVDS separately?
"LVDS_OUTPUT" { return ECL_OUTPUT; }
"SMA_INPUT"  { return SMA_INPUT; }
"SMA_OUTPUT" { return SMA_OUTPUT; }
"BNC_INPUT"  { return LEMO_INPUT; } // TODO, handle BNC separately?
"BNC_OUTPUT" { return LEMO_OUTPUT; }
"BNC_CONNECTOR" { return CONNECTOR; }
"CONNECTOR"  { return CONNECTOR; } // Bidirectional connector, like a bus
"SERIAL_CON" { return CONNECTOR; } // TODO: handle serial separately
"SIGNAL"    { return SIGNAL; }
"TENSION"   { return TENSION; }
"BROKEN"    { return BROKEN; }
"LABEL"     { return LABEL; }
"NAME_MAP"  { return NAME_MAP; }
"NAME"      { return NAME; }
"CONTROL"   { return CONTROL; }
"SERIAL"    { return SERIAL; }
"MODEL_AS"  { return MODEL_AS; }
"HANDLER"   { return HANDLER; }
"TITLE"     { return TITLE; }
"LT_RANGE"  { return LT_RANGE; }
"=>"        { return SETTING_TO; }
"->"        { return CABLE_TO; }
"<-"        { return CABLE_FROM; }

[_a-zA-Z][_a-zA-Z0-9]* { 
                yylval.strValue = find_str_identifiers(yytext);
                return IDENTIFIER;
            }

[-+*/;(){},:\|] {
                return *yytext;
            }

\"[^\"\n]*\" { /* Cannot handle \" inside strings. */
                yylval.strValue = find_str_strings(yytext+1,strlen(yytext)-2);
                return STRING;
            }

[ \t]+      { /*yylloc.last_column += strlen(yytext);*/ }       /* ignore whitespace */

[\n]+       { /*yylloc.last_line += strlen(yytext); yylloc.last_column = 0;*/ }       /* ignore whitespace */
.           { char str[64]; sprintf (str,"Unknown character: '%s'.",yytext); yyerror(str); }

"# "[0-9]+" \"".+"\""[ 0-9]*\n {  /* Information about the source location. */
              char file[1024] = "\0";
	      int line = 0;
	      char *endptr;
	      char *endfile;

	      /*yylloc.last_line++;*/

	      line = strtol(yytext+2,&endptr,10);

	      endfile = strchr(endptr+2,'\"');
	      if (endfile)
		strncpy(file,endptr+2,endfile-(endptr+2));
	      else
		strcpy(file,"UNKNOWN");

	      // fprintf(stderr,"Now at %s:%d (%d)\n",file,line,yylineno);

	      lineno_map *map = new lineno_map;

	      map->_internal = yylineno;
	      map->_line = line;
	      map->_file = strdup(file);
	      map->_prev = _last_lineno_map;

	      _last_lineno_map = map;
	    }

%%

void print_lineno(FILE* fid,int internal,bool end_colon)
{
  lineno_map *map = _last_lineno_map;

  if (!map)
    {
      fprintf (fid,"BEFORE_START:0:");
      return;
    }

  while (internal < map->_internal)
    {
      map = map->_prev;
      if (!map)
	{
	  fprintf (fid,"LINE_NO_TOO_EARLY(%d):0:",internal);
	  return;
	}
    }

  fprintf(fid,"%s:%d%s",
	  map->_file,map->_line + internal - map->_internal,
	  end_colon ? ":" : "");
}

int yywrap(void) {
    return 1;
}

int lexer_read_fd = -1;

ssize_t lexer_read(char* buf,size_t max_size)
{
  ssize_t n;

  for ( ; ; )
    {
      n = read(lexer_read_fd,buf,max_size);

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue;
	  fprintf(stderr,"Failure reading from lexer pipe\n");
	}
      return n;
    }
}
