#!/bin/sh

echo "Searching for $1"

FILE=`find modules/ -type f | egrep "\.hh$" | xargs egrep -l "(CRATE|MODULE|UNIT).*$1"`

HITS=`echo $FILE | wc -w`

if (($HITS == 0)); then
    echo "No such module: $1"; 
    exit 1; 
fi
if (($HITS >  1)); then 
    echo "Several modules: $1:"; 
    find modules/ -type f | egrep "\.hh$" | xargs egrep "(CRATE|MODULE|UNIT).*$1"; 
    exit 1; 
fi

less +/$1 $FILE 
