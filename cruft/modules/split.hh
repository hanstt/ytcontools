

MODULE(SPLIT_BOX)
{
  LABEL("Split box.");

  // TODO:  this needs special class for tracing signals, since they
  // are merged in disorder. 

  // out:   in
  //
  // 1      1
  // 2        9
  // 3      2
  // 4        10
  // 5      3
  // 6        11
  // 7      4
  // 8        12
  // 9      5
  // 10       13
  // 11     6
  // 12       14
  // 13     7
  // 14       15
  // 15     8
  // 16       16

  HANDLER("SPLIT_BOX","in=t,e");

  LEMO8_INPUT(in1_8);
  LEMO8_INPUT(in9_16);

  LEMO8_OUTPUT(t1_8);
  LEMO8_OUTPUT(t9_16);

  LEMO8_OUTPUT(e1_8);
  LEMO8_OUTPUT(e9_16);
}

UNIT(PRESPLIT_CROSS)
{
  // This module has two 8-fold inputs, and two 8-fold outputs.
  
  // The first four cables in the two inputs are intermixed routed to
  // the first 8-fold output, while the last four are routed to the last
  // 8-fold output.

  // The idea is such that when this is put before a split card, the
  // input in one 8-fold cable will appear at the output of one 8-fold
  // cable.

  LEMO8_INPUT(in1_8);
  LEMO8_INPUT(in9_16);

  LEMO8_OUTPUT(out1_8);
  LEMO8_OUTPUT(out9_16);

  HANDLER("SPLIT_BOX","out=in"); // it acts as an inverse split box

  //HANDLER("MULTI_IN_OUT","in1=out1:in2=out9:in3=out2:in4=out10:in5=out3:in6=out11:in7=out4:in8=out12:in9=out5:in10=out13:in11=out6:in12=out14:in13=out7:in14=out15:in15=out8:in16=out16");
}

