


MODULE(CO4001)
{
  LABEL("QUAD COINC");
}

UNIT(CO4001_U)
{

  HAS_SETTING("A" => "AND" , "OR");
  HAS_SETTING("B" => "AND" , "OR");
  HAS_SETTING("C" => "AND" , "OR");
  HAS_SETTING("OUT" => "AND" , "OR");

  LEMO_INPUT(inA_C);
  LEMO_OUTPUT(out1_3);
  LEMO_OUTPUT(notout1_2);
  LEMO_OUTPUT(overlap);
}


MODULE(CO4000)
{
  LABEL("QUAD COINC");
}

UNIT(CO4000_U)
{

  HAS_SETTING("A" => "AND" , "OR");
  HAS_SETTING("B" => "AND" , "OR");
  HAS_SETTING("C" => "AND" , "OR");
  HAS_SETTING("OUT" => "AND" , "OR");

  LEMO_INPUT(inA_C);
  LEMO_OUTPUT(out1_3);
  LEMO_OUTPUT(not_out);
  LEMO_OUTPUT(overlap);
}

MODULE(PS756)
{
  LABEL("QUAD FOUR-FOLD LOGIC UNIT");
}

UNIT(PS756_U)
{
  HAS_SETTING("LEVEL" => "1","2","3","4");

  LEMO_INPUT(inA);
  LEMO_INPUT(inB);
  LEMO_INPUT(inC);
  LEMO_INPUT(inD);

  LEMO_INPUT(veto);

  LEMO_OUTPUT(out1);
  LEMO_OUTPUT(out2);
  LEMO_OUTPUT(out3);
  LEMO_OUTPUT(out4);
  LEMO_OUTPUT(out_not);

}


MODULE(CO1600)
{
  LABEL("16 channel 3 input Overlap Coincidence");

  HAS_SETTING("A" => "AND" , "OR");
  HAS_SETTING("B" => "AND" , "OR");
  HAS_SETTING("C" => "AND" , "OR");
  HAS_SETTING("OUT" => "AND" , "OR");

  ECL_INPUT(inA1_16);
  ECL_INPUT(inB1_16);
  ECL_INPUT(inC1_16);

  ECL_OUTPUT(out1a_16c);
}



MODULE(LECROY465)
{
  LABEL("Coincidence unit");
}

UNIT(LECROY465_U)
{
  HAS_SETTING("A" => "on", "off");
  HAS_SETTING("B" => "on", "off");
  HAS_SETTING("C" => "on", "off");
  HAS_SETTING("D" => "on", "off");

  HAS_SETTING("wdith" => "%fns");

  LEMO_INPUT(inA_D);
  LEMO_INPUT(veto);

  LEMO_OUTPUT(linear_out1_2);
  LEMO_OUTPUT(out1_2);
  LEMO_OUTPUT(not_out);
 
}
