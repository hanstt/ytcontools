
#include "fan_in_fan_out.hh"
#include "nim_ecl_nim.hh"


#include "amplifier.hh"
#include "clock.hh"
#include "delay.hh"
#include "gate.hh"
#include "logic.hh"
#include "constant_fraction.hh"
#include "hv.hh"
#include "misc.hh"


CRATE(NIM_CRATE)
{
  LABEL("NIM crate");
}


MODULE(OCTAL_SPLIT)
{
  LABEL("Octal Split");

  HANDLER("MULTI_IN_OUT","in=outa,outb");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(outa1_8);
  LEMO_OUTPUT(outb1_8);
}

UNIT(OCTAL_SPLIT_U)
{
  LEMO_INPUT(in);

  LEMO_OUTPUT(out1_2);
}

MODULE(PS710)
{
  LABEL("OCTAL DISCRIMINATOR");
}

UNIT(PS710_U)
{
  LEMO_INPUT(in);
  LEMO_OUTPUT(out1_3);
}

MODULE(LA8000)
{
  LABEL("LEVEL ADAPTER");
}

UNIT(LA8000_U)
{
  LEMO_INPUT(in_nim);
  LEMO_INPUT(in_ttl);

  LEMO_OUTPUT(out_nim);
  LEMO_OUTPUT(out_ttl);
}

MODULE(TO4000)
{
  LABEL("TTL OPTOCOUPLED INPUT ISOLATED");
}

UNIT(TO4000_U)
{
  BNC_INPUT(in);
  BNC_OUTPUT(out);
}



MODULE(TC590)
{
  LEMO_INPUT(in);
}
