

/* Nim-ecl-nim converter, 8 channels.
 */
MODULE(EC8000)
{
  HANDLER("MULTI_IN_OUT","in,in_ecl=out,out_ecl");

  LEMO_INPUT(in1_8);
  ECL_INPUT(in_ecl1_8);

  LEMO_OUTPUT(out1_8);
  ECL_OUTPUT(out_ecl1_8);
}

/* Nim-ecl-nim converter, 16 channels.
 */
MODULE(EC1610)
{
  HANDLER("MULTI_IN_OUT","in,in_ecl=out,out_ecl");

  LEMO_INPUT(in1_16);
  ECL_INPUT(in_ecl1_16);

  LEMO_OUTPUT(out1_16);
  ECL_OUTPUT(out_ecl1_16);
}

/* Nim-ecl-nim converter, 16 channels.
 */
MODULE(EC1601)
{
  HANDLER("MULTI_IN_OUT","in,in_ecl=out,out_ecl");

  LEMO_INPUT(in1_16);
  ECL_INPUT(in_ecl1_16);

  LEMO_OUTPUT(out1_16);
  ECL_OUTPUT(out_ecl1_16);
}


MODULE(CAEN92)
{
  LABEL("");
}

UNIT(CAEN92_U1)
{
  LEMO_INPUT(in1_8);
  ECL_OUTPUT(out1a_8b);
}

UNIT(CAEN92_U2)
{
  ECL_INPUT(in1_8);
  LEMO_OUTPUT(out1a_8b);
}




MODULE(N638)
{
//ECL NIM Translator, 8 channels, 2 Units
  LABEL("");
}


UNIT(N638_U)
{
  HANDLER("MULTI_IN_OUT","in,in_ecl=outA,outB,out_ecl");

  LEMO_INPUT(in0_7);
  ECL_INPUT(in_ecl0_7);
  LEMO_INPUT(common0_1);

  LEMO_OUTPUT(outA0_7);
  LEMO_OUTPUT(outB0_7);
  ECL_OUTPUT(out_ecl0_7);
}

