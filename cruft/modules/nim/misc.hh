/////////////////////////////////////////////////////////////////////

MODULE(RD2000)
{
  LABEL("Rate divider");
}

UNIT(RD2000_U)
{
  LEMO_INPUT(in);
  LEMO_INPUT(inhibit);

  HAS_SETTING("DOWNSCALE" => "%d");

  LEMO_OUTPUT(divn_1);              // 1/n
  LEMO_OUTPUT(divn_2);              // 1/n
  LEMO_OUTPUT(divn_3);              // 1/n
  LEMO_OUTPUT(not_divn);            // 1-1/n
  LEMO_OUTPUT(one_minusdivn_1);     // !(1-1/n)
  LEMO_OUTPUT(not_one_minusdivn_1); // !(1/n)
}

/////////////////////////////////////////////////////////////////////

MODULE(ORTEC462)
{
  HAS_SETTING("PERIOD" => "%fus");
  HAS_SETTING("RANGE"  => "%fus");

  BNC_OUTPUT(start);
  BNC_OUTPUT(stop);

  BNC_INPUT (dispersion_in);
  BNC_OUTPUT(dispersion_out);
}

/////////////////////////////////////////////////////////////////////

