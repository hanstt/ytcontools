/////////////////////////////////////////////////////////////////////

MODULE(AMP1003) // TODO: name
{
  LABEL("Quad amplifier");
}

UNIT(AMP1003_U)
{
  HAS_SETTING("POLARITY" => "POS","NEG");
  HAS_SETTING("GAIN" => "20","50","100","200","500","1000");

  BNC_INPUT(in);
  BNC_OUTPUT(out);
}

/////////////////////////////////////////////////////////////////////

MODULE(TC248)
{
  HAS_SETTING("COARSE_GAIN" => "10","20","50","100","200","500");
  HAS_SETTING("FINE_GAIN" => "%f");

  HAS_SETTING("POLARITY" => "POS","NEG");
  HAS_SETTING("PZBLR" => "P/Z","BLR");

  HAS_SETTING("TIMING_GAIN" => "5","10","25","50","100","250");
  
  BNC_INPUT(in);
  BNC_OUTPUT(out_uni);
  BNC_OUTPUT(out_bi);

  LEMO_OUTPUT(out_time);
}

/////////////////////////////////////////////////////////////////////

MODULE(PHILLIPS779)
{
  LABEL("32 CHANNEL AMPLIFIER");

  HAS_SETTING("AMPLIFICATION"=>"%f");
  
  HANDLER("MULTI_IN_OUT","in=out");

  LEMO_INPUT(in1_32);
  LEMO_OUTPUT(out1_32);
}

/////////////////////////////////////////////////////////////////////

MODULE(FA8000)
{
  LABEL("Fast amplifier");
}

UNIT(FA8000_U)
{
  HAS_SETTING("AMPLIFICATION"=>"%f");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out1_2);
}

/////////////////////////////////////////////////////////////////////


MODULE(FL8000)
{
  LABEL("Octal Fast Linear Amplifier");
}

UNIT(FL8000_U)
{
  HAS_SETTING("AMPLIFICATION"=>"%f");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out);
}

/////////////////////////////////////////////////////////////////////
