/* Definitions for fan-in-fan-out modules.
 */


/* General model for a fan-in-fan-out.
 */

#define UNIT_FAN_IN_FAN_OUT(Nin,Nout,Nnotout)          \
UNIT(FAN_IN_FAN_OUT_##Nin##_##Nout##_##Nnotout)   \
{                                                 \
  LEMO_INPUT(in1_##Nin);                          \
  LEMO_OUTPUT(out1_##Nout);                       \
  LEMO_OUTPUT(notout1_##Nnotout);                 \
}

UNIT_FAN_IN_FAN_OUT(4,4,2);
UNIT_FAN_IN_FAN_OUT(8,8,4);
UNIT_FAN_IN_FAN_OUT(16,16,8);

/* 4-fold unit of a fan-in-fan-out.
 */

MODULE(LF8000)
{
  LABEL("LF8000");


}

UNIT(LF8000_4_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_4);

  MODEL_AS(FAN_IN_FAN_OUT_4_4_2); // 4 in, 4 out, 2 !out
}

UNIT(LF8000_8_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_8);

  MODEL_AS(FAN_IN_FAN_OUT_8_8_4); // 4 in, 4 out, 2 !out
}

UNIT(LF8000_16_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_16);

  MODEL_AS(FAN_IN_FAN_OUT_16_16_8); // 4 in, 4 out, 2 !out
}

MODULE(LF4000)
{
  LABEL("LF8000");


}

UNIT(LF4000_4_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(not_out1_2);

  MODEL_AS(FAN_IN_FAN_OUT_4_4_2); // 4 in, 4 out, 2 !out
}

UNIT(LF4000_8_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
  LEMO_OUTPUT(not_out1_4);

  MODEL_AS(FAN_IN_FAN_OUT_8_8_4);
}

UNIT(LF4000_16_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_16);
  LEMO_OUTPUT(out1_18);
  LEMO_OUTPUT(not_out1_8);

  MODEL_AS(FAN_IN_FAN_OUT_16_16_8);
}

MODULE(LECROY429A)
{
  LABEL("");
}

UNIT(LECROY429A_U)
{
  MODEL_AS(FAN_IN_FAN_OUT_4_4_2);
}











