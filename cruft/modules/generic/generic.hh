/////////////////////////////////////////////////////////////////////
//
// Generic modules.  Their names are used by the database generator
// to find starting points.

/////////////////////////////////////////////////////////////////////

MODULE(DAQ_EVENTWISE)
{
  // Data from a module like this is usually collected each event
  LABEL("DAQ eventwise");
}

/////////////////////////////////////////////////////////////////////

MODULE(DAQ_MONITOR)
{
  // Data from a module like this is usually collected once in a while
  LABEL("DAQ monitor");
}

/////////////////////////////////////////////////////////////////////

MODULE(SLOW_CONTROL)
{
  // A module like this is used for slow control
  LABEL("Slow control");
}

/////////////////////////////////////////////////////////////////////

MODULE(CFD)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("CFD");
}

/////////////////////////////////////////////////////////////////////

MODULE(TDC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("TDC");
}

/////////////////////////////////////////////////////////////////////

MODULE(QDC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("QDC");
}

/////////////////////////////////////////////////////////////////////

MODULE(ADC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("ADC");
}

/////////////////////////////////////////////////////////////////////

MODULE(SCALER)
{
  MODEL_AS(DAQ_MONITOR);

  LABEL("SCALER");
}

/////////////////////////////////////////////////////////////////////

MODULE(DETECTOR)
{
  LABEL("Detector (1 ch)");
}

/////////////////////////////////////////////////////////////////////

MODULE(HV)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("HV");
}

/////////////////////////////////////////////////////////////////////

MODULE(CONTROLLER)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("CONTROLLER");
}

/////////////////////////////////////////////////////////////////////

MODULE(PROCESSOR)
{
  LABEL("PROCESSOR");

  HAS_SETTING("HOSTNAME"=>"[_a-zA-Z0-9-]+");

  CONNECTOR(vsb_out);
}

/////////////////////////////////////////////////////////////////////


