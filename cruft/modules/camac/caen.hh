/////////////////////////////////////////////////////////////////////

MODULE(CAENC414)
{
  MODEL_AS(TDC);

  HAS_SETTING("GAIN" => "%fps/ch");

  CONTROL("CLASS","caen_c414");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("CTRL_FCN","clear");
  CONTROL("DAQ_FCN","ARGS:N:qnd_read_clear");

  CONTROL("FIRST_CHANNEL","stop1");
  CONTROL("ELECLOC_NAME","TDC_8");

  CONTROL("DISABLE_CHANNELS","ON");

  LEMO_INPUT(stop1_8,SIGNAL);

  LEMO_INPUT(common_start);
  LEMO_INPUT(common_stop);
  LEMO_INPUT(common_clear);
}

/////////////////////////////////////////////////////////////////////
