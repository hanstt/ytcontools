/////////////////////////////////////////////////////////////////////

MODULE(PHILLIPS7186H)
{
  MODEL_AS(TDC);

  HAS_SETTING("GAIN" => "%fps/ch");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  CONTROL("CLASS","phillips7186h");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("DAQ_FCN","ARGS:N:qnd_read");

  CONTROL("FIRST_CHANNEL","stop1");
  CONTROL("ELECLOC_NAME","TDCPHI16");

  CONTROL("DISABLE_CHANNELS","ON");

  ECL_INPUT(common_start);
  ECL_INPUT(clear);
  ECL_INPUT(test);
  ECL_INPUT(stop1_16,SIGNAL);

  ECL_INPUT(clear_bus);
  ECL_INPUT(test_bus);
}

/////////////////////////////////////////////////////////////////////

MODULE(PHILLIPS7164H)
{
  MODEL_AS(ADC);

  CONTROL("CLASS","phillips7164h");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("DAQ_FCN","ARGS:N:qnd_read");

  CONTROL("FIRST_CHANNEL","in1");
  CONTROL("ELECLOC_NAME","ADCPHI16");

  CONTROL("DISABLE_CHANNELS","ON");

  ECL_INPUT(gate);
  ECL_INPUT(clear);
  ECL_INPUT(in1_16,SIGNAL);
}

/////////////////////////////////////////////////////////////////////

MODULE(PHILLIPS7120)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("Time/Charge Calibrator");

  CONTROL("CLASS","phillips7120");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","qnd_load_primary_fire");

  BNC_OUTPUT(start);
  BNC_OUTPUT(stop);
  BNC_OUTPUT(gate);
  BNC_OUTPUT(Qout);
}

/////////////////////////////////////////////////////////////////////


