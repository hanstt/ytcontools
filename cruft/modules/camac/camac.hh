
#include "caen.hh"
#include "gsi.hh"
#include "phillips.hh"
#include "silena.hh"
#include "lecroy.hh"

CRATE(CAMAC_CRATE)
{
  LABEL("Camac crate");
}

MODULE(LIFO)
{
  LABEL("LINEAR FAN OUT AMP");

  LEMO_INPUT(in);
  
  LEMO_OUTPUT(out1_16);
}

MODULE(DL1610)
{
  HANDLER("MULTI_IN_OUT","in=e,outa,outb");

  ECL_INPUT(in1_8);
  ECL_INPUT(in9_16);

  ECL_OUTPUT(outa1_8);
  ECL_OUTPUT(outa9_16);

  ECL_OUTPUT(outb1_8);
  ECL_OUTPUT(outb9_16);
}

MODULE(DL1600)
{
  HANDLER("MULTI_IN_OUT","in=e,outa,outb");

  ECL_INPUT(in1_8);
  ECL_INPUT(in9_16);

  ECL_OUTPUT(outa1_8);
  ECL_OUTPUT(outa9_16);

  ECL_OUTPUT(outb1_8);
  ECL_OUTPUT(outb9_16);
}

MODULE(RMX3)
{
  MODEL_AS(SLOW_CONTROL);

  CONTROL("CLASS","rmx3");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");

  CONTROL("ELECLOC_NAME","RM4800");

  CONTROL("FIRST_CHANNEL","out1");
  CONTROL("FIRST_CHANNEL_MULTIPLEXER","out1");
  CONTROL("MULTIPLEXER","out1_12=in1a_12d");

  LEMO12_INPUT(in1a_3d,SIGNAL);
  LEMO12_INPUT(in4a_6d,SIGNAL);
  LEMO12_INPUT(in7a_9d,SIGNAL);
  LEMO12_INPUT(in10a_12d,SIGNAL);

  LEMO12_OUTPUT(out1_12);
}

MODULE(RMX2)
{
  MODEL_AS(SLOW_CONTROL);

  CONTROL("CLASS","rmx2");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");

  CONTROL("ELECLOC_NAME","RM2400");

  CONTROL("FIRST_CHANNEL","out1");
  CONTROL("FIRST_CHANNEL_MULTIPLEXER","out1");
  CONTROL("MULTIPLEXER","out1_6=in1a_6d");

  LABEL("");
  LEMO_INPUT(in1a_6d,SIGNAL);

  LEMO_OUTPUT(out1_6);
}

MODULE(RM2400)
{
  MODEL_AS(SLOW_CONTROL);

  CONTROL("CLASS","rmx3");

  CONTROL("ELECLOC_NAME","RM4800");

  CONTROL("FIRST_CHANNEL","out1");
  CONTROL("FIRST_CHANNEL_MULTIPLEXER","out1");
  CONTROL("MULTIPLEXER","out1_6=in1a_6d");

  LABEL("");

  LEMO_INPUT(in1a_6d,SIGNAL);

  LEMO_OUTPUT(out1_6);
}

MODULE(SU1200)
{
  LEMO_INPUT(in1_12);

  HAS_SETTING("SELECT"   => "1-6" , "ALL");
  HAS_SETTING("POLARITY" => "NEG" , "POS");

  LEMO_OUTPUT(analog_out);
}

MODULE(TFA2000_6)
{
  LABEL("");
}

UNIT(TFA2000_6_U)
{
  HANDLER("MULTI_IN_OUT","in=out_pos,out_neg");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out_pos);
  LEMO_OUTPUT(out_neg);
}

MODULE(TFA2000_8)
{
  LABEL("");
}

UNIT(TFA2000_8_U)
{
  HANDLER("MULTI_IN_OUT","in=out_pos,out_neg");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out_pos);
  LEMO_OUTPUT(out_neg);
}

MODULE(HDA2000_12)
{
  LABEL("");
}

UNIT(HDA2000_12_U)
{
  LEMO_INPUT(in);
  LEMO_OUTPUT(out);
}

MODULE(AT8000)
{
  LABEL("");
}


MODULE(CAMAC_CRATE_CONTROLLER)
{
  LABEL("CRATE_CONTROLLER");

  HAS_SETTING("CRATE_NO" => 
	      "0","1","2","3","4","5","6","7","8",
	      "9","10","11","12","13","14","15","16");

}

// Although sometimes the VSB cable goes in and out from two different
// slots, For simplicity, please continue the chain in the slot where
// the controller is.  (I.e. use both in and output)

MODULE(CAV1000)
{
  MODEL_AS(CAMAC_CRATE_CONTROLLER);

  CONNECTOR(vsb_in);
  CONNECTOR(vsb_out);
}

MODULE(CBV1000)
{
  MODEL_AS(CAMAC_CRATE_CONTROLLER);

  CONNECTOR(vsb_in);
  CONNECTOR(vsb_out);
}

MODULE(DMS1000)
{
  LABEL("DISPLAY");
}

MODULE(DATAWAY_DISPLAY)
{
  LABEL("Dataway Display");
}

MODULE(CAMAC_TRIGGER)
{
  LABEL("Trigger module");

  ECL_INPUT(in1_8);
  ECL_OUTPUT(out1_8);
}

/////////////////////////////////////////////////////////////////////

MODULE(CVC4)
{
  MODEL_AS(PROCESSOR);
  MODEL_AS(CAMAC_CRATE_CONTROLLER);
}

/////////////////////////////////////////////////////////////////////



















