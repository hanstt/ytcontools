/////////////////////////////////////////////////////////////////////

MODULE(SILENA4418T)
{
  MODEL_AS(TDC);

  HAS_SETTING("GAIN" => "%fps/ch");

  CONTROL("CLASS","silena4418");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("CTRL_FCN","clear");
  CONTROL("DAQ_FCN","ARGS:N:qnd_read_clear");

  CONTROL("FIRST_CHANNEL","stop0");
  CONTROL("ELECLOC_NAME","TDCS4418");

  CONTROL("DISABLE_CHANNELS","ON");

  ECL_INPUT(start);
  ECL_INPUT(stop0_7,SIGNAL);
}

/////////////////////////////////////////////////////////////////////

MODULE(SILENA4418Q)
{
  MODEL_AS(QDC);

  CONTROL("CLASS","silena4418");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("CTRL_FCN","clear");
  CONTROL("DAQ_FCN","ARGS:N:qnd_read_clear");

  CONTROL("FIRST_CHANNEL","in0");
  CONTROL("ELECLOC_NAME","QDC4418Q");

  CONTROL("DISABLE_CHANNELS","ON");

  ECL_INPUT(gate);
  ECL_INPUT(in0_7,SIGNAL);

  ECL_OUTPUT(gate_bus);
}

/////////////////////////////////////////////////////////////////////



