/* Definitions for trigger-related modules.
 */




MODULE(GSI_TRIGGER_BOX)
{
  HANDLER("MULTI_IN_OUT","in,ecl_in=out%a,out%b,before_deadtime,after_deadtime,after_reduction%a,after_reduction%b");

  // Note, the downscale and enable switches are NOT included
  // in the scheme, since they are usually very far from kept
  // constant...

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1a_8b); // two outputs per ch

  LEMO_INPUT(deadtime);

  LEMO_OUTPUT(sum1_3);
  LEMO_OUTPUT(sum_not);

  ECL_INPUT(ecl_in1_8);
  ECL_OUTPUT(before_deadtime1_8);
  ECL_OUTPUT(after_deadtime1_8);
  ECL_OUTPUT(after_reduction1a_8b); // two outputs per ch
}

MODULE(PRIORITY_ENCODER)
{
  LEMO_INPUT(clear);
  LEMO_INPUT(gate);
  LEMO_INPUT(in1_15);

  ECL_OUTPUT(out1_4);
}


