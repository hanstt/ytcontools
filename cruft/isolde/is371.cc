#include "modules/modules.hh"

MODULE(MNV4)
{
  LABEL("4 channel NIM voltage supply");
}

MODULE(MRC1)
{
  LABEL("Remote control master");

  BNC(bus0);
  BNC(bus1);

  SERIAL(rs232);
}

MODULE(STM16)
{
  LABEL("16 channel shaper");
}

MODULE(ORTEC673)
{
  LABEL("SPECTROSCOPY AMPLIFIER AND GATED INTEGRATOR");
}

MODULE(ORTEC579)
{
  LABEL("FAST FILTER AMP");
}

MODULE(ORTEC584)
{

}

MODULE(ORTEC752)
{
  LABEL("AMPLIFIER");
}

MODULE(ORTEC474)
{
  LABEL("TIMING FILTER AMP");
}

MODULE(LECROY4616)
{
  LABEL("ECL-NIM-ECL")
}

MODULE(ORTECGG8010)
{
  LABEL()
}

MODULE(LECROY429)
{
  LABEL("LOGIC FAN-IN/FAN-OUT")
}

MODULE(CAEN_N145)
{
  LABEL("QUAD SCALER AND PRESET COUNTER TIMER");
}

UNIT(CAEN_N145_U)
{
  LEMO_INPUT(nim_in);
  LEMO_INPUT(ttl_in);

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);
}

MODULE(CAEN_N405)
{
  LABEL("3 FOLD LOGIC UNIT");
}

UNIT(CAEN_N405_U)
{
  ;
}

MODULE(LECROY688AL)
{
  LABEL("LEVEL ADAPTER")
}

UNIT(LECROY688AL_U)
{
  HAS_SETTING("MODE" => "NORM","COMPL");
}

////////////////////////////////////////////////
//
// ISOLDE experiment IS371 (oct 2004)
//

////////////////////////////////////////////////
//
// 
//


VME_CRATE(r1c1)
{
  LABEL("DAQ");
}

VME_DISPLAY(r1c1s1)
{

}

RIO2(r1c1s2)
{

}

TRIGGER_MODULE(r1c1s4)
{
 trig2:      /*in  2*/   r1c1s18/outa0;
 
 fast_clear: /*out 2*/   r1c1s18/ecl_in2;
 deadtime:   /*out 4*/   r1c1s18/ecl_in4;
}

CAEN_V993(r1c1s5)
{
  // TODO!
}

CAEN_V830(r1c1s7)
{
 clr: "SCALER CLR" <-   , r1c1s18/outa2;
 trig_in:                 .s9/gate_out;
 trig_out: 50ohm;

 in0:                     .s7u2/out;
}

CAEN_V785(r1c1s9)
{
 gate_in:   .s11/gate_out;
 gate_out:  .s7/trig_in;
 rst:       .s17/outb1;

 in0:       r1c2u3/ecl_out1;
 in1:       r1c2u3/ecl_out2;
}

CAEN_V785(r1c1s11)
{
 gate_in:   .s13/com_out;
 gate_out:  .s9/gate_in;
 rst:       .s17/outb0;

 in0_15:    r2c2s1/shaper_out1_16;
 in16_31:   r2c2s5/shaper_out1_16;
}

CAEN_V775(r1c1s13)
{
 com_in:    .s15/com_out;
 com_out:   .s11/gate_in;
 rst:       .s17/outa1;
}

CAEN_V775(r1c1s15)
{
 com_in:    r2c3s9u2/out2;
 com_out:   .s13/gate_in;
 rst:       .s17/outb1;
}

CAEN_V538A(r1c1s17)
{
 in0:       .s18/outb1;
 in1:       .s18/outa1;

 ecl_outa0: .s15/rst;
 ecl_outa1: .s13/rst;
 ecl_outb0: .s11/rst;
 ecl_outb1: .s9/rst;
}


CAEN_V538A(r1c1s18)
{
 in0:       r2c3s7u2/out1;
 ecl_outa0: .s4/trig2;

 ecl_in1:   .s4/fast_clear;
 outa1:     .s17/in1;
 outb1:     .s17/in0;

 in2:       r2c3s23u4/nim_out1;
 ecl_outa2: .s7/clr;

 ecl_in3:   .s4/deadtime;
 outa3:     r2c3s3u3/in1;
}

////////////////////////////////////////////////
//
// 
//

NIM_CRATE(r2c1)
{
  LABEL("HIGH VOLTAGE");
}

MNV4(r1c1s3)
{

}

MRC1(r2c1s7)
{

}

////////////////////////////////////////////////
//
// 
//

NIM_CRATE(r2c2)
{
  LABEL("AMPLIFIERS");
}

STM16(r2c2s1)
{
 in1_16:             ;
 shaper_out1_16:     ;
 delayed_timing_out: ;

 trig:      r2c3s13u1/in1;
}

STM16(r2c2s5)
{
 in1_16:             ;
 shaper_out1_16:     ;
 delayed_timing_out: ;
}

ORTEC673(r2c2s9)
{
 unipolar_out: ;
}

ORTEC579(r2c2s13)
{
  SETTING("COARSE GAIN" => "500");
  SETTING("FINE GAIN"   => "1.0");

 input: ;
 output: .s15/input;
}

ORTEC584(r2c2s15)
{
  SETTING("DELAY" => "2ns");

 input:  .13/output;
 timing_out1:   r2c3s13u1/in;
 timing_out2: ;
}

ORTEC572(r2c2s19)
{
 uni_out: ;
}

ORTEC474(r2c2s21)
{
 in:  ;
 out: .s23/input;
}

ORTEC584(r2c2s23)
{
  SETTING("DELAY" => "2ns");

 input:  .s21/out;
}

////////////////////////////////////////////////
//
// 
//

NIM_CRATE(r2c3)
{
  LABEL("TRIGGER");
}

LECROY429A(r2c3s3)
{
  
}

LECROY429A_U(r2c3s3u1)
{
 in1:      .s9/start;
 out2: ;
 out4:     .s15u1/out1;
}

LECROY429A_U(r2c3s3u3)
{
 in1:      r1c1s18/outa3;
 in2:      .s9u2/out1;
 in3:      .s7u1/out1;
}

CAEN_2355A(r2c3s5)
{

}

CAEN_2355A_U(r2c3s5u1)
{
 start:       .s5u2/end_marker;
 end_marker:  .s5u2/start;
}

CAEN_2355A_U(r2c3s5u2)
{
 start:       .s5u1/end_marker;
 end_marker:  .s5u1/start;
}

CAEN_2355A(r2c3s7)
{

}

CAEN_2355A_U(r2c3s7u1)
{
  LABEL("VETO");

 start:       .s9u2/end_marker;
 end_marker:  .s2u2/start;

 out1:        .s3u3/in3;
}

CAEN_2355A_U(r2c3s7u2)
{
 start:       .s7u1/end_marker;
 out:         r1c1s18/in0;
}

CAEN_2355A(r2c3s9)
{

}

CAEN_2355A_U(r2c3s9u1)
{
 start:       .s3u1/out4;
 veto:        .s3u3/out4;
 out:         .s9u2/start;
}

CAEN_2355A_U(r2c3s9u2)
{
  LABEL("TRIG/GATE");

 start:       .s9u1/out;
 end_marker:  .s7u1/start;
  
 out1:        .s3u3/in2;
 out2:        r1c1s15/com_in;
}

LECROY4616(r2c3s11)
{
 in1:         r2c3s13u1/out1;
 in2:         r2c3s13u2/out1;
 ecl_out1_16: r1c1s13/in0_15;
}

ORTECGG8010(r2c3s13)
{

}

ORTECGG8010_U(r2c3s13u1)
{
 in:          r2c2s15/timing_out1:
 out1:        r2c3s11/in1;
 out2:        .s1/in4;
}

ORTECGG8010_U(r2c3s13u2)
{
 in:          r2c2s23/timing_out1:
 out1:        r2c3s11/in2;
 out2:        .s1/in7;
}

LECROY429(r2c3s15)
{

}

LECROY429_U(r2c3s15u1)
{
 in1:         r2c2s1/trig;
 out1:        .s21u1/in1;
 out2:        .s1/in3;
}

LECROY429_U(r2c3s15u2)
{
 in1:         .s5u1/out1;
 out1:        .s21u1/in4;
 out2:        .s1/in2;
}

CAEN_N145(r2c3s17)
{

}

CAEN_N145_U(r2c3s17u1)
{
 nim_in:    .s3u1/out3;

 gate_in:   .u5/out2;
 gate_out:  .u2/gate_in;
}

CAEN_N145_U(r2c3s17u2)
{
 nim_in:    .s9u1/out1;

 gate_in:   .u1/gate_in;
 gate_out:  50ohm;
}

CAEN_N145_U(r2c3s17u3)
{
 gate_in:   .u5/out1;
 gate_out:  .u4/gate_in;
}

CAEN_N145_U(r2c3s17u4)
{
 gate_in:   .u3/gate_in;
 gate_out:  50ohm;
}

CAEN_N145_U(r2c3s17u5)
{
 out1:      .u3/gate_in;
 out2:      .u1/gate_in;

 load:      .s5u2/out1;
}

CAEN_N405(r2c3s21)
{

}

CAEN_N405_U(r2c3s21u1)
{
 in1:       .s15u1/out1;
 in2:       r2c2s15/timing_out2;
 in3:       r2c2s23/timing_out2;
 in4:       .s15u2/out1;

 SETTING("1" => "ON" );
 SETTING("2" => "ON" );
 SETTING("3" => "OFF");
 SETTING("4" => "ON" );

 SETTING("MAJORITY" => "OR");
}

LECROY688AL(r2c3s23)
{
  

}

LECROY688AL_U(r2c3s23u3)
{
  SETTING("MODE" => "COMPL");

 in1:      external/T1;
 out1:     .s5u2/start;

 in4:      external/EBIS;
 out4:     .s5u1/start;
}


