#include "modules/modules.hh"
/*
MODULE(MNV4)
{
  LABEL("4 channel NIM voltage supply");
}

MODULE(MRC1)
{
  LABEL("Remote control master");

  BNC(bus0);
  BNC(bus1);

  SERIAL(rs232);
}

MODULE(STM16)
{
  LABEL("16 channel shaper");
}
*/
MODULE(ORTEC673)
{
  LABEL("SPECTROSCOPY AMPLIFIER AND GATED INTEGRATOR");
}

MODULE(ORTEC579)
{
  LABEL("FAST FILTER AMP");
}

MODULE(ORTEC584)
{
  LABEL("");
}

MODULE(ORTEC752)
{
  LABEL("AMPLIFIER");
}

MODULE(ORTEC474)
{
  LABEL("TIMING FILTER AMP");
}

MODULE(LECROY4616)
{
  LABEL("ECL-NIM-ECL");
}

MODULE(ORTECGG8010)
{
  LABEL("");
}

MODULE(LECROY429)
{
  LABEL("LOGIC FAN-IN/FAN-OUT");
}

MODULE(CAEN_N145)
{
  LABEL("QUAD SCALER AND PRESET COUNTER TIMER");
}

UNIT(CAEN_N145_U)
{
  LEMO_INPUT(nim_in);
  LEMO_INPUT(ttl_in);

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);
}

MODULE(CAEN_N405)
{
  LABEL("3 FOLD LOGIC UNIT");
}

UNIT(CAEN_N405_U)
{
  LABEL("");
}

MODULE(LECROY688AL)
{
  LABEL("LEVEL ADAPTER");
}

UNIT(LECROY688AL_U)
{
  HAS_SETTING("MODE" => "NORM","COMPL");
}

MODULE(VME_DISPLAY)
{
  LABEL("");
}

////////////////////////////////////////////////
//
// ISOLDE experiment IS371 (oct 2004)
//

////////////////////////////////////////////////
//
// 
//


VME_CRATE(r1c1)
{
  LABEL("DAQ");
}

VME_DISPLAY(r1c1s1)
{
  ;
}

RIO2(r1c1s2)
{
  SETTING("HOSTNAME"=>"caisol3");
}

TRIVA3(r1c1s4) // check name
{
 in2:        /*trig2*/   r1c1s20/ecl_out0a;
 
 out2:  /*fast_clear*/   r1c1s20/ecl_in1;
 out4:    /*deadtime*/   r1c1s20/ecl_in3;
}

CAEN_V993(r1c1s5)
{
  ;
}

CAEN_V993_U(r1c1s5u1)
{
 start:                  r1c1s5u2/end_marker;
 end_marker:             r1c1s5u2/start;
}

CAEN_V993_U(r1c1s5u2)
{
 start:                  r1c1s5u1/end_marker;
 end_marker:             r1c1s5u1/start;
 ecl_out:                r1c1s7/in0;
}

CAEN_V830(r1c1s7)
{
 clr: "SCALER CLR" <-   , r1c1s20/ecl_out2a;
 trig_in:                 .s9/gate_out;
 trig_out: ; // 50ohm

 in0:                     .s5u2/ecl_out;
 in1:                     .s20/ecl_out4b;
 in2:                     .s20/ecl_out6a;
 in3:                     .s19/ecl_out3b;
}

CAEN_V785(r1c1s9)
{
  SETTING("ADDRESS"=>"0x00400000");

 gate_in:    r1c3s5u2/out1;
 gate_out:  .s7/trig_in;
 rst:       .s19/ecl_out1b;
}

CAEN_V538A(r1c1s19)
{
 in1:       .s20/out1a;
 in3:       ; // ??

 ecl_out1b: .s9/rst;

 ecl_out3b: .s7/in3;
}


CAEN_V538A(r1c1s20)
{
 in0:       r1c3s3u2/out2;
 ecl_out0a: .s4/in2;

 ecl_in1:   .s4/out2;
 out1a:     .s19/in1;

 in2:       ; // fake T2: r1c3s1u2/out2;
 ecl_out2a: .s7/clr;

 ecl_in3:   .s4/out4;
 // out3a:     r2c3s3u3/in1;
 
 in4:       r1c3s9u1/out3; 
 out4a:     r1c3s9u3/in2;
 ecl_out4b: .s7/in1;

 in6:       r1c3s3u2/out1;
 ecl_out6a: .s7/in2;
}

////////////////////////////////////////////////
//
// 
//

NIM_CRATE(r1c3)
{
  LABEL("TRIGGER");
}

CAEN_N93B_U(r1c3s3u1)
{
  LABEL("Trigger");
 start:       r1c3s5u1/end_marker;
 end_marker:  .s3u2/start; 
}

CAEN_N93B_U(r1c3s3u2)
{
 start:       .s3u1/end_marker;
 out1:        r1c1s20/in6;
 out2:        r1c1s20/in0;
}

CAEN_N93B_U(r1c3s5u1)
{
  LABEL("Gate");
 start:       .s9u1/out1;
 veto:        r1c3s9u3/out1;
 out1:        .s5u2/start;
 end_marker:  .s3u1/start;
}

CAEN_N93B_U(r1c3s5u2)
{
 start:       .s5u1/out1;
 out1:        r1c1s9/gate_in;
 out2:        .s9u3/in3; 
}

CAEN_N93B_U(r1c3s7u1)
{
 start:       r1c3s9u1/out2;
 out1:        r1c3s9u3/in1;
}

LECROY429A_U(r1c3s9u1)
{
  LABEL("Raw Trigger");
   
 in1:         ; // TRIGGER (CFD)
 
 out1:        .s5u1/start;
 out2:        .s7u1/start;
 out3:        r1c1s20/in4;
 out4:        ; // scaler;
}

LECROY429A_U(r1c3s9u3)
{
  LABEL("Deadtime");

 in1:         .s7u1/out1;
 in2:         r1c1s20/out4a;
 in3:         .s5u2/out2;
 
 out1:        .s5u1/veto; 
}
