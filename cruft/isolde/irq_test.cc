#include "modules/modules.hh"

//////////////////////////////////////////////////////

NIM_CRATE(r1c1)
{
  ;
}

LECROY465(r1c1s5)
{
  ;
}

LECROY465_U(r1c1s5u3)
{
 inA:       .s11u2/out2;
 inB:       .s9u4/notout1;

 out1:      .c2s11/com_in;
}

LECROY429A(r1c1s9)
{
  ;
}

LECROY429A_U(r1c1s9u4)
{
 in1:       .s15u2/out7a;
 in3:       .s15u2/out8a;

 notout1:   .s5u3/inB;
}

CAEN2255B(r1c1s11)
{
  // Model number right?  at least it is a dual timer...
  ;
}

CAEN2255B_U(r1c1s11u1)
{
 start:      .s11u2/end_marker;
 end_marker: .s11u2/start;

 out2:       .s15u1/in8;
}

CAEN2255B_U(r1c1s11u2)
{
 start:      .s11u1/end_marker;
 end_marker: .s11u1/start;

 out1:       .s15u1/in6;
 out2:       .s5u3/inA;
}

CAEN92(r1c1s15)
{
  ;
}

CAEN92_U1(r1c1s15u1)
{
 in6:        .s11u2/out1;
 in8:        .s11u1/out2;

 // out7:    r1c2s5/trig1;

 out8a:      r1c2s11/in1;
 out8b:      r1c2s11/in0;
}

CAEN92_U2(r1c1s15u2)
{
 in7:        r1c2s11/busy;
 in8:        r1c2s5/out4;

 out7a:      r1c1s9u4/in1; // busy
 out8a:      r1c1s9u4/in3; // deadtime
}

//////////////////////////////////////////////////////

VME_CRATE(r1c2)
{
  ;
}

DISPLAY(r1c2s1)
{
  ;
}

RIO2(r1c2s3)
{
  SETTING("HOSTNAME" => "caisol1");
}

TRIVA3(r1c2s5)
{
  // in1:    .c1s15u1/out7;
 out4:       .c1s15u2/in8; // deadtime
}

//CAEN_V262(r1c2s7)
//{
//  SETTING("ADDRESS" => "0x500000");
//}

CAEN_V775(r1c2s11)
{
  SETTING("ADDRESS" => "0x500000");

 com_in:     .c1s5u3/out1;
 busy:       .c1s15u2/in7;

 in0:        .c1s15u1/out8b;
 in1:        .c1s15u1/out8a;
}

//CAEN_V269(r1c2s20)
//{
//  ;
//}

//////////////////////////////////////////////////////
