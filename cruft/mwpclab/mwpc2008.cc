/*
 * File used for RPC Test 
 */

#include "modules/modules.hh"

//Added definations
CRATE(SPLIT_BOX_FAKE)
{
  LABEL("Split Box");
}

MODULE(SPLITTER)
{
  HANDLER("MULTI_IN_OUT","in1_10=outA1_10,outB1_10");
  
  LEMO_INPUT(in1_10);

  LEMO_OUTPUT(outA1_10);
  LEMO_OUTPUT(outB1_10);
}

MODULE(DELAY_1)
{
  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out);
}

MODULE(DELAY_2)
{
  HAS_SETTING("DELAY" => "%fns");
  
  LEMO_INPUT(in1_2);
  LEMO_OUTPUT(out1_2);
}

MODULE(DELAY_4)
{
  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
}

//-----------Camac modules---------//

MODULE(BORER_DATAWAY_DISPLAY)
{
  LABEL("Borer Dataway display/monitor");
}

MODULE(LECROY2551)
{
  MODEL_AS(SCALER);

  //CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(in0_11,SIGNAL);

  LEMO_INPUT(clear);
  LEMO_INPUT(inhibit);
}

MODULE(ORTEC_CF8200)
{
  MODEL_AS(CFD);

  HANDLER("MULTI_IN_OUT","in=out_eclA,out_eclB");
  //CONTROL("FIRST_CHANNEL","in1");

  LEMO_INPUT(in1_8,SIGNAL);
  LEMO_INPUT(veto);
  LEMO_INPUT(test);

  ECL_OUTPUT(out_eclA0_7,SIGNAL);
  ECL_OUTPUT(out_eclB0_7,SIGNAL);

  LEMO_OUTPUT(m);
  LEMO_OUTPUT(or);
}

MODULE(LECROY4303)
{
  LABEL("TFC");
  MODEL_AS(TDC);

  //CONTROL("FIRST_CHANNEL","in_ecl1");

  ECL_INPUT(in_ecl1_16,SIGNAL);
  ECL_OUTPUT(out_ecl0_15,SIGNAL);
  
  ECL_OUTPUT(gate);
  ECL_INPUT(start);
  ECL_INPUT(stop);
  ECL_INPUT(test);
}

MODULE(LECROY4300B)
{
  LABEL("QDC");
  MODEL_AS(QDC);
  
  CONTROL("FIRST_CHANNEL","in_ecl0");

  ECL_INPUT(in_ecl0_15,SIGNAL);
  ECL_OUTPUT(out_ecl1_16);

  ECL_INPUT(WST);
  ECL_INPUT(REQ);
  ECL_INPUT(CLR);
  ECL_INPUT(gate);
  ECL_INPUT(WAK);
  ECL_INPUT(GND);
  ECL_INPUT(TRV1);
  ECL_INPUT(I);
  ECL_INPUT(TRV2);
  ECL_INPUT(REN);
  ECL_INPUT(PASS);
}

MODULE(LECROY2332A)
{
  LABEL("Dual Gate generator");
}

UNIT(LECROY2323A_U)
{
  // MODEL_AS(SLOW_CONTROL);

  LEMO_INPUT(start1_2);
  LEMO_INPUT(stop);

  LEMO_OUTPUT(or);
  LEMO_OUTPUT(blank);
  LEMO_OUTPUT(nim);
  LEMO_OUTPUT(invnim);
  LEMO_OUTPUT(delay);
  LEMO_OUTPUT(ttl);
}

//---------------Nim modules-------------//
MODULE(ESN810L)
{
  LABEL("Fast timing amplifier");

  //CONTROL("FIRST_CHANNEL","in1");

  LEMO_INPUT(in1_8,SIGNAL);
  LEMO_OUTPUT(out1_8,SIGNAL);
}

MODULE(LECROY4616)
{
  LABEL("16ch nim-ecl-nim converter");
  HANDLER("MULTI_IN_OUT","in,in_ecl=outA,outB,out_ecl");

  LEMO_INPUT(in1_16);
  ECL_INPUT(in_ecl1_16);

  LEMO_OUTPUT(outA1_16);
  LEMO_OUTPUT(outB1_16);
  ECL_OUTPUT(out_ecl1_16);
}

MODULE(LEMOECL)
{
  LABEL("LEMO to ECL converter");

  LEMO_INPUT(in1_16);
  ECL_OUTPUT(out_ecl0_15);
}

MODULE(LN405)
{
  LABEL("3Fold logic unit");
}

UNIT(LN405_U)
{
  HAS_SETTING("CH1" => "OFF","ON");
  HAS_SETTING("CH2" => "OFF","ON"); 
  HAS_SETTING("CH3" => "OFF","ON");
  HAS_SETTING("CH4" => "OFF","ON");

  HAS_SETTING("OUT" => "AND","OR");
  
  LEMO_INPUT(in1_4);
  LEMO_INPUT(veto);
  LEMO_INPUT(lin);

  LEMO_OUTPUT(out1_2);
  LEMO_OUTPUT(notout);
}

MODULE(PSM794)
{
  LABEL("Quad gate/delay generator");
}

UNIT(PSM794_U)
{
  HAS_SETTING("DELAY" => "%fus");
  
  LEMO_INPUT(trigger);
  LEMO_OUTPUT(delay);
  LEMO_OUTPUT(gate);
  LEMO_OUTPUT(notgate);
  LEMO_OUTPUT(ttl);
  LEMO_OUTPUT(or);
  LEMO_INPUT(inhibit);
  LEMO_INPUT(reset);
}

MODULE(CAEN454)
{
  LABEL("4-4 fan-in-fan-out");
  HAS_SETTING("MODE" => "4x4","2x8");
  //Possibility to have 4 4fold or 2 8fold
}

UNIT(CAEN454_U)
{
  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(notout1_2);
} 

//-----DETECTORS----------------//

MODULE(MWPC)
{
  LABEL("MWPC detector");
  CONNECTOR(hv);
}

UNIT(MWPC_U)
{
  CONNECTOR(signal_right);
  CONNECTOR(signal_left);
  CONNECTOR(signal_odd);
  CONNECTOR(signal_even);
}

//-----FRONT PLANE-------------//
MWPC(rMWPC1c1s1) { ; } //Front Plane

MWPC_U(rMWPC1c1s1u1)
{
 signal_right: r0c2s2/in1; //Front plane, right anode
 signal_left:  r0c2s2/in2; //Front plane, left anode

 signal_odd:   r0c3s1/in5; //Front plane, odd katode
 signal_even:  r0c3s1/in6;  //Front plane, even katod 
}

//---------------BACK PLANE------------// 
MWPC_U(rMWPC1c1s1u2)
{
 signal_right: r0c2s2/in3; //Back plane right anode
 signal_left:  r0c2s2/in4; //back plane, left anode

 signal_odd:   r0c3s1/in7; //Back plane, odd katode
 signal_even:  r0c3s1/in8;  //Back plane, even katode
}


//-------CAMAC CRATE----------------------//


CAMAC_CRATE(r0c1)
{
  ;
}


BORER_DATAWAY_DISPLAY(r0c1s1)
{
  ;//Monitor.
}

LECROY2551(r0c1s3)
{
  LABEL("Scaler");
 in0: r0c2s9u1/out2;//Trigger req
 in1: r0c2s12u2/out1;//Trigger acc
 
 in2: r0c2s6u1/outA0; //Anodes
 in3: r0c2s6u1/outA1;
 in4: r0c2s6u1/outA2;
 in5: r0c2s6u1/outA3;

 in7: r0c2s10u4/gate;  //TCal
 in8: r0c2s6u2/outB2;  //Tcal sent
 in9:r0c2s10u1/gate;  //Clock
}


ORTEC_CF8200(r0c1s8)
{
  LABEL("Constant fraction discriminator");
  
 in1: r0c3s1/outA1; //Anodes
 in2: r0c3s1/outA2;
 in3: r0c3s1/outA3;
 in4: r0c3s1/outA4;

 or: r0c2s9u1/in1;   //Trigger
 out_eclB0_7: r0c2s6u1/in_ecl0_7; //nim/ecl-> TFC
}

LECROY4303(r0c1s12)
{
  LABEL("TFC");

 in_ecl1_4: r0c2s8/out_ecl1_4; //Anode timings

 in_ecl12:r0c2s6u2/out_ecl0; //Tcal check time

 out_ecl0_15: r0c1s16/in_ecl0_15;    //To QDC

 gate:    r0c1s16/gate;
 start:    r0c2s6u2/out_ecl1;
}

LECROY4300B(r0c1s16)
{
  LABEL("QDC/TDC");

 in_ecl0_15: r0c1s12/out_ecl0_15;
 gate:   r0c1s12/gate;
}

LECROY4300B(r0c1s17)
{
  LABEL("QDC");
  
 in_ecl0_3: r0c2s11/out_ecl0_3;
 gate:   r0c2s8/out_ecl11; 
}

LECROY2323A_U(r0c1s20u1)
{
  LABEL("Deadtime");
  
 start1: r0c2s12u1/out1; //Master start!
 nim:    r0c2s12u3/in1; //Set deadtime
}

LECROY2323A_U(r0c1s20u2)
{
  LABEL("Tcal");

 stop: r0c2s12u4/out1;     
 nim:  r0c2s10u4/trigger;  //start tcal event
 delay:r0c2s6u2/in2;       //count number of sucessful tcal triggers
}


//---------NIM CRATE-------------------------------//

NIM_CRATE(r0c2)
{
  ;
}

ESN810L(r0c2s2)
{
 in1: rMWPC1c1s1u1/signal_right;  //right anode front
 in2: rMWPC1c1s1u1/signal_left;  //left anode front
 in3: rMWPC1c1s1u2/signal_right;  //right anode back
 in4: rMWPC1c1s1u2/signal_left;  //left anode back

 out1:r0c3s1/in1;
 out2:r0c3s1/in2;
 out3:r0c3s1/in3;
 out4:r0c3s1/in4;
}

VARIABLE_DELAY(r0c2s4)
{
 in: r0c2s6u2/outB1;
 out:r0c2s6u2/in0;
}

N638(r0c2s6)
{
  ;
}

N638_U(r0c2s6u1)
{
 in_ecl0_7:r0c1s8/out_eclB0_7; //From TFC
 
 common0:  r0c2s6u2/outA2;
 
 outA0_3:  r0c1s3/in2_5;  //Scalers
 outB0_3:  r0c4s1/in1_4;  //delays
}

N638_U(r0c2s6u2)
{
 in0:     r0c2s4/out;//Delay tcal
 in1:     r0c2s12u2/out2;   //Master start
 in2:     r0c1s20u2/delay;  //Tcal
 in7:     r0c2s10u4/delay;  //Tcal
 
 in_ecl1: r0c2s6u2/out_ecl7;

 outA2:   r0c2s6u1/common0; //tcal
 outB1:   r0c2s4/in;       //dly tcal
 outB2:   r0c1s3/in8;      //scaler
 
 out_ecl0:r0c1s12/in_ecl12;// Tcal check
 out_ecl1:r0c1s12/start;   //TDC start
 out_ecl7:r0c2s6u2/in_ecl1;
}

LECROY4616(r0c2s8)
{
 in1_4: r0c4s1/out1_4; //Delayed times from anodes
 in11:  r0c2s10u3/gate;//gate from clocks, to qdc

 out_ecl1_4: r0c1s12/in_ecl1_4;//to TDC
 out_ecl11:  r0c1s17/gate;     //to QDC
}

LN405(r0c2s9)
{
  ;
}

LN405_U(r0c2s9u1)
{
 in1: r0c1s8/or;      //trigger
 in4: r0c2s10u2/gate; //feeder

 out1:r0c2s9u2/in1;   //to veto/deadtime
 out2:r0c1s3/in0;    //scaler ( requested triggers)

 //------Data taking-----------//
 SETTING("CH1" => "ON");
 SETTING("CH2" => "OFF");
 SETTING("CH3" => "OFF");
 SETTING("CH4" => "OFF");

 //---------Feeder------------//
/*
 SETTING("CH1" => "OFF");
 SETTING("CH2" => "OFF");
 SETTING("CH3" => "OFF");
 SETTING("CH4" => "ON");
*/
 SETTING("OUT" => "OR");

}

LN405_U(r0c2s9u2)
{
 in1:r0c2s9u1/out1;  //incoming triggers
 out1:r0c2s12u1/in1; //accepted triggers 

 veto:r0c2s12u3/out1; //input veto/deadtime.

 //------Data taking-----------//
 SETTING("CH1" => "ON");
 SETTING("CH2" => "OFF");
 SETTING("CH3" => "OFF");
 SETTING("CH4" => "OFF");

 SETTING("OUT" => "AND");
}

PSM794(r0c2s10)
{
  ;
}

PSM794_U(r0c2s10u1)
{
  //TCAL
  SETTING("DELAY" => "1 us");

 trigger: r0c2s10u1/notgate;
 notgate: r0c2s10u1/trigger;
  
 delay:   r0c2s12u4/in1;
 gate:    r0c1s3/in9;
}

PSM794_U(r0c2s10u2)
{
  //FEEDER
  SETTING("DELAY" => "100 us");

 trigger: r0c2s10u2/notgate;
 notgate: r0c2s10u2/trigger;

 gate:    r0c2s9u1/in4;
}

PSM794_U(r0c2s10u3)
{
  //QDC-GATE
  SETTING("DELAY" => "1 us");

 trigger: r0c2s12u1/out3;
 gate:    r0c2s8/in11;
}

PSM794_U(r0c2s10u4)
{
  //TCAL
  SETTING("DELAY" => "1 us");

 trigger: r0c1s20u2/nim;
 delay:   r0c2s6u2/in7;
 gate:    r0c1s3/in7;
}

LEMOECL(r0c2s11)
{
 in1_4:      r0c4s2/out1_4;    //From delayed QDC signals
 out_ecl0_3: r0c1s17/in_ecl0_3; //To QDC
}

CAEN454(r0c2s12)
{
  SETTING("MODE" => "4x4");
}

CAEN454_U(r0c2s12u1)
{
 in1: r0c2s9u2/out1;
 
 out1:r0c1s20u1/start1;
 out2:r0c4s3/in;
 out3:r0c2s10u3/trigger;
 out4:r0c2s12u2/in3;
}

CAEN454_U(r0c2s12u2)
{
 in3: r0c2s12u1/out4;

 out1:r0c1s3/in1;
 out2:r0c2s6u2/in1;
}

CAEN454_U(r0c2s12u3)
{
 in1: r0c1s20u1/nim;
 in2: r0c4s3/out;
 
 out1:r0c2s9u2/veto;
}

CAEN454_U(r0c2s12u4)
{
 in1: r0c2s10u1/delay;
 out1:r0c1s20u2/stop;
}


//"CRATE" 3 SPLITTER
SPLIT_BOX_FAKE(r0c3)
{
  ;
}

SPLITTER(r0c3s1)
{
 in1_4:  r0c2s2/out1_4;
 in5:    rMWPC1c1s1u1/signal_odd;
 in6:    rMWPC1c1s1u1/signal_even;
 in7:    rMWPC1c1s1u2/signal_odd;
 in8:    rMWPC1c1s1u2/signal_even;

 outA1_4:r0c1s8/in1_4;
 outA5_8:r0c4s2/in1_4;
}

//----------Cable Delays-------------_//
CABLE_DELAY(r0c4)
{
  ;
}

DELAY_4(r0c4s1)
{
  SETTING("DELAY" => "100 ns");
 in1_4: r0c2s6u1/outB0_3;
 out1_4:r0c2s8/in1_4;
}

DELAY_4(r0c4s2)
{
  SETTING("DELAY" => "150 ns");
 out1_4: r0c2s11/in1_4;
 in1_4:r0c3s1/outA5_8;
}

DELAY_1(r0c4s3)
{
  SETTING("DELAY" => "50 ns");
 in: r0c2s12u1/out2;
 out:r0c2s12u3/in2;
}

