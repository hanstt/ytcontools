#!/usr/bin/perl -W

# This scripts copies the part of the input which is between lines containing
# BEGIN_${NAME} and END_${NAME} to the output

my $inside = 0;

while ( my $line = <STDIN> )
{
    # Begin copy?
    if ($line =~ /BEGIN_${ARGV[0]}/) { $inside = 1; }

    # Do copy
    if ($inside) { print $line; }

    # End copy?
    if ($line =~ /END_${ARGV[0]}/) { $inside = 0; }
}



