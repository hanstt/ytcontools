#!/usr/bin/perl

use strict;
use warnings;

print "Bad signals file: ".$ARGV[0];
print ".ytc file:        ".$ARGV[1];

open BADSIGS , "<$ARGV[0]";

my %devices = ();
my %devtype = ();
my %problems = ();

broken_signal:
while (my $line = <BADSIGS>)
{
    chomp $line;
    if (!$line) { next broken_signal; }
    if (!($line =~ /^([A-Za-z0-9_]*)\s+(.*)$/)) { die "Cannot parse: $line"; }
    
    my $sig = $1;
    my $symptom = $2;

    print ("=============================================\n");
    print ("signal: $sig    symptom: $symptom\n");

    # Now find lines containing this signal in the dump

    my $found = `grep $sig $ARGV[1] | egrep "\/\/ (SIGNAL|TENSION)"`;

    print $found;

    print ("---------------------------------------------\n");

    # Figure out what devices can be involved in the disaster...

    my @found = split /-/,$found;

    foreach my $device (@found)
    {
	# We are interested in things which can be located (rcsu)

	if ($device =~ /(r[_A-Z0-9]+c[_A-Z0-9]+(s.+)?)\(([a-zA-Z0-9_]+)\)\/([a-zA-Z0-9_]+)(\s+\|\|\s+([a-zA-Z0-9_]+))?/)
	{
	    my $rcsu = $1;
	    my $type = $3;
	    my $con1 = $4;
	    my $con2 = $6;

	    if (!$rcsu) { die "RCSU undefined for $type\n"; }

	    $devtype{$rcsu} = $type;

	    my $prob_ref = $problems{$rcsu};
	    
	    if (!$prob_ref)
	    {
		my %newhash = ();
		$problems{$rcsu} = $prob_ref = \%newhash;
	    }

	    if ($con1) { $prob_ref->{$con1} = "$sig:$symptom"; }
	    if ($con2) { $prob_ref->{$con2} = "$sig:$symptom"; }

	    # Sanitize for printing

	    if (!$con1) { $con1 = "n/a"; }
	    if (!$con2) { $con2 = "n/a"; }

	    print "device: $rcsu $type $con1 $con2 $device\n";
	}
    }
}

sub bylastnum
{
    my $a_pre;
    my $b_pre;
    my $a_num;
    my $b_num;

    if ($a =~ /^(.*?)(\d+)$/) { $a_pre = $1; $a_num = $2; }else { $a_pre = $a;}
    if ($b =~ /^(.*?)(\d+)$/) { $b_pre = $1; $b_num = $2; }else { $b_pre = $b;}

    if ($a_pre eq $b_pre && $a_num && $b_num)
    {
	return $a_num <=> $b_num;
    }
    else { return $a cmp $b; }
};

# Then print summary information per device

my @devices = sort keys %devtype;

foreach my $rcsu (@devices)
{
    print ("Q =============================================\n");
    print ("Q device: $devtype{$rcsu} $rcsu\n");
    
    my $prob_ref = $problems{$rcsu};

    my @connectors = sort bylastnum keys %$prob_ref;

    foreach my $con (@connectors)
    {
	print sprintf ("Q %-20s %s\n","$rcsu/$con",$prob_ref->{$con});
    }
}

