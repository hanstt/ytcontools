
#include <stdio.h>  // This must be first include

#include "util.hh"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

void print_header(const char *marker,
		  const char *comment)
{
  printf ("\n"
	  "/** BEGIN_%s **********************************************\n"
	  " *\n"
	  " * %s\n"
	  " *\n"
	  " * Do not edit - automatically generated.\n"
	  " */\n",
	  marker,
	  comment);
}

void print_footer(const char *marker)
{
  printf ("/** END_%s **********************************************/\n"
	  "\n",
	  marker);
}



//#include <stdio.h>  // This must be first include

// Due to general problems getting va_list to work nicely
// in make_message together with vsnprintf, we need to figure
// out if vsnprintf uses old or new style.  If stdarg was pulled
// in by stdio, then it's new style.  Otherwise it's old style

// I hate this shit.

#ifdef _STDARG_H
#define VSNPRINTF_USES_STDARG 1
#endif
// #ifdef __powerpc__
// For some reason of course things has to be difficult.
// #define VSNPRINTF_USES_STDARG 1
// #endif

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


// code stolen from: Linux Programmer's Manual PRINTF(3)   

#ifdef VSNPRINTF_USES_STDARG
// If your machine wants to use this, then I'd like to know if
// if works (and compiles cleanly)
#error "Please contact the author, this code possibly does not work."
char *make_message(const char *fmt,...)
{
  assert(fmt);

  va_list ap;
  /* Guess we need no more than 100 bytes. */
  int n, size = 1+strlen(fmt);
  char *p;

  if ((p = (char*) malloc (size)) == NULL)
    return NULL;
  while (1) {
    /* Try to print in the allocated space. */
    va_start(ap,fmt);
    n = vsnprintf (p, size, fmt, ap);
    va_end(ap);
    /* If that worked, return the string. */
    if (n > -1 && n < size)
      return p;
    /* Else try again with more space. */
    if (n > -1)    /* glibc 2.1 */
      size = n+1; /* precisely what is needed */
    else           /* glibc 2.0 */
      size *= 2;  /* twice the old size */
    if ((p = (char*) realloc (p, size)) == NULL)
      return NULL;
  }
}    

#else //!VSNPRINTF_USES_STDARG

#ifdef USING_GCC_2_95
extern "C" int vsnprintf (char *__restrict __s, size_t __maxlen,
                      __const char *__restrict __format, _G_va_list __arg)
     __THROW __attribute__ ((__format__ (__printf__, 3, 0)));
#endif

char *make_message_internal(const char *fmt,va_list ap)
{
  /* Guess we need no more than 100 bytes. */
  int n, size = 1+strlen(fmt);
  char *p, *p_new;

  if ((p = (char*) malloc (size)) == NULL)
    return NULL;
  while (1) {
    /* We must make a copy, since amd64 (and ppc?)
     * choke on multiple traversals
     */
    va_list aq;
#ifdef va_copy
    va_copy(aq,ap);
#else
    __va_copy(aq,ap);
#endif
    /* Try to print in the allocated space. */
    n = vsnprintf (p, size, fmt, aq);
    va_end(aq);
    /* If that worked, return the string. */
    if (n > -1 && n < size)
      return p;
    /* Else try again with more space. */
    if (n > -1)    /* glibc 2.1 */
      size = n+1; /* precisely what is needed */
    else           /* glibc 2.0 */
      size *= 2;  /* twice the old size */
    p_new = (char *)realloc (p, size);
    if (NULL == p_new) {
      free(p);
      return NULL;
    }
    p = p_new;
  }
}

char *make_message(const char *fmt,...)
{
  va_list ap;
  char *p;

  assert(fmt);
  
  va_start(ap, fmt);
  p = make_message_internal(fmt,ap);
  va_end(ap);

  return p;
}

#endif //!VSNPRINTF_USES_STDARG
