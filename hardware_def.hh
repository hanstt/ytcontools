#ifndef __HARDWARE_DEF_H__
#define __HARDWARE_DEF_H__

#include "file_line.hh"

#include "rcsu.hh"

#include <vector>
#include <map>
#include <set>

#include <stdio.h>

class hardware_attr
{
  public:
    virtual ~hardware_attr() { }
  
    file_line _loc;
};

#include <list>

// This is a list and not a vector such that we
// can modify it (due to expand) when we are apply()-ing
// it to the hardware module.  (otherwise iterators are
// dead)

typedef std::list<hardware_attr*> hardware_attr_list;

// Markers used for tracing signals

#define CI_SIGNAL  0x01 // connector is signal carrier
#define CI_TENSION 0x02 // connector is high voltage carrier

class connector_attr : public hardware_attr
{
  public:
    connector_attr(const char *label, int info)
    {
      _label = label; 
      _info = info;
    }
    virtual ~connector_attr() { }
    bool expand(hardware_attr_list* attr, hardware_attr_list::iterator&
        insert_after);
    virtual connector_attr* create_copy() = 0;
    virtual void dump() = 0;
    virtual bool is_direction(bool input) = 0;
  
    const char *_label;
    int _info;
};

class bidi_connector : public connector_attr
{
  public:
    bidi_connector(const char *label,int info) : connector_attr(label,info) { }
    virtual ~bidi_connector() { }
    virtual connector_attr* create_copy() {
      return new bidi_connector(NULL,_info);
    }
    virtual void dump() {
      printf ("  CONNECTOR(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool) {
      return true;
    }
};

class ecl_input : public connector_attr
{
  public:
    ecl_input(const char *label,int info) : connector_attr(label,info) { }
    virtual ~ecl_input() { }
  
    virtual connector_attr* create_copy() {
      return new ecl_input(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_INPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return input;
    }
};

class ecl_output : public connector_attr
{
  public:
    ecl_output(const char *label,int info) : connector_attr(label,info) { }
    virtual ~ecl_output() { }
    virtual connector_attr* create_copy() {
      return new ecl_output(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_OUTPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return !input;
    }
};

class lemo_input : public connector_attr
{
  public:
    lemo_input(const char *label,int info) : connector_attr(label,info) { }
    virtual ~lemo_input() { }
  
    virtual connector_attr* create_copy() {
      return new lemo_input(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_INPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return input;
    }
};

class lemo_output : public connector_attr
{
  public:
    lemo_output(const char *label,int info) : connector_attr(label,info) { }
    virtual ~lemo_output() { }
    virtual connector_attr* create_copy() {
      return new lemo_output(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_OUTPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return !input;
    }
};

class sma_input : public connector_attr
{
  public:
    sma_input(const char *label,int info) : connector_attr(label,info) { }
    virtual ~sma_input() { }
  
    virtual connector_attr* create_copy() {
      return new sma_input(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_INPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return input;
    }
};

class sma_output : public connector_attr
{
  public:
    sma_output(const char *label,int info) : connector_attr(label,info) { }
    virtual ~sma_output() { }
    virtual connector_attr* create_copy() {
      return new sma_output(NULL,_info);
    }
    virtual void dump() {
      printf ("  LEMO_OUTPUT(%s);\n",_label/*->c_str()*/);
    }
    virtual bool is_direction(bool input) {
      return !input;
    }
};

std::vector<const char *> *append_node(std::vector<const char *> *v, const
    char *a);

class has_setting : public hardware_attr
{
  public:
    has_setting(const char *name,std::vector<const char *> *values)
    {
      _name   = name; 
      _values = values;
    }
    virtual ~has_setting() { }
    void dump();
  
  public:
    const char                *_name;
    std::vector<const char *> *_values;
};

class ha_label : public hardware_attr
{
  public:
    ha_label(const char *label) { _label = label; };
    virtual ~ha_label() { }
    void dump() {
      printf ("  LABEL(\"%s\");\n",_label/*->c_str()*/);
    }
  
    const char *_label;
};

class ha_control : public hardware_attr
{
  public:
    ha_control(const char *key,const char *value) {
      _key = key; _value = value;
    }
    virtual ~ha_control() { }
    bool operator<(const ha_control &rhs) const
    {
      return _key < rhs._key;
    }
    void dump() {
      printf ("  CONTROL(\"%s\",\"%s\");\n",_key,_value);
    }

  public:
    const char *_key;
    const char *_value;
};

class ha_name : public hardware_attr
{
  public:
    ha_name(const char *name) {
      _name = name;
    };
    virtual ~ha_name() { }
    void dump() {
      printf ("  NAME(\"%s\");\n",_name/*->c_str()*/);
    }
  
    const char *_name;
};

class ha_handler : public hardware_attr
{
  public:
    ha_handler(const char *name,const char *initstring)
    {
      _name = name; 
      _initstring = initstring; 
    }
    virtual ~ha_handler() { }
    virtual std::vector<const char *> *get_continuation(const char *) = 0;
    void dump() 
    {
      printf ("  HANDLER(\"%s\",\"%s\");\n",
  	    _name/*->c_str()*/,
  	    _initstring/*->c_str()*/); 
    }
  
  public:
    const char *_name;
    const char *_initstring;
};

ha_handler *new_ha_handler(const char *name,const char *initstring);


class hardware_def;


class ha_model_as : public hardware_attr
{
  public:
    ha_model_as(hardware_def *parent) 
    { 
      _parent = parent;
    }
    virtual ~ha_model_as() { }
    void dump();
  
    hardware_def *_parent;
};

hardware_attr_list *append_node(hardware_attr_list *v,
				hardware_attr *a);

enum hardware_type
  {
    HW_RACK = 1,
    HW_DET,
    HW_CRATE,
    HW_MODULE,
    HW_UNIT,
    HW_DEVICE // generic, any (and none) in RCSU hierarchy
  };

typedef std::map<const char *,has_setting*>    has_settings_map;
typedef std::map<const char *,connector_attr*> connector_attr_map;

template<typename T>
struct compare_ptr
{
  bool operator()(const T *m1, const T *m2) const
  {
    return *m1 < *m2;
  }
};

typedef std::multiset<ha_control*,compare_ptr<ha_control> > control_multiset;

typedef std::vector<hardware_def*>            modelled_as_vector;

class hw_module;

typedef std::vector<hw_module*>               module_vector;

typedef std::vector<ha_model_as*>             model_as_vector;

class hardware_def
{
  public:
    hardware_def();
    void apply(hardware_attr_list *attr);
    bool check_rcsu_level(rcsu& rcsu);
    void dump();
    void get_connectors(std::vector<const char *> &dest);
    const char *get_control(const char *key);
    std::vector<const char*> *get_controls(const char *key, std::vector<const
        char*> *vect = NULL);
    bool has_controls(const char *key);
  
  public:
    hardware_type     _hw_type;
    const char *_name;
  
    ha_label    *_label;
    ha_name     *_name_lit;
  
    ha_handler  *_handler;
  
    model_as_vector    _model_as;
  
    has_settings_map   _settings;
    connector_attr_map _connectors;
  
    control_multiset   _controls;
  
    modelled_as_vector _modelled_as;
  
    module_vector      _instances;
  
    file_line _loc;
};


hardware_def *new_hardware_def(file_line loc,
			       hardware_type hw_type,
			       const char *name);

/* Keeping a list of all known kind of hardware.
 */

void append_hardware(hardware_def *a);

hardware_def *find_hardware_def(const char *model);

void dump_hardware();

#define APPLY_MULTIPLE_INSTANCE(type,local,key,member_list,value_type)       \
      { type *local = dynamic_cast<type*>(a);                                \
      if (local)                                                             \
	{                                                                    \
	  if (member_list.find(local->key) != member_list.end()) {           \
            local->_loc.print_lineno(stderr);                                \
	    fprintf(stderr,"already has " #local " with name '%s'\n",local->key/*->c_str()*/); \
          }                                                                  \
	  else member_list.insert(value_type(local->key,local));             \
          continue;                                                          \
        } }

#define APPLY_MULTIPLE_INSTANCE_CHECK(type,local,key,member_list,value_type) \
      { type *local = dynamic_cast<type*>(a);                                \
      if (local)                                                             \
	{                                                                    \
	  if (member_list.find(local->key) != member_list.end()) {           \
            local->_loc.print_lineno(stderr);                                \
	    fprintf(stderr,"already has " #local " with name '%s'\n",local->key/*->c_str()*/); \
          }                                                                  \
          else { check_valid_##type(local);                                  \
	  member_list.insert(value_type(local->key,local)); }                \
          continue;                                                          \
        } }

#define APPLY_SINGLE_INSTANCE(type,local,member)                  \
      { type *local = dynamic_cast<type*>(a);                     \
      if (local)                                                  \
	{                                                         \
	  if (member) {                                           \
            local->_loc.print_lineno(stderr);                     \
	    fprintf(stderr,"already has " #local);                \
          }                                                       \
	  else member = local;                                    \
	  continue;                                               \
	} }

std::vector<std::string>* expand_string(const char* label,
					bool allow_reversed = false,
					std::vector<int> *levels = NULL);
std::vector<std::string>* expand_string_one(const char* label,
					    bool allow_reversed = false,
					    std::vector<int> *levels = NULL);

#endif// __HARDWARE_DEF_H__
