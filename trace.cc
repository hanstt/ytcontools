
#include "trace.hh"

#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

#include "util.hh"


void connection_parts::print()
{
  if (_label)
    printf ("[%s]",_label);
  if (_channel != -1)
    printf ("{%d}",_channel);
  /*
  for (connection_part_vect::iterator i = _trace.begin();
       i != _trace.end(); ++i)
    {
      // if (i->_conn[0])
	
      printf("/%s(%s)/",
	     i->_module->_location.to_string().c_str(),
	     i->_module->_model->_name);
      
      if (i->_conn[1])
	{
	  printf("%s",
		 i->_conn[1]->_attr->_label);
	  printf(" | %s",
		 i->_conn[1]->_other_attr->_label);
	}
    }
*/
}

const char *merge_prefix_postfix(const char *prefix_postfix,
				 const char *label)
{
  // The old prefix and postfix will be outermost

  if (!prefix_postfix)
    return label; // nothing to do

  // find the separator

  const char *sep = strchr(prefix_postfix,'+');

  // prefix is prefix_postfix .. sep-1
  // postfix is sep+1 .. '\0'

  size_t length = strlen(prefix_postfix) + strlen(label); // one '+' will go -> '\0'

  char *new_label = (char*) malloc(length);

  // copy the old prefix first
  strncpy(new_label,prefix_postfix,sep-prefix_postfix);
  new_label[sep-prefix_postfix] = 0;
  // the the new label (which will add the '+' on the right side :-) )
  strcat(new_label,label);
  // and then the postfix
  strcat(new_label,sep+1);
  /*
  fprintf (stderr,"<%s> (<%s>) <%s> -> <%s>\n",
	   prefix_postfix,sep,label,new_label);
  */
  assert(strlen(new_label) == length - 1);

  return new_label;
}

void trace_signal(connection_parts_vector *traces,
		  connection_parts *trace,
		  int info,
		  bool backwards)
{
  // Responsibility for deleting trace is ours.
  // Either it is (modified and) added to trace (once).
  // Or we are to delete it.  
  // For multiple addition, we need to make copies.

  // First find the other module

  hw_connection *conn = trace->back()._conn[1];

  hw_module *other = find_module(conn->_other);

  if (!other)
    {
      fprintf (stderr,"Dangling cable found.  Impossible? (previous errors?)\n");
      // Dangling cable, really should not happen
      delete trace;
      return;
    }

  if (!conn->_other_attr)
    {
      fprintf (stderr,"No attributes for other connector.  Impossible? (previous errors?)\n");
      delete trace;
      return;
    }

  hw_connection *conn_other = other->get_connection(conn->_other_attr->_label);

  if (!conn_other)
    {
      fprintf (stderr,"Could not find other connector.  Impossible? (previous errors?)\n");
      delete trace;
      return;
    }

  printf (" - %s(%s)/%s",
    other->_location.to_string().c_str(),
    other->_model->_name/*->c_str()*/,
    conn->_other_attr->_label/*->c_str()*/);

  // Then, either that module has a label, in which case the tracing
  // will stop here.  We also accept if a connector has a label.

  const char *label = NULL;

  if (conn_other->_label)
    {
      label = conn_other->_label;
    }

  if (conn->_label)
    {
      if (label)
	{
	  // We already had a label.  (the labels at both ends of the
	  // cable do not match).  TODO: this testing should perhaps
	  // be done when checking the documentation sanity, but then
	  // on the fact that the labels should (if both present) be
	  // equal

	  conn->_loc.print_lineno(stderr);
	  fprintf (stderr,"Labels at both ends of cable, this (%s).\n",
		   conn->_label);
	  conn_other->_loc.print_lineno(stderr);
	  fprintf(stderr,"Using other end label (%s).\n",label);
	}
      else
	label = conn->_label;
    }

  // A label on the connector takes precedence over the label on the
  // module.  But _both_ ends of the cable may not be labeled.
  // (Actually we'd prefer the output to be labeled, since that would
  // be the production point of a signal.)  And, if possible (it makes
  // sense), one should rather label the entire module/unit than the
  // individual connectors.

  if (other->_label && !label)
    label = other->_label->_label;

  if (label)
    {
      // Is the label a pre-/postfix label?

      if (label[0] == '+' ||
	  label[strlen(label)-1] == '+')
	{
	  printf("[%s]",label);
	 
	  // for pre-/postfix labels we continue the search
	  // this stuff will be added at the end...

	  trace->_label = merge_prefix_postfix(trace->_label,label);
	}
      else
	{
	  // We have found the end
	  
	  printf (" -- %s",label/*->c_str()*/);
	  
	  // Now, has it been disabled???
	  
	  hw_module *parent = other;
	  
	  do
	    {
	      const char *daq_active = parent->get_control("DAQ_ACTIVE");
	      
	      if (daq_active)
		{
		  printf ("(DAQ_ACTIVE=%s)",daq_active);
		  if (strcmp(daq_active,"ON")  != 0 &&
		      strcmp(daq_active,"OFF") != 0)
		    {
		      fprintf (stderr,"Invalid DAQ_ACTIVE : %s , must be ON or OFF:\n",daq_active);
		      delete trace;
		      return;
		    }
		  
		  if (strcmp(daq_active,"OFF") == 0)
		    {
		      
		      delete trace;
		      return;
		    }
		}
	      
	      // If this channel is declared as a module, slot or unit,
	      // also check that no of the enclosing modules are declared as
	      // deactivated
	      
	      parent = parent->find_parent_module();
	      
	      printf ("P:%d",parent!=NULL);
	    }
	  while (parent);
	  
	  trace->push_back(connection_part(conn_other,other));
	  trace->_label = merge_prefix_postfix(trace->_label,label);
	  traces->push_back(trace);
	  return;
	}
    }

  // A little special casing: if the signal we reached is an multiplexed
  // signal, then we can stop here, mark the signal MUX !

  std::vector<const char*> *muxs = other->_model->get_controls("MULTIPLEXER");

  if (muxs)
    {
      // We have a set of parameters
      
      std::vector<const char*>::const_iterator mi;
      
      for (mi = muxs->begin(); mi != muxs->end(); ++mi)
	{
	  const char *multiplexer = *mi;

	  // We are a multiplexer.  Does the name match?
	  /*
	    fprintf (stderr,"multiplex: %s == %s ?\n",
	    conn->_other_attr->_label,
	    multiplexer);
	  */
	  // The multiplexer string is on the form  OUTPUTS=INPUTS
	  
	  const char *equals = strchr(multiplexer,'=');
	  char *output_str = NULL;
	  if (equals)
	    output_str = strndup(multiplexer,equals-multiplexer);
	  if (output_str)
	    {
	      std::vector<std::string> *outputs = NULL;
	      
	      outputs = expand_string_one(output_str);
	      free(output_str);
	      
	      std::vector<std::string>::const_iterator oi;
	      
	      for (oi = outputs->begin(); oi != outputs->end(); ++oi)
		{
		  if (strcmp(conn->_other_attr->_label,oi->c_str()) == 0)
		    {
		      // We have a multiplexer match!
		      
		      trace->push_back(connection_part(conn_other,other));
		      trace->_label = "MULTIPLEXED";
		      traces->push_back(trace);
		      return;
		    }	      
		}
	    }
	  
	  //fprintf (stderr,"multiplex: %s == %s no\n",
	  //       conn->_other_attr->_label,
	  //       multiplexer);
	  
	}
    }
  
  // Or we need to continue.  This means to figure out
  // what input is the corresponding one...  Yippie.  This
  // will be a little heuristic.  No, we go according to
  // a few simple rules:
  //
  // Only one connector in the continuation direction => chosen
  // Only one of the connectors marked SIGNAL/TENSION => chosen
  // Several connectors, then a handler is needed.

  // First, if we have a handler, use that

  assert(other->_model);

  if (other->_model->_handler)
    {
      std::vector<const char *> *cont =
	other->_model->_handler->get_continuation(conn->_other_attr->_label);

      if (!cont)
	{
	  printf (" HANDLER_FAILED ");
	  delete trace;
	  return;
	}

      std::vector<const char *>::iterator i;

      for (i = cont->begin(); i != cont->end(); ++i)
	{
	  hw_connection* continuation = other->get_connection(*i);

	  if (continuation)
	    {
	      printf(" || %s",continuation->_attr->_label/*->c_str()*/);

	      if (trace->has_continuation(continuation))
		{
		  printf (" CABLE-LOOP ");
		}
	      else
		{
		  connection_parts *trace_copy = new connection_parts(*trace);
		  
		  trace_copy->insert_continuation(continuation);
		  trace_copy->push_back(connection_part(conn_other,other,continuation));
		  trace_signal(traces,trace_copy,info,backwards);
		}
	    }
	}

      delete cont;
      delete trace;

      // If we had a handler, we're done

      return;
    }

  // Look for input signals.  Accept if we either have none yet,
  // or if this one is distinguished by having appropriate marker

  hw_connection *continuation = NULL;
  bool has_marker  = false;
  bool need_marker = false;

  connections_map::iterator i;

  for (i = other->_connections.begin(); 
       i != other->_connections.end(); ++i)
    {
      hw_connection *next_conn = dynamic_cast<hw_connection*>(i->second);

      if (next_conn &&
	  next_conn->_attr->is_direction(backwards))
	{
	  // Have connection in the right direction

	  if (next_conn->_other_attr &&
	      next_conn->_other_attr->_label == conn->_attr->_label)
	    continue; // hmm, going out same way we came in?

	  bool with_marker = !!(next_conn->_attr->_info & info);

	  if (next_conn->_attr->_info & (CI_SIGNAL | CI_TENSION))
	    if (!with_marker)
	      continue; // wrong kind of marker

	  if (continuation)
	    {
	      if (has_marker)
		{
		  if (with_marker)
		    {
		      other->_loc.print_lineno(stderr);
		      fprintf (stderr,"Standing at: %s(%s)/%s\n",
			       other->_location.to_string().c_str(),
			       other->_model->_name/*->c_str()*/,
			       conn->_other_attr->_label/*->c_str()*/);
		      fprintf (stderr,"Searching for continuation, "
			       "but several possibilities with markers.  "
			       "Cannot distinguish.\n");
		      continuation = NULL;
		      break;
		    }
		}
	      need_marker = true;
	    }
	  if (!has_marker || with_marker)
	    {
	      continuation = next_conn;
	      has_marker = with_marker;
	    }
	}
    }

  if (continuation &&
      (!need_marker || has_marker))
    {
      printf(" | %s",continuation->_attr->_label/*->c_str()*/);

      if (trace->has_continuation(continuation))
	{
	  printf (" CABLE-LOOP ");
	  delete trace;
	}
      else
	{
	  trace->insert_continuation(continuation);
	  trace->push_back(connection_part(conn_other,other,continuation));
	  trace_signal(traces,trace,info,backwards);
	}
      return;
    }

  delete trace;
  return;

  // int info = conn->_attr->_info & (CI_SIGNAL | CI_TENSION);
}

int get_channel_index(const hw_module *module,
		      const hw_connection *conn,
		      const char *name,
		      const char *first_channel,
		      bool allow_failure)
{
  // name is the channel id, e.g. in7
  // first_channel is the channel which will become 0 (zero based), e.g. in1 (or in0)

  // Possibly (but not likely) you will need several variants in first_channel,
  // then use comma.  For now, we do it the easy way

  if (!first_channel)
    goto failure;

  {
    const char *p1 = name;
    const char *p2 = first_channel;
    
    while (*p1 && isalpha(*p1))
      {
	if (*p1 != *p2)
	  goto failure;
	p1++;
	p2++;
      }

    if (!*p1 && !*p2)
      return 0; // both have no index, and compared equal so far
    
    if (!isdigit(*p1) || !isdigit(*p2))
      goto failure;
    
    {
      int index     = atoi(p1);
      int zero_base = atoi(p2);

      if (index < zero_base)
	{
	  if (conn)
	    conn->_loc.print_lineno(stderr);
	  fprintf (stderr,
		   "Zero-based index for %s wrong, channel: %d < base: %d.\n",
		   name,index,zero_base);
	  module->_model->_loc.print_lineno(stderr);
	  fprintf (stderr,
		   "Model declared here.\n");
	}
      
      return index - zero_base;
    }
  }

 failure:
  if (!allow_failure)
    {
      if (conn)
	conn->_loc.print_lineno(stderr);
      fprintf (stderr,
	       "Cannot find zero-based index for %s, (search string: %s)\n",
	       name,first_channel);
      module->_model->_loc.print_lineno(stderr);
      fprintf (stderr,
	       "Model declared here.\n");
    }

  return -1;
}

void find_signals(controllable_module *cm_module)
{
  // The signals connected to a module is defined as all INPUTS
  // that are marked CI_SIGNAL (or CI_TENSION, slow_control purposes)

  // We store which signal is connected, and where to

  // The signal is traced back through all connections until we find
  // a label for it.  (no label => warning)

  // #define CI_SIGNAL  0x01 // connector is signal carrier
  // #define CI_TENSION 0x02 // connector is high voltage carrier

  // Most easy is to loop though all connections, and operate on
  // the wanted ones.

  printf ("// Finding signals for %s@%s...\n",
	  cm_module->_module->_model->_name/*->c_str()*/,
	  cm_module->_module->_location.to_string().c_str());

  int usable_first_channel = true;

  const char *first_channel = 
    cm_module->_module->_model->get_control("FIRST_CHANNEL");

  if (cm_module->_module->_model->get_control("FIRST_CHANNEL_MULTIPLEXER"))
    {
      // HACK: When we are a multiplexer, the first_channel is for
      // the wrong connector
      usable_first_channel = false;
      first_channel = NULL;
    }

  cm_module->_signals = new physical_signals;

  connections_map::iterator i;

  for (i = cm_module->_module->_connections.begin(); 
       i != cm_module->_module->_connections.end(); ++i)
    {
      hw_connection *conn = dynamic_cast<hw_connection*>(i->second);

      if (!conn) // possibly broken marker
	continue;

      assert(conn->_attr);

      //printf("1\n");
      //fflush(stdout);

      if ((conn->_attr->_info & CI_SIGNAL)  && 
	  conn->_attr->is_direction(true  /*input*/))
	{
	  printf ("// SIGNAL:  %s",i->first/*->c_str()*/);
	  //fflush(stdout);

	  // Now trace the signal

	  connection_parts_vector *traces = new connection_parts_vector;
	  connection_parts *trace = new connection_parts(0/*backwards*/);

	  // trace->_channel = i->first;

	  trace->_channel = 
	    get_channel_index(cm_module->_module,conn,
			      i->first,
			      first_channel,
			      !usable_first_channel);

	  printf ("(%2d)",trace->_channel);

	  trace->push_back(connection_part(cm_module->_module,conn));

	  trace_signal(traces,trace,CI_SIGNAL,true  /*input*/);

	  for (connection_parts_vector::iterator t = traces->begin();
	       t != traces->end(); ++t)
	    {
	      (*t)->print();	
	    }
	  cm_module->_signals->insert(traces);

	  delete traces;
	  printf ("\n");
	}

      //printf("2\n");
      //fflush(stdout);

      if ((conn->_attr->_info & CI_TENSION) && 
	  conn->_attr->is_direction(false/*output*/))
	{
	  printf ("// TENSION:  %s",i->first/*->c_str()*/);
	  //fflush(stdout);

	  // Now trace the tension

	  connection_parts_vector *traces = new connection_parts_vector;
	  connection_parts *trace = new connection_parts(1/*forwards*/);

	  trace->_channel = 
	    get_channel_index(cm_module->_module,conn,
			      i->first,
			      cm_module->_module->_model->get_control("FIRST_CHANNEL"));

	  trace->push_back(connection_part(cm_module->_module,conn));

	  trace_signal(traces,trace,CI_TENSION,false/*output*/);

	  for (connection_parts_vector::iterator t = traces->begin();
	       t != traces->end(); ++t)
	    {
	      (*t)->print();
	    }
	  cm_module->_signals->insert(traces);

	  delete traces;
	  printf ("\n");
	}
      //printf("3\n");
      //fflush(stdout);
    }
  //printf("Done...\n");
  //fflush(stdout);

  int num_actual_signals = cm_module->_signals->one_after_last();
  int max_actual_signals = 0;

  // Loop through all connecters, but only use the numbers which
  // are valid indexable inputs

  std::vector<const char *> connectors;
  std::vector<const char *>::iterator connector;

  cm_module->_module->_model->get_connectors(connectors);

  for (connector = connectors.begin(); 
       connector != connectors.end();
       ++connector)
    {
      // printf("[%s]",*connector);
      
      int index = get_channel_index(cm_module->_module,NULL,
				    *connector,
				    first_channel,true);

      // -1 on failure (i.e. not indexable)
      
      if (index >= max_actual_signals)
	max_actual_signals = index+1;
    }
    
  const char *disable;

  disable = cm_module->_module->_model->get_control("DISABLE_CHANNELS");

  if (disable && 
      strcmp(disable,"ON") == 0)
    {
      // We only cut down on channels if at least 25% are unused
      
      if (num_actual_signals <= (max_actual_signals*3)/4)
	{
	  // We also disable channels in bunches of the largest power of
	  // two, smaller than or equal to 25% of the number of channels.
	  // (The idea would be that this way the chance is much larger
	  // that we are actually including connectors that enter the
	  // QDC/TDCs, such that last minute recablings (WHICH ARE
	  // FORBIDDEN) due to single broken channels, anyhow might
	  // survive without major changes.
	  
	  int quantum;
	  
	  for (quantum = 0x1000; quantum >= 2; quantum >>= 1)
	    if (quantum <= max_actual_signals / 4)
	      break;
	  
	  cm_module->_num_use_channels = 
	    ((num_actual_signals + quantum-1) / quantum) * quantum;
	  
	  printf ("// Limiting: max: %d quantum: %d  used: %d => using: %d\n",
		  max_actual_signals,quantum,num_actual_signals,
		  cm_module->_num_use_channels);
	  
	}
    }

  return ;
}



int physical_signals::num_signals()
{
  return _signals.size();
}

int physical_signals::one_after_last()
{
  connection_parts_vector::iterator i;

  int last = -1;

  for (i = _signals.begin(); i != _signals.end(); ++i)
    if ((*i)->_channel > last)
      last = (*i)->_channel;

  return last + 1;
}

connection_parts_vector *physical_signals::get_signals_for_channel(int channel)
{
  connection_parts_vector *match = new connection_parts_vector;

  connection_parts_vector::iterator i;

  for (i = _signals.begin(); i != _signals.end(); ++i)
    if ((*i)->_channel == channel)    
      match->push_back(*i);

  return match;
}
