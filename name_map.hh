#ifndef NAME_MAP_HH
#define NAME_MAP_HH

#include <string>

void name_map_add(char const *, char const *);
void name_map_dump();

#endif
