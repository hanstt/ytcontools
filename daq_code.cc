
#include "daq_code.hh"
#include "enumerate.hh"
#include "hw_module.hh"
#include "trace.hh"
#include "str_set.hh"
#include "util.hh"

#include "setup_param.hh"

#include <stdlib.h>

#include <set>
#include <map>

typedef std::map<hw_module*,controllable_module*> cm_map;

cm_vector cm_modules;

cm_map    cm_modules_map;

struct cm_module_text_rcsu_compare
{
	bool operator()(const controllable_module *m1, 
			const controllable_module *m2) const
	{
		return m1->_module->_location.is_less_text(m2->_module->_location);
	}
};

typedef std::set<controllable_module*,cm_module_text_rcsu_compare> cm_module_text_rcsu_compare_set;

struct hw_module_text_rcsu_compare
{
	bool operator()(const hw_module *m1, 
			const hw_module *m2) const
	{
		return m1->_location.is_less_text(m2->_location);
	}
};

typedef std::set<hw_module*,hw_module_text_rcsu_compare> module_text_rcsu_compare_set;

void find_controllable_modules(const char *type,int flags)
{
	module_vector modules;     

	enumerate_modules(modules,type,"DAQ_CODE");
	enumarate_modules_control(modules,type,"DAQ_CODE");

	module_vector::iterator i;

	for (i = modules.begin(); i < modules.end(); i++)
	{
		hw_module *module = *i;

		printf ("Searched for: %s , found %s\n",type,module->_location.to_string().c_str());

		controllable_module *cm = new controllable_module(module,flags);

		cm_modules.push_back(cm);
		cm_modules_map.insert(cm_map::value_type(module,cm));
	}
}

controllable_module *find_controllable_module(hw_module *module)
{
	cm_map::iterator i;

	i = cm_modules_map.find(module);

	if (i == cm_modules_map.end())
		return NULL;

	return i->second;
}

hw_module *find_vsb_processor(hw_module *controller)
{
	// We look for the processor by following all input VSB cables.
	// We'll either find a controller, or a processor (hopefully).

	while (controller &&
			!controller->is_model_as("PROCESSOR"))
	{
		// Is there any "vsb_in" connector?

		hw_connection *vsb_in = controller->get_connection("vsb_in");

		if (vsb_in)
		{
			// Where does it go?

			controller = find_module(vsb_in->_other);
			continue;
		}

		// Perhaps it is using GTB?

		hw_connection *gtb_master = controller->get_connection("gtb_master");

		if (gtb_master)
		{
			// Where does it go?

			controller = find_module(gtb_master->_other);
			continue;
		}

		// Perhaps we are in a 'crate' which is a computer?

		rcsu rc_crate = controller->_location;

		rc_crate._slot = -1;
		rc_crate._unit = -1;

		hw_module *crate = find_module(rc_crate);

		if (crate && crate->is_model_as("COMPUTER"))
			return crate;

		controller = NULL;
	}  

	return controller;
}

hw_module *find_hv1445_serial_interface(hw_module *controller)
{
	// We look for the processor by following the serial chain.  We'll
	// either find another LeCroy1445 or a LeCroy2132 controller or
	// processor (hopefully).

	while (controller &&
			!controller->is_model_as("PROCESSOR") &&
			!controller->is_model_as("LECROY2132"))
	{
		// Is there any "vsb_in" connector?

		hw_connection *j1 = controller->get_connection("j1");

		// Where does it go?

		controller = find_module(j1->_other);
	}  
	return controller;
}

hw_module *find_vme_processor(hw_module *module,bool break_tie)
{
	// Find all other modules in the crate.
	// Then find the processor.

	// Break ties by selecting the one to the left of the module.  (and warn)

	rcsu rc_crate = module->_location;

	rc_crate._slot = 0;
	rc_crate._unit = -1;

	modules_map::iterator i = all_modules.lower_bound(rc_crate);

	rc_crate._slot = 0x7fff;

	modules_map::iterator j = all_modules.upper_bound(rc_crate);

	hw_module *processor = NULL;

	for ( ; i != j; ++i)
	{
		hw_module *candidate = i->second;

		// module->_loc.print_lineno(stderr);
		// fprintf (stderr,"cand: %s\n",candidate->_location.to_string().c_str());

		if (!candidate->is_model_as("PROCESSOR"))
			continue;

		if (processor)
		{
			// We have a tie.

			module->_loc.print_lineno(stderr);
			fprintf(stderr,"While searching for vme processor, several found.");
			processor->_loc.print_lineno(stderr);
			fprintf(stderr,"Here.");
			candidate->_loc.print_lineno(stderr);
			fprintf(stderr,"Here.");

			if (!break_tie)
				return NULL; // not allowed (e.g. fastbus)

			if (processor->_location._slot > module->_location._slot)
				continue;	    
		}

		processor = candidate;
	}
	return processor;
}

hw_module *find_gtb_master(hw_connection *gtb_master,
		hw_connection **gtb_last)
{
	hw_module *master = NULL;

	*gtb_last = NULL;

	while (gtb_master)
	{
		*gtb_last = gtb_master;

		master = find_module(gtb_master->_other);

		if (!master)
			return NULL;

		gtb_master = master->get_connection("gtb_master");
	}

	return master;
}

int find_gtb_slave_distance(hw_module *slave)
{
	for (int slave_no = 1; slave; ++slave_no)
	{
		hw_connection *gtb_slave = slave->get_connection("gtb_slave");

		if (!gtb_slave)
			return slave_no;

		slave = find_module(gtb_slave->_other);
	}
	return -1;
}

hw_module *find_processor(controllable_module *cm_module)
{
	hw_module *orig_module = cm_module->_module;
	hw_module *module = orig_module;

	const char *control_processor = module->_model->get_control("PROCESSOR");

	if (control_processor)
	{
		if (strcmp(control_processor,"NONE") == 0)
			return NULL;

		orig_module->_loc.print_lineno(stderr);
		fprintf(stderr,"Looking for processor for module, but got unknown PROCESSOR control directive: %s\n",control_processor);
		return NULL;
	}

	// If we have a module UNIT, then go find the module instead.

	if (module->_location._unit != -1)
	{
		rcsu rc_module = module->_location;

		rc_module._unit = -1;

		module = find_module(rc_module);

		if (!module)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for unit, but does not even have an enclosing module.\n");
			return NULL;
		}
	}

	// If the module has a gtb_master connector, it means that it is a slave,
	// controlled by whoever goes up the gtb master chain...

	hw_connection *gtb_master = module->get_connection("gtb_master");

	if (gtb_master)
	{
		hw_connection *gtb_master_last;

		hw_module *master = find_gtb_master(gtb_master,&gtb_master_last);

		if (!master)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.  "
					"But GTB master chain not connected.\n");  
			return NULL;
		}

		int slave_no = find_gtb_slave_distance(module);

		cm_module->_processor = find_vme_processor(master,true);

		if (!cm_module->_processor)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module");
			master->_loc.print_lineno(stderr);
			fprintf(stderr,"But none found in crate of GTB master module.\n");
			return NULL;
		}

		const char *sam_no = master->get_setting("SAM_NO");

		if (!sam_no)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			master->_loc.print_lineno(stderr);
			fprintf(stderr,"But GTB master has no sam # (SAM_NO).\n");
			return NULL;
		}

		int sam = atoi(sam_no);

		if (!gtb_master_last || 
				!gtb_master_last->_other_connector ||
				strncmp(gtb_master_last->_other_connector,"gtb",3) != 0)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			master->_loc.print_lineno(stderr);
			fprintf(stderr,"But connector on GTB master side unknown.\n");
			return NULL;
		}

		int branch = atoi(gtb_master_last->_other_connector+strlen("gtb"));

		cm_module->_flags |= CM_GTB;

		cm_module->_reach.gtb(sam,branch,slave_no);

		printf ("// GTB: %d %d %d : PROC@%s\n",
				cm_module->_reach._access._gtb._sam,
				cm_module->_reach._access._gtb._branch,
				cm_module->_reach._access._gtb._slave,
				cm_module->_processor->_location.to_string().c_str());

		return cm_module->_processor; // useless return?
	}

	// First we need to find the crate the module is in.

	rcsu rc_crate = module->_location;

	rc_crate._slot = -1;
	rc_crate._unit = -1;

	hw_module *crate = find_module(rc_crate);

	if (!crate)
	{
		orig_module->_loc.print_lineno(stderr);
		fprintf(stderr,"Looking for processor for module, but does not even have a crate.\n");
		return NULL;
	}

	// Depending on the kind of crate, we search for the processor in different ways

	if (crate->is_model_as("CAMAC_CRATE"))
	{
		// Either the crate has an intelligent controller (like a CVC)
		// which we currently do no support...


		// Or it has a stupid one like CAV/CBV, so we need to trace cables
		// further to find the big boss

		// Anyhow, crate controllers go into slot 24

		rcsu rc_controller = rc_crate;

		rc_controller._slot = 24;
		rc_controller._unit = -1;

		hw_module *controller = find_module(rc_controller);

		if (!controller || 
				!controller->is_model_as("CAMAC_CRATE_CONTROLLER"))
		{
			// It seems that CAV usually is in slot 25.  Check that one also

			rc_controller._slot = 25;
			rc_controller._unit = -1;

			controller = find_module(rc_controller);

			if (!controller || 
					!controller->is_model_as("CAMAC_CRATE_CONTROLLER"))
			{
				orig_module->_loc.print_lineno(stderr);
				fprintf(stderr,"Looking for processor for module.\n");
				crate->_loc.print_lineno(stderr);
				fprintf(stderr,"But no camac crate controller.\n");
				return NULL;
			}
		}

		// So we found a controller

		// Figure out the crate number

		const char *crate_no = controller->get_setting("CRATE_NO");

		if (!crate_no)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			controller->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate controller has no crate number (CRATE_NO).\n");
			return NULL;
		}

		cm_module->_flags |= CM_CAMAC;

		cm_module->_reach.camac(atoi(crate_no),
				cm_module->_module->_location._slot);

		//cm_module->_access._camac._crate = atoi(crate_no);
		//cm_module->_access._camac._slot  = cm_module->_module->_location._slot;

		// Now go find the processor at the end of the VSB cable

		cm_module->_processor = find_vsb_processor(controller);

		if (!cm_module->_processor)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			controller->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate controller is not connected to any.\n");
			return NULL;
		}

		printf ("// CAMAC: %d %d : PROC@%s\n",
				cm_module->_reach._access._camac._crate,
				cm_module->_reach._access._camac._slot,
				cm_module->_processor->_location.to_string().c_str());

		return cm_module->_processor; // useless return?
	}
	else if (crate->is_model_as("FASTBUS_CRATE"))
	{
		cm_module->_processor = find_vme_processor(module,false);

		if (!cm_module->_processor)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			crate->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate has none.\n");
			return NULL;
		}

		// For the time being, FASTBUS modules does not get any treatment
		// we can most likely handle them as VME crates (at least if there is a NGF)

		cm_module->_flags |= CM_FASTBUS;

		// TODO: the rack and crate are the rack and crate numbers of
		// the cabling documentation, not that used by slow control/daq

		cm_module->_reach.fastbus(cm_module->_module->_location._rack,
				cm_module->_module->_location._crate,
				cm_module->_module->_location._slot);

		//cm_module->_access._fastbus._slot  = cm_module->_module->_location._slot;
		//cm_module->_access._fastbus._crate = cm_module->_module->_location._crate;
		//cm_module->_access._fastbus._rack  = cm_module->_module->_location._rack;

		printf ("// FASTBUS: %d\n",
				cm_module->_reach._access._fastbus._slot);

		return NULL;
	}
	else if (crate->is_model_as("VME_CRATE"))
	{
		const char *address = module->get_setting("ADDRESS");

		if (!address)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"But modules has no address (ADDRESS).\n");
			return NULL;
		}

		const char *crate_no = crate->get_setting("CRATE_NO");

		if (!crate_no)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			crate->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate has no number (CRATE_NO).\n");
			return NULL;
		}

		cm_module->_flags |= CM_VME;

		cm_module->_reach.vme(atoi(crate_no),
				atoi(address),
				cm_module->_module->_location._slot);

		// cm_module->_access._vme._address = atoi(address);

		// Now go find the processor at the end of the VSB cable

		cm_module->_processor = find_vme_processor(module,true);

		if (!cm_module->_processor)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module. But none found in crate.\n");
			return NULL;
		}

		printf ("// VME: 0x%08x : PROC@%s\n",
				cm_module->_reach._access._vme._address,
				cm_module->_processor->_location.to_string().c_str());

		return cm_module->_processor; // useless return?
	}
	else if (crate->is_model_as("LECROY1440"))
	{
		printf ("// Search controller...\n");

		// Anyhow, crate controllers go into slot 19 (LECROY1445)

		rcsu rc_controller = rc_crate;

		rc_controller._slot = 19;
		rc_controller._unit = -1;

		hw_module *controller = find_module(rc_controller);

		if (!controller || 
				!controller->is_model_as("LECROY1445"))
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for controller for module.\n");
			crate->_loc.print_lineno(stderr);
			fprintf(stderr,"But no mainframe crate controller.\n");
			return NULL;
		}

		// Ok, so we found the controller

		// Figure out the crate number

		const char *crate_no = controller->get_setting("CRATE_NO");

		if (!crate_no)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for processor for module.\n");
			controller->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate controller has no crate number (CRATE_NO).\n");
			return NULL;
		}

		cm_module->_flags |= CM_MAINFRAME;

		cm_module->_reach.mainframe(atoi(crate_no),
				cm_module->_module->_location._slot);

		// Ok, now we now how to reach the controller (crate no), and
		// the module itself.  But what is reaching us?  A camac serial
		// adapter, or a serial interface of a computer

		// We for the moment support being connected to an LECROY2131
		// camac to serial adapter (controller), find him!

		// Whatever it is, it has the next connection...

		hw_module *serial_interface = find_hv1445_serial_interface(controller);

		if (!serial_interface)
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for serial interface for module.\n");
			controller->_loc.print_lineno(stderr);
			fprintf(stderr,"But crate controller is not connected to any.\n");
			return NULL;
		}

		// Ok, so we found a serial interface.  We for now only support it being a
		// LeCroy 2132

		if (serial_interface->is_model_as("LECROY2132"))
		{
			controllable_module *controller = find_controllable_module(serial_interface);

			if (!controller)
			{
				serial_interface->_loc.print_lineno(stderr);
				fprintf(stderr,"The LeCroy2132 is not a controllable module.\n");
				return NULL;
			}  

			cm_module->_controller = controller;

			printf ("// MFR: %d %d : CONTROLLER@%s\n",
					cm_module->_reach._access._mfr._crate,
					cm_module->_reach._access._mfr._slot,
					cm_module->_controller->_module->_location.to_string().c_str());

			return NULL;
		}
		else
		{
			orig_module->_loc.print_lineno(stderr);
			fprintf(stderr,"Looking for serial interface for module.\n");
			serial_interface->_loc.print_lineno(stderr);
			fprintf(stderr,"Found something, but only LeCroy2132 supported.\n");
			return NULL;
		}
	}
	else if (crate->is_model_as("CAENSY1527"))
	{
		// HTT: This one has a built-in CPU.
		// TODO: Make better?
		cm_module->_flags |= CM_MAINFRAME;
		cm_module->_processor = crate;
		return NULL;
	}
	else // Deal with other crates, etc...
	{
		orig_module->_loc.print_lineno(stderr);
		fprintf(stderr,"Looking for processor for module.\n");
		crate->_loc.print_lineno(stderr);
		fprintf(stderr,"But this kind of crate is unsupported.\n");

		return NULL;
	}
}

void fix_hostname(char *fixed_hostname,
		const char *hostname)
{
	for ( ; *hostname; hostname++, fixed_hostname++)
	{
		if (*hostname == '-')
			*fixed_hostname = '_';
		else
			*fixed_hostname = *hostname;
	}
	*fixed_hostname = '\0';
}

struct compare_string_ptr
{
	bool operator()(const char *m1, const char *m2) const
	{
		//printf ("%s < %s ?",m1,m2);
		return strcmp(m1,m2) < 0;
	}
};

typedef std::set<const char*,compare_string_ptr > string_set;

// Any function with a KILL: directive (usually found in the instance)
// removes a DAQ_FCN with the same name from the list (most likely
// coming from the type declaration)

std::vector<const char*> *kill_functions(std::vector<const char*> *fcns,
		string_set *fcns_list)
{
	std::vector<const char*> *fcns_keep = 
		new std::vector<const char*>;

	for (size_t i = 0; i < fcns->size(); i++)
	{
		const char *fcn_name = (*fcns)[i];

		if (strncmp(fcn_name,"KILL:",5) == 0 ||
				strstr(fcn_name,":KILL:") != NULL)
		{
			fcn_name = fcn_name+5;

			// Find any instance of this function and remove it from the
			// list

			string_set::iterator iter = fcns_list->find(fcn_name);

			if (iter == fcns_list->end())
				fprintf(stderr,"Attempt to KILL non-existing function: %s\n",fcn_name);
			else
			{
				fcns_list->erase(iter);
			}
		}
		else
			fcns_keep->push_back(fcn_name);
	}

	delete fcns;
	return fcns_keep;
}

void generate_daq_code(hw_module *processor,const char* prefix,int module_type)
{
	const char *hostname = processor->get_setting("HOSTNAME");

	if (!hostname)
		fprintf (stderr,"No hostname for processor.\n");

	char marker[64];
	char comment[512];

	char fixed_hostname[512];

	fix_hostname(fixed_hostname,hostname);

	sprintf (marker,
			"%s_CODE_%s",prefix,fixed_hostname);
	sprintf (comment,
			"%s code for %s (as %s) (include and use the macros)",
			prefix,hostname,fixed_hostname);

	print_header(marker,comment);

	// Find out what modules are connected to this processor...
	// And get them into stable order

	cm_module_text_rcsu_compare_set modules;

	for (cm_vector::iterator i = cm_modules.begin();
			i != cm_modules.end(); ++i)
	{
		controllable_module *cm = *i;

		if (cm->_processor == processor &&
				(cm->_flags & CM_MODULE_TYPE) & module_type)
		{
			if (cm->_num_use_channels == 0)
			{
				// No channels to read out, module should be disabled

				if (cm->_module->get_control("VARNAME") == NULL)
					continue; // it also is not forced in
			}

			modules.insert(cm);
		}
	}

	printf ("\n");

	std::set<int> camac_crates;

	printf ("#define %s_DECL_%s \\\n",prefix,fixed_hostname);

	for (cm_module_text_rcsu_compare_set::iterator i = modules.begin();
			i != modules.end(); ++i)
	{
		const char *class_name = NULL;
		// const char *phys_channels = "PHYS_CH";

		controllable_module *cm = *i;

		class_name = cm->_module->_model->get_control("CLASS");

		if (!class_name)
			fprintf (stderr,"Missing CONTROL(CLASS) for %s.\n",cm->_module->_model->_name);

		cm->_name = cm->_module->get_control("VARNAME");

		if (!cm->_name)
		{
			std::string name = cm->_module->_location.to_string() + "_" + cm->_module->_model->_name;

			int len = name.length();

			char *s = new char[len+1];

			strcpy(s,name.c_str()); 

			cm->_name = s;
		}

		printf ("  static %-15s %-25s; /* %2d last, %2d signals */ \\\n",
				class_name,(*i)->_name,
				cm->_signals->one_after_last(),
				cm->_signals->num_signals());

		if (cm->_flags & CM_CAMAC)
		{
			reach_module *reach_camac = cm->single_reach(RM_CAMAC);
			if (reach_camac)
				camac_crates.insert(reach_camac->_access._camac._crate);
		}
	}

	for (std::set<int>::iterator i = camac_crates.begin();
			i != camac_crates.end(); ++i)
	{
		printf ("  static camac_crate camac_crate_%d; \\\n",*i);
	}

	printf ("  camac_address_module cam_%s[] = { \\\n",fixed_hostname);

	for (cm_module_text_rcsu_compare_set::iterator i = modules.begin();
			i != modules.end(); ++i)
	{
		const char *class_name = NULL;

		controllable_module *cm = *i;

		class_name = cm->_module->_model->get_control("CLASS");

		if (cm->_flags & CM_CAMAC)
		{
			reach_module *reach_camac = cm->single_reach(RM_CAMAC);
			if (reach_camac)
				printf ("    { %2d, %2d, &%s CLASS_NAME(%s) }, \\\n",
						reach_camac->_access._camac._crate,
						reach_camac->_access._camac._slot,
						cm->_name,class_name);
		}
	}

	printf ("  }; \\\n");

	printf ("  camac_address_crate cac_%s[] = { \\\n",fixed_hostname);

	for (std::set<int>::iterator i = camac_crates.begin();
			i != camac_crates.end(); ++i)
	{
		printf ("    { %2d, &camac_crate_%d }, \\\n",*i,*i);
	}

	printf ("  };\n"
			"\n");

	printf ("#define %s_DEFINE_HARDWARE_ADDR_%s \\\n",prefix,fixed_hostname);
	printf ("  CAMAC_CRATES_DEFINE_HARDWARE_ADDR(cac_%s); \\\n",fixed_hostname);
	printf ("  CAMAC_MODULES_DEFINE_HARDWARE_ADDR(cam_%s); \\\n",fixed_hostname);
	printf ("  ;\n");

	printf ("\n");

	printf ("#ifdef __cplusplus\n");
	printf ("#define %s_CRATE_init_%s() \\\n",prefix,fixed_hostname);
	for (std::set<int>::iterator i = camac_crates.begin();
			i != camac_crates.end(); ++i)
	{
		printf ("  camac_crate_%d.init(); \\\n",
				*i);
	}
	printf ("  ;\n");
	printf ("#else\n");
	printf ("#define %s_CRATE_init_%s() \\\n",prefix,fixed_hostname);
	for (std::set<int>::iterator i = camac_crates.begin();
			i != camac_crates.end(); ++i)
	{
		printf ("  camac_crate__init(&camac_crate_%d); \\\n",
				*i);
	}
	printf ("  ;\n");
	printf ("#endif\n");
	printf ("\n");

	// Find out what functions our modules provide

	string_set functions;
	std::map<controllable_module*,string_set*> module_functions;

	for (cm_module_text_rcsu_compare_set::iterator i = modules.begin();
			i != modules.end(); ++i)
	{
		string_set *fcns_this = new string_set;
		{
			std::vector<const char*> *fcns_ctrl = (*i)->_module->_model->get_controls("CTRL_FCN");

			if (fcns_ctrl)
			{
				fcns_this->insert(fcns_ctrl->begin(),fcns_ctrl->end());
				delete fcns_ctrl;
			}
		}
		{
			std::vector<const char*> *fcns_daq = (*i)->_module->_model->get_controls("DAQ_FCN");

			if (fcns_daq)
			{
				fcns_this->insert(fcns_daq->begin(),fcns_daq->end());
				delete fcns_daq;
			}
		}
		{
			std::vector<const char*> *fcns_ctrl = (*i)->_module->get_controls("CTRL_FCN");

			if (fcns_ctrl)
			{
				fcns_ctrl = kill_functions(fcns_ctrl,fcns_this);

				fcns_this->insert(fcns_ctrl->begin(),fcns_ctrl->end());
				delete fcns_ctrl;
			}
		}
		{
			std::vector<const char*> *fcns_daq = (*i)->_module->get_controls("DAQ_FCN");

			if (fcns_daq)
			{
				fcns_daq = kill_functions(fcns_daq,fcns_this);

				fcns_this->insert(fcns_daq->begin(),fcns_daq->end());
				delete fcns_daq;
			}
		}

		functions.insert(fcns_this->begin(),fcns_this->end());

		module_functions.insert(std::map<controllable_module*,string_set*>::value_type(*i,fcns_this));
	}

	// Now loop over all functions, and output them for the affected modules.

	for (string_set::iterator f = functions.begin();
			f != functions.end(); ++f)
	{
		const char *fcn_name = *f;
		const char *macro_name = NULL;
		char macro[64];
		const char *macro_args = "";
		const char *fcn_call   = "";
		const char *fcn_call_c = "";
		bool n_channels_selectable = false;

		int var_args_gcc = 1;

		for ( ; ; )
		{
			if (strncmp(fcn_name,"ARGS:",5) == 0)
			{
				fcn_name = fcn_name+5;
				macro_args = "...";
				fcn_call   = "__VA_ARGS__";
				fcn_call_c = ",__VA_ARGS__";

				var_args_gcc = 2;
			}  
			else if (strncmp(fcn_name,"PHASE:",6) == 0)
			{
				const char* end = strchr(fcn_name+6,':');

				if (!end)
					fprintf(stderr,"Bad PHASE:   %s\n",fcn_name);
				else
				{
					strncpy(macro,fcn_name+6,end-(fcn_name+6));
					macro[end-(fcn_name+6)] = '\0';
					macro_name = macro;
					fcn_name   = end+1;
				}
			}
			else if (strncmp(fcn_name,"N:",2) == 0)
			{
				fcn_name = fcn_name+2;
				n_channels_selectable = true;
			}  
			else
				break;
		}

		if (!macro_name)
			macro_name = fcn_name;

		if (var_args_gcc == 2)
		{
			// 2.95 do not do iso99 variadic macros
			printf ("#if defined __GNUC__ && __GNUC__ < 3\n");
		}

		for (int vag = 0; vag < var_args_gcc; vag++)
		{
			printf ("#ifdef __cplusplus\n");
			printf ("#define %s_%s_%s(%s%s) \\\n",prefix,macro_name,fixed_hostname,
					(var_args_gcc == 2 && vag == 0)?fcn_call:"",macro_args);

			for (cm_module_text_rcsu_compare_set::iterator i = modules.begin();
					i != modules.end(); ++i)
			{
				controllable_module *cm = *i;

				if (n_channels_selectable && 
						cm->_num_use_channels == 0)
					continue;

				string_set *fcns_this = module_functions.find(cm)->second;

				if (fcns_this->find(*f) != fcns_this->end())
				{	  
					const char *n_channels_select = "";
					char n_channels[64] = "";

					if (n_channels_selectable && 
							cm->_num_use_channels > 0)
					{
						n_channels_select = "_n";
						sprintf(n_channels,",%d",cm->_num_use_channels);
					}

					printf ("  %s.%s%s(%s%s); \\\n",
							cm->_name,
							fcn_name,
							n_channels_select,
							fcn_call,
							n_channels);
				}
			}

			printf ("  ;\n");
			printf ("#else /* __cplusplus */\n");
			printf ("#define %s_%s_%s(%s%s) \\\n",
					prefix,macro_name,fixed_hostname,
					(var_args_gcc == 2 && vag == 0)?fcn_call:"",macro_args);

			for (cm_module_text_rcsu_compare_set::iterator i = modules.begin();
					i != modules.end(); ++i)
			{
				controllable_module *cm = *i;

				if (n_channels_selectable && 
						cm->_num_use_channels == 0)
					continue;

				string_set *fcns_this = module_functions.find(cm)->second;

				if (fcns_this->find(*f) != fcns_this->end())
				{	  
					const char *class_name = NULL;
					const char *enforce_cast = "";

					class_name = cm->_module->_model->get_control("CLASS");

					if (strcmp(fcn_name,"init") == 0)
					{
						class_name = "camac_module";
						enforce_cast = "(camac_module *)";
					}
					else if (strcmp(fcn_name,"test") == 0 ||
							strcmp(fcn_name,"init_state") == 0)
					{
						class_name = "state_test";
						enforce_cast = "(state_test *)";
					}

					const char *n_channels_select = "";
					char n_channels[64] = "";

					if (n_channels_selectable && 
							cm->_num_use_channels > 0)
					{
						n_channels_select = "_n";
						sprintf(n_channels,",%d",cm->_num_use_channels);
					}


					printf ("  %s__%s%s(%s&%s%s%s); \\\n",
							class_name,fcn_name,
							n_channels_select,
							enforce_cast,
							cm->_name,
							fcn_call_c,
							n_channels);
				}
			}
			printf ("  ;\n");
			printf ("#endif /* __cplusplus */\n");
			if (var_args_gcc == 2 && vag == 0)
				printf ("#else /* defined __GNUC__ && __GNUC__ < 3 */\n");
		}
		if (var_args_gcc == 2)
			printf ("#endif /* defined __GNUC__ && __GNUC__ < 3 */\n");
		printf ("\n");
	}

	print_footer(marker);
}


struct land_signal_handler
{
	public:
		land_signal_handler()
		{
			_cm_module  = NULL;
			_connection = NULL;
		}


	public:
		controllable_module *_cm_module;
		connection_parts    *_connection;
};


struct land_signal
{
	public:
		land_signal()
		{
			_label = NULL;
		}

	public:
		const char *_label;   // Name of physical channel

	public:
		land_signal_handler _cfd;
		land_signal_handler _tdc;
		land_signal_handler _qdc;
		land_signal_handler _hv;

	public:
		bool operator<(const land_signal &rhs) const
		{
			return strcmp(_label,rhs._label) < 0;
		}

};

typedef std::set<land_signal*,compare_ptr<land_signal> > land_signal_set;

void generate_land_tables()
{
	// Generate signal listing (partially specific for LAND analysis code)
	// (corresponding to the LIMITS, ELECLOC, SIG_{BEAM/LAND/TDET} tables
	// (TODO: also GEOM{BEAM/LAND/TDET} should come from here.

	// LIMITS(IDENTIFIER,MIN_VAL,RANGE,NADDR,MAX_VAL)
	//
	// The contents of the LIMITS table is generally quite bastardized,
	// so it must be considered to be a candiate for manual (mis)treatment

	// ELECLOC(MOD_ID,MOD_TYPE,CRATE,SLOT,BRANCH,ACQUCON)
	//
	// MOD_ID   = SERIAL
	// MOD_TYPE = name in LIMITS table
	// BRANCH   = 0 if west or FASTBUS, 1 if cave slow control
	// ACQUCON  = CAMAC or FASTBUS or GTB or OFF (if not TDC/QDC/ADC)

	char marker[64];
	char comment[64];

	sprintf (marker,"CFG_%s","ELECLOC");
	sprintf (comment,"Land DAQ/Analysis table %s","ELECLOC");

	print_header(marker,comment);

	// Every module which is DAQ/slow control goes into the elecloc table

	printf ("\n");

	for (cm_vector::iterator i = cm_modules.begin();
			i != cm_modules.end(); ++i)
	{
		const char* acqucon = "OFF";
		const char* serial = NULL;
		const char* name   = NULL;
		char name_buffer[256];
		int crate = -1, slot = -1;
//		int crate2 = -1; // level above crate, crappy name...

		int branch = 1;

		controllable_module *cm = *i;

		// This is a dirty hack, as branch essentially is replaced by the
		// processor identifier...  Therefore, the rule becomes:  
		// TDC/QDC/ADC : branch = 0, else 1 (slow control)

		if (cm->_module->is_model_as("TDC") ||
				cm->_module->is_model_as("QDC") ||
				cm->_module->is_model_as("ADC"))
		{
			if (cm->_num_use_channels == 0)
			{
				/* If there are no signals, and the module supports
				 * disabling, do not expect to see any data.  Rather.
				 * There will be none.  It will also be killed from the
				 * readout.
				 */

				acqucon = "OFF";
			}
			else
			{
				if (cm->_flags & CM_CAMAC)
					acqucon = "CAMAC";
				else if (cm->_flags & CM_FASTBUS)
					acqucon = "FASTBUS";
				else if (cm->_flags & CM_VME)
					acqucon = "VME";
				else if (cm->_flags & CM_GTB)
					acqucon = "GTB";
				else
					acqucon = "UNKNOWN";
			}

			branch = 0;
		}

		if (cm->_flags & CM_CAMAC)
		{
			reach_module *reach_camac = cm->single_reach(RM_CAMAC);
			if (reach_camac)
			{
				crate = reach_camac->_access._camac._crate;
				slot  = reach_camac->_access._camac._slot;
			}
		}
		else if (cm->_flags & CM_GTB)
		{
			reach_module *reach_gtb = cm->single_reach(RM_GTB);
			if (reach_gtb)
			{
//				crate2 = reach_gtb->_access._gtb._sam;
				crate = reach_gtb->_access._gtb._branch; // really crate ??
				slot  = reach_gtb->_access._gtb._slave;
			}
		}
		else if (cm->_flags & CM_VME)
		{
			reach_module *reach_vme = cm->single_reach(RM_VME);
			if (reach_vme)
			{
				crate = reach_vme->_access._vme._crate;
				slot  = reach_vme->_access._vme._slot;
			}
		}
		else if (cm->_flags & CM_FASTBUS)
		{
			reach_module *reach_fastbus = cm->single_reach(RM_FASTBUS);
			if (reach_fastbus)
			{
				/* Hack for two crates, old crate (r13 is crate 1), new
				 * (Aladin), r11 is crate 2.
				 */
				if (reach_fastbus->_access._fastbus._rack == 13 &&
						reach_fastbus->_access._fastbus._crate == 2)
					crate = 1;
				else if (reach_fastbus->_access._fastbus._rack == 11 &&
						reach_fastbus->_access._fastbus._crate == 2)
					crate = 2;
				else if (reach_fastbus->_access._fastbus._rack == 105 &&
						reach_fastbus->_access._fastbus._crate == 2)
					crate = 3;
				else
				{
					fprintf(stderr,"Warning: unexpected fastbus crate (r%dc%d).\n",
							reach_fastbus->_access._fastbus._rack,
							reach_fastbus->_access._fastbus._crate);
					crate = -1;
				}

				slot  = reach_fastbus->_access._fastbus._slot;
			}
		}

		const char *virtual_slot;

		if ((virtual_slot = cm->_module->get_setting("VIRTUAL_SLOT")))
			slot = atoi(virtual_slot);

		if (cm->_module->_serial)
			serial = cm->_module->_serial->_serial;

		name = cm->_module->_model->get_control("ELECLOC_NAME");      

		if (!name)
			name = cm->_module->_model->_name;

		if (cm->_num_use_channels > 0)
		{
			sprintf(name_buffer,"%s_%dCH",name,cm->_num_use_channels);
			name = name_buffer;
		}

		/* Dirty hack.  LeCroy mainframes are NOT in ELECLOC table. */
		if (cm->_module->is_model_as("LECROY1443N"))
			continue;

		if (!serial)
			printf ("// "); // No serial number is not acceptable

		/* BL: Why is this done? Not needed in land02 */
		/* printf ("%s",crate2 != -1 ? "ELECLOC2" : "ELECLOC"); */
		printf ("ELECLOC");

		/* BL: Special handling for GTB bus modules on NeuLAND (TACQUILA) */
		if (cm->_flags & CM_GTB && strcmp(name, "TACQ_QDC2") == 0) {
			printf ("( %8s , %13s , %2d , %2d , %d , %8s ); // %2d last, %2d signals \n",
					serial, "M_TACQUILA",
					0 /* BL: Always 0 for land02 */, crate, slot, "TACQUILA",
					cm->_signals->one_after_last(),
					cm->_signals->num_signals());
		} else {
			printf ("( %8s , %13s , %2d , %2d , %d , %8s ); // %2d last, %2d signals \n",
					serial, name,
					crate, slot, branch, acqucon,
					cm->_signals->one_after_last(),
					cm->_signals->num_signals());
		}
	}

	printf ("\n");

	print_footer(marker);

	// Tables with all signals.  SIG_BEAM SIG_LAND SIG_TDET
	// The only difference is where to the signal belongs.  LAND is land,
	// TDET is the gamma detector (XB/CB) BEAM is the rest

	// For simplicity, they will be generated in one table.  Sorting is
	// basically arbitrary.  But

	// SIG_BEAM(SIG_ID,DET_ID,CFD_ID,CFD_ADDR,MFR_CRAT,MFR_SLOT,MFR_ADDR,
	//          QDC_ID,QDC_ADDR,TDC_ID,TDC_ADDR,PM_IDENT,ACTIVE,SIG_CABL,CABL_POS)

	// SIG_ID,DET_ID      signal and detector it is part of (strip last index)
	// CFD_ID,CFD_ADDR    CF module (or NIM) and channel #
	// MFR_CRAT,MFR_SLOT,MFR_ADDR  HV mainframe crate,slot,channel
	// QDC_ID,QDC_ADDR    QDC
	// TDC_ID,TDC_ADDR    TDC
	// PM_IDENT           PM identifier
	// ACTIVE             1 ??
	// SIG_CABL,CABL_POS  Cable from detector

	sprintf (marker,"CFG_%s","SIG_BEAM");
	sprintf (comment,"Land DAQ/Analysis table %s","SIG_BEAM");

	print_header(marker,comment);

	printf ("\n");

	// Since we have the reverse information (QDC, CFD and TDC), we need
	// entries from three places to fill the tables.  Let's first find
	// all data.  (Note that we cannot handle multiple converters for
	// the same signal (is this important?)).

	land_signal_set signals;

	for (cm_vector::iterator i = cm_modules.begin();
			i != cm_modules.end(); ++i)
	{
		for (connection_parts_vector::iterator s = (*i)->_signals->_signals.begin();
				s != (*i)->_signals->_signals.end(); ++s)
		{
			connection_parts *c = *s;

			// insert it into the set

			land_signal *ins = new land_signal;

			ins->_label = c->_label;

			std::pair<land_signal_set::iterator,bool> success = signals.insert(ins);

			if (!success.second)
				delete ins;

			land_signal *item = *success.first;

			if ((*i)->_module->is_model_as("TDC"))
			{
				if (item->_tdc._cm_module)
					fprintf(stderr,"Warning: multiple TDCs for %s.\n",item->_label);

				item->_tdc._cm_module  = *i;
				item->_tdc._connection = *s;
			}
			else if ((*i)->_module->is_model_as("QDC") ||
					(*i)->_module->is_model_as("ADC"))
			{
				if (item->_qdc._cm_module)
					fprintf(stderr,"Warning: multiple A/QDCs for %s.\n",item->_label);

				item->_qdc._cm_module  = *i;
				item->_qdc._connection = *s;
			}
			else if ((*i)->_module->is_model_as("CFD"))
			{
				if (item->_cfd._cm_module)
					fprintf(stderr,"Warning: multiple CFDs for %s.\n",item->_label);

				item->_cfd._cm_module  = *i;
				item->_cfd._connection = *s;
			}
			else if ((*i)->_module->is_model_as("HV"))
			{
				if (item->_hv._cm_module)
					fprintf(stderr,"Warning: multiple HVs for %s.\n",item->_label);

				item->_hv._cm_module  = *i;
				item->_hv._connection = *s;
			}
		}
	}

	for (land_signal_set::iterator i = signals.begin();
			i != signals.end(); ++i)
	{
		land_signal *item = *i;

		if (item->_hv._cm_module ||
				item->_cfd._cm_module ||
				item->_qdc._cm_module ||
				item->_tdc._cm_module)
		{
			const char *label = item->_label;
			char det[64];

			strcpy(det,item->_label);

			char *p = strrchr(det,'_');
			if (p)
				*p = '\0';

			printf ("SIG_BEAM( %8s , %8s ,",label,det);

			// CFD

			const char *cfd = "NIM";
			int        ncfd = -1;

			if (item->_cfd._cm_module)
			{
				if (item->_cfd._cm_module->_module->_serial)
					cfd = item->_cfd._cm_module->_module->_serial->_serial;
				ncfd = item->_cfd._connection->_channel;
			}
			printf (" %8s , %2d ,",cfd,ncfd);	      

			// HV

			int hv_cr = -1, hv_sl = -1, hv_ch = -1;

			if (item->_hv._cm_module)
			{
				if (item->_hv._cm_module->_flags & CM_CAMAC)
				{
					reach_module *reach_camac = item->_hv._cm_module->single_reach(RM_CAMAC);
					if (reach_camac)
					{
						hv_cr = reach_camac->_access._camac._crate;
						hv_sl = reach_camac->_access._camac._slot;
					}
				}
				else if (item->_hv._cm_module->_module->is_model_as("LECROY1443N"))
				{
					// special handling for this one.  

					hv_cr = item->_hv._cm_module->_module->_location._crate;
					hv_sl = item->_hv._cm_module->_module->_location._slot;
				}
				else if (item->_hv._cm_module->_module->is_model_as("CAENA1733"))
				{
					// HTT: More special handling I guess...
					hv_cr = item->_hv._cm_module->_module->_location._crate;
					hv_sl = item->_hv._cm_module->_module->_location._slot;
				}
				else
				{
					fprintf (stderr,"Unknown HV supply type.\n");
				}
				hv_ch = item->_hv._connection->_channel;
			}

			printf (" %2d , %2d , %2d ,",hv_cr,hv_sl,hv_ch);	      

			// QDC

			const char *qdc = "NONE";
			int        nqdc = -1;

			if (item->_qdc._cm_module)
			{
				if (item->_qdc._cm_module->_module->_serial)
					qdc = item->_qdc._cm_module->_module->_serial->_serial;
				nqdc = item->_qdc._connection->_channel;
			}
			printf (" %8s , %2d ,",qdc,nqdc);	      

			// TDC

			const char *tdc = "NONE";
			int        ntdc = -1;

			if (item->_tdc._cm_module)
			{
				if (item->_tdc._cm_module->_module->_serial)
					tdc = item->_tdc._cm_module->_module->_serial->_serial;
				ntdc = item->_tdc._connection->_channel;
			}
			printf (" %8s , %2d ,",tdc,ntdc);	      

			printf (" PM_IDENT ,");
			printf (" 1 ,");
			printf (" CABLE ,");
			printf (" -1 ");

			printf (");\n");
		}
	}

	printf ("\n");

	print_footer(marker);
}

typedef std::map<int,const char *> map_channel_name;

void generate_scalers_tables()
{
	// Generate listing of which scaler channel is what signal

	char marker[64];
	char comment[64];

	sprintf (marker,"SCALER_LISTING");
	sprintf (comment,"Land DAQ/Analysis table %s","SCALER_LISTING");

	print_header(marker,comment);

	printf ("\n");

	for (cm_vector::iterator i = cm_modules.begin();
			i != cm_modules.end(); ++i)
	{
		controllable_module *cm = *i;

		if (cm->_module->is_model_as("SCALER"))
		{
			int branch  = -1;
			int crate   = -1;
			int slot    = -1;

			if (cm->_flags & CM_CAMAC)
			{
				reach_module *reach_camac = cm->single_reach(RM_CAMAC);
				if (reach_camac)
				{
					crate = reach_camac->_access._camac._crate;
					slot  = reach_camac->_access._camac._slot;
				}
			}
			else if (cm->_flags & CM_VME)
			{
				reach_module *reach_vme = cm->single_reach(RM_VME);
				if (reach_vme)
				{
					crate = reach_vme->_access._vme._crate;
					slot  = reach_vme->_access._vme._slot;
				}
			}

			const char *virtual_slot;

			if ((virtual_slot = cm->_module->get_setting("VIRTUAL_SLOT")))
				slot = atoi(virtual_slot);

			map_channel_name ch_name;

			for (connection_parts_vector::iterator s = cm->_signals->_signals.begin();
					s != cm->_signals->_signals.end(); ++s)
			{
				connection_parts *c = *s;

				int channel = c->_channel;

				const char *name = c->_label;

				std::pair<map_channel_name::iterator, bool> success =
					ch_name.insert(map_channel_name::value_type(channel,name));

				if (!success.second)
				{
					fprintf(stderr,
							"Several names for SCALER_CH(%2d,%2d,%2d,%2d): "
							"%s, %s\n",
							branch,crate,slot,channel,
							success.first->second,name);
				}
			}

			for (map_channel_name::iterator c = ch_name.begin();
					c != ch_name.end(); ++c)
			{
				printf ("SCALER_CH(%2d,%2d,%2d,%2d,\"%s\") \\\n",
						branch,crate,slot,c->first,c->second);
			}
		}
	}

	printf ("\n");

	print_footer(marker);
}

void generate_code_and_tables()
{
	// For the DAQ code, we are primarily interested in the modules that
	// a DAQ can talk to, namely TDCs, ADCs, QDCs, and SCALERS

	// As the documentation may contain several branches of the DAQ, we
	// enumerate them based on the processor which is actually connected
	// to the crates in question.

	// An unfortunate side-effect of this is that the program somehow
	// have to follow crate controllers etc to find out where things are
	// connected.  On the other hand, someone has to find out the crate
	// numbers, and this is the place to do it.



	// Let's first find all modules (modules without controlling
	// processors give warnings, as if they were forgotten)

	// In the same sense, we will also enumerate all detectors, and
	// those without readout will give warnings.

	find_controllable_modules("DAQ_EVENTWISE",CM_DAQ_EVENTWISE);
	find_controllable_modules("DAQ_MONITOR",CM_DAQ_MONITOR);
	find_controllable_modules("SLOW_CONTROL",CM_SLOW_CONTROL);

	module_text_rcsu_compare_set processors;

	for (cm_vector::iterator i = cm_modules.begin();
			i != cm_modules.end(); ++i)
	{
		// For each module, we want to know what processor it belongs to
		// (along with addressing information)

		// printf ("%s \n",i->_module->_location.to_string().c_str());
		find_processor(*i);

		if ((*i)->_processor)
			processors.insert((*i)->_processor);

		// For each module, we want to know what signals are connected

		find_signals(*i);
	}

	// Now that the information is collected, it's about time to dump the information
	// We'd really prefer if the output always come out in the same order, so that
	// diff'ing it produces minimal changes.  Therefore sort on names or other
	// similar things.  rcsu is another good candidate

	// Generate code to be included in DAQ programs

	for (module_text_rcsu_compare_set::iterator i = processors.begin();
			i != processors.end(); ++i)
	{
		generate_daq_code(*i,"DAQ",CM_DAQ_EVENTWISE | CM_DAQ_MONITOR);
		generate_daq_code(*i,"SLOW_CTRL",CM_SLOW_CONTROL);
	}

	// Generate signal listing (partially specific for LAND analysis code)
	// (corresponding to the LIMITS, ELECLOC, SIG_{BEAM/LAND/TDET} tables
	// (TODO: also GEOM{BEAM/LAND/TDET} should come from here.

	generate_land_tables();

	generate_scalers_tables();

	// Generate a listing of all slow control parameters
	generate_setup_table();

	/*
	   for (module_vector::iterator i = vector_tdc.begin();
	   i != vector_tdc.end(); ++i)
	   {
	   printf ("TDC: %s\n",(*i)->_location.to_string().c_str());
	   }
	 */
}



/*
   const struct index_label label[] = {
   { 5, "Pos2"     },
   { 6,"Pixel"    },
   { 7,"TOF"},
   { 8,"LAND"   },
   {12,"CsI"},
   {13, "GFI1"        },
   {14,"GFI2"        },
   {15,"GFI3"     },
   {18,"MW3"      },
   {19,"MW4"   },
   {22,"Seetram"  },
   {23,"Clock" },
   {24,"Trig1"  },
   {25, "Trig2"    },
   {26,"Trig3"    },
   {27,"Trig4" },
   {28,"Trig5"  },
   {29, "Trig6"    },
   {30,"TrigOR1"        },
   {31,"TrigOR2"     },
   };
   SCALER_CH(-1, 2,14, 0,"ROL01_01");
   SCALER_CH(-1, 2,14, 1,"ROL01_02");
   SCALER_CH(-1, 2,14, 2,"ROL01_03");
   SCALER_CH(-1, 2,14, 3,"ROL01_04");
   SCALER_CH(-1, 2,14, 4,"POS1");
   SCALER_CH(-1, 2,14, 5,"POS1");
   SCALER_CH(-1, 2,14, 9,"PSP01_05");
   SCALER_CH(-1, 2,14,10,"PSP02_05");
   SCALER_CH(-1, 2,14,11,"PSP03_05");
   SCALER_CH(-1, 2,14,13,"PIX1_01"); ?
   SCALER_CH(-1, 2,14,14,"PIX2_01"); ?
   SCALER_CH(-1, 2,14,15,"PIX3_01"); ?
   SCALER_CH(-1, 2,14,16,"ZST1_5");
   SCALER_CH(-1, 2,14,17,"ZST2_5");
   SCALER_CH(-1, 2,14,20,"S2");
   SCALER_CH(-1, 2,14,21,"S8");
 */
