/* This file contains the grammar for a cabling documentation file file.
 *
 * With the help of bison(yacc), a C parser is produced.
 *
 * The lexer.lex produces the tokens.
 *
 * Don't be afraid.  Just look below and you will figure
 * out what is going on.
 */


%{
#define YYERROR_VERBOSE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "cable_node.hh"
#include "file_line.hh"
#include "name_map.hh"

int yylex(void);

void yyerror(const char *s);

void parse_calib();

extern int yylineno;

struct md_ident_fl
{
 public:
  md_ident_fl(const char *id,const file_line &loc)
  {
    _id   = id;
    _loc = loc;
  }

 public:
  const char *_id;
  file_line   _loc;
};

#define CURR_FILE_LINE file_line(yylineno)

%}

%union {
  // What we get from the lexer:

  double  fValue;              /* double value */
  int     iValue;              /* integer value */
  rcsu    rcsuValue;           /* rack,crate,slot,unit */
  rcsu_range rcsuRangeValue;   /* rack,crate,slot,unit */
  const char *strValue;        /* string */

  // We generate internally:

  cabling_node         *nPtr;       /* node pointer */
  cabling_node_list    *nodes;      /* list of nodes */

  hardware_def         *hw_def;
  hardware_attr        *hw_attr;
  hardware_attr_list   *hw_attr_list;

  hw_module            *module;
  hw_module_part_list  *md_part_list;
  hw_module_part       *md_part;
  hw_connection        *md_connection;

  md_ident_fl          *md_idfl;

  cable_marker         *cb_mark;

  std::vector<const char*> *string_list;
};

/* Things we get from the lexer */

%token <iValue>    INTEGER
%token <fValue>    DOUBLE
%token <rcsuValue> RCSU
%token <rcsuValue> PARTIAL_RCSU
%token <rcsuRangeValue> RCSU_RANGE
%token <rcsuRangeValue> PARTIAL_RCSU_RANGE
%token <strValue>  STRING
%token <strValue>  IDENTIFIER
%token LT_RANGE
%token RACK
%token DET
%token CRATE
%token MODULE
%token UNIT
%token DEVICE
%token SETTING
%token HAS_SETTING
%token ECL_INPUT
%token ECL_OUTPUT
%token LEMO_INPUT
%token LEMO_OUTPUT
%token SMA_INPUT
%token SMA_OUTPUT
%token CONNECTOR
%token SIGNAL
%token TENSION
%token BROKEN
%token LABEL
%token SERIAL
%token NAME_MAP
%token NAME
%token CONTROL
%token MODEL_AS
%token HANDLER
%token TITLE
%token SETTING_TO
%token CABLE_TO
%token CABLE_FROM

/* Operands for simple calculations */

%left '+' '-'
%left '*' '/'
%nonassoc UMINUS

/* The statements (nodes) */

%type <nPtr> stmt
%type <nodes> stmt_list

/* A vector of doubles */

/*%type <vect_d> vector_items*/
/*%type <vect_d> vector*/
/*%type <fValue> value*/

/* Compounds(?) */

%type <hw_def> hardware_definition
%type <hw_attr_list> hardware_attr_list
%type <hw_attr> hardware_attr
%type <hw_attr> connector_input
%type <hw_attr> connector_output
%type <hw_attr> connector_ecl_input
%type <hw_attr> connector_ecl_output
%type <hw_attr> connector_lemo_input
%type <hw_attr> connector_lemo_output
%type <hw_attr> connector_sma_input
%type <hw_attr> connector_sma_output
%type <hw_attr> connector_bidi
%type <iValue> connector_info_list
%type <iValue> connector_info
%type <hw_def> hardware_decl
%type <hw_def> hardware_decl_crate
%type <hw_def> hardware_decl_module
%type <hw_def> hardware_decl_unit
%type <hw_def> hardware_decl_device

%type <module>        module_definition
%type <module>        module_decl
%type <md_part_list>  module_stmt_list
%type <md_part>       module_stmt
%type <md_part>       module_setting
%type <md_part>       module_connection
%type <md_connection> md_cnct_other
%type <md_idfl>       md_ident
%type <strValue>      md_label
%type <rcsuRangeValue> part_rcsu_range
%type <cb_mark>       cable_marker
%type <strValue>      cm_string

%type <string_list>   string_list

%%

/* This specifies an entire file */
program:
          stmt_list             { append_nodes(&all_cable_nodes,$1); }
        | /* NULL */
        ;

stmt_list:
          stmt                  { $$ = append_node(NULL,$1); }
        | stmt_list stmt        { $$ = append_node($1,$2); }
        ;

/* Each statement is either an range specification or a parameter specification. */
stmt:
          ';'                                      { $$ = NULL; }
/*        | '{' stmt_list '}'                        { $$ = node_list_pack($2); }*/
        | hardware_definition                      { append_hardware($1); $$ = NULL; }
        | map_definition                           { $$ = NULL; }
        | module_definition                        { $$ = $1; }
/* | LT_RANGE '(' STRING ',' STRING ')' stmt  { $$ = lt_range($3,$5,append_node(NULL,$7)); } */
        ;

/* A parameter has a name, a detector, values and errors. */
/* param_spec:
   ; */

/* A definition of what some hardware can do (inputs/outputs). */
hardware_definition:
	  hardware_decl '{' hardware_attr_list '}' { $1->apply($3); $$ = $1; }
        ;

hardware_attr_list:
          hardware_attr                            { $$ = append_node(NULL,$1); }
        | hardware_attr_list hardware_attr         { $$ = append_node($1,$2); }
        ;

hardware_attr:
	  connector_input                          { $$ = $1; }
	| connector_output                         { $$ = $1; }
	| connector_bidi                           { $$ = $1; }
	| HAS_SETTING '(' STRING SETTING_TO string_list ')' ';' { $$ = new has_setting($3,$5); }
        | LABEL '(' STRING ')' ';'                 { $$ = new ha_label($3); }
        | NAME '(' STRING ')' ';'                  { $$ = new ha_name($3); }
        | CONTROL '(' STRING ',' STRING ')' ';'    { $$ = new ha_control($3,$5); }
        | MODEL_AS '(' IDENTIFIER ')' ';'
          {
	    hardware_def *parent = find_hardware_def($3);

	    if (!parent)
	      {
		print_lineno(stderr,yylineno);
		fprintf(stderr,"Unknown hardware model: %s\n",$3/*->c_str()*/);
		exit(1);
	      }

            $$ = new ha_model_as(parent);
          }
        | HANDLER '(' STRING ',' STRING ')' ';' { $$ = new_ha_handler($3,$5); }
        ;

connector_input:
	  connector_ecl_input
	| connector_lemo_input
	| connector_sma_input

connector_output:
	  connector_ecl_output
	| connector_lemo_output
	| connector_sma_output

connector_ecl_input:
	  ECL_INPUT  '(' IDENTIFIER ')' ';' { $$ = new ecl_input($3,0); }
	| ECL_INPUT  '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ = new
ecl_input($3,$5); }
        ;

connector_ecl_output:
	  ECL_OUTPUT '(' IDENTIFIER ')' ';' { $$ = new ecl_output($3,0); }
	| ECL_OUTPUT '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ = new
ecl_output($3,$5); }
        ;

connector_lemo_input:
	  LEMO_INPUT  '(' IDENTIFIER ')' ';' { $$ = new lemo_input($3,0); }
	| LEMO_INPUT  '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ =
new lemo_input($3,$5); }
        ;

connector_lemo_output:
	  LEMO_OUTPUT '(' IDENTIFIER ')' ';' { $$ = new lemo_output($3,0); }
	| LEMO_OUTPUT '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ =
new lemo_output($3,$5); }
        ;

connector_sma_input:
	  SMA_INPUT  '(' IDENTIFIER ')' ';' { $$ = new sma_input($3,0); }
	| SMA_INPUT  '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ =
new sma_input($3,$5); }
        ;

connector_sma_output:
	  SMA_OUTPUT '(' IDENTIFIER ')' ';' { $$ = new sma_output($3,0); }
	| SMA_OUTPUT '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ =
new sma_output($3,$5); }
        ;

connector_bidi:
	  CONNECTOR '(' IDENTIFIER ')' ';'         { $$ = new bidi_connector($3,0); }
	| CONNECTOR '(' IDENTIFIER ',' connector_info_list ')' ';' { $$ = new bidi_connector($3,$5); }
        ;

connector_info_list:
          connector_info                           { $$ = $1;      }
        | connector_info_list '|' connector_info   { $$ = $1 | $3; }
        ;

connector_info:
	  SIGNAL                                   { $$ = CI_SIGNAL; }
        | TENSION                                  { $$ = CI_TENSION; }
	;

string_list:
	  STRING                                   { $$ = append_node(NULL,$1); }
        | string_list ',' STRING                   { $$ = append_node($1,$3); }
	;

hardware_decl:
          hardware_decl_crate    { $$ = $1; }
        | hardware_decl_module   { $$ = $1; }
        | hardware_decl_unit     { $$ = $1; }
        | hardware_decl_device   { $$ = $1; }
	;

hardware_decl_crate:
	RACK '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_RACK,$3); }
        ;

hardware_decl_crate:
	DET '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_DET,$3); }
        ;

hardware_decl_crate:
	CRATE '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_CRATE,$3); }
        ;

hardware_decl_module:
	MODULE '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_MODULE,$3); }
        ;

hardware_decl_unit:
	UNIT '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_UNIT,$3); }
        ;

hardware_decl_device:
	DEVICE '(' IDENTIFIER ')' { $$ = new_hardware_def(CURR_FILE_LINE,HW_DEVICE,$3); }
        ;

/* Maps all matching strings on output. */
map_definition:
        NAME_MAP '(' STRING ',' STRING ')'
          {
	    name_map_add($3, $5);
          }
	;

/* A definition of one piece of hardware. */
module_definition:
	module_decl '{' module_stmt_list '}' { $1->apply($3); $$ = $1; }
        ;

module_decl:
	IDENTIFIER '(' RCSU ')'
          {
	    hardware_def *model = find_hardware_def($1);

	    if (!model)
	      {
		print_lineno(stderr,yylineno);
		fprintf(stderr,"Unknown hardware model: %s\n",$1/*->c_str()*/);
		//#ifdef YYBISON
		fprintf(stderr,"Warning: %s RCSU: l%d,c%d-l%d,c%d\n",
			$1/*->c_str()*/,
			@3.first_line, @3.first_column,
			@3.last_line,  @3.last_column);
		//#endif
		exit(1);
	      }
	    if (!model->check_rcsu_level($3))
	      {
		print_lineno(stderr,yylineno);
		fprintf(stderr,"Wrong rcsu level for hardware: %s\n",$1/*->c_str()*/);
	      }

	    hw_module *module = new hw_module(model,$3);

	    model->_instances.push_back(module);

	    module->_loc = CURR_FILE_LINE;

            $$ = module;
          }
        ;

module_stmt_list:
          module_stmt                    { $$ = append_node(NULL,$1); }
        | module_stmt_list module_stmt   { $$ = append_node($1,$2); }
        ;

module_stmt:
          ';'                                      { $$ = NULL; }
        | LABEL '(' STRING ')' ';'                 { $$ = new hw_label($3); }
        | SERIAL '(' STRING ')' ';'                { $$ = new hw_serial($3); }
        | BROKEN ';'                               { $$ = new hw_broken(); }
        | module_setting ';'                       { $$ = $1; }
        | module_connection ';'                    { $$ = $1; }
        | CONTROL '(' STRING ',' STRING ')' ';'    { $$ = new hw_control($3,$5); }
        | IDENTIFIER ':' ';'                       { $$ = NULL; } /* unconnected cable (ignored) */
        ;

module_setting:
	SETTING '(' STRING SETTING_TO/*=>*/ STRING ')'      { hw_setting *setting = new hw_setting($3,$5); setting->_loc = CURR_FILE_LINE; $$ = setting; }
        ;

module_connection:
          md_ident                               md_cnct_other { $2->_loc = $1->_loc; $2->set_cnctr($1->_id); $$ = $2; delete $1; }
        | md_ident md_label                      md_cnct_other { $3->_loc = $1->_loc; $3->set_cnctr($1->_id); $3->set_label($2); $$ = $3; delete $1; }
        | md_ident              cable_marker ',' md_cnct_other { $4->_loc = $1->_loc; $4->set_cnctr($1->_id); $4->set_marker($2); $$ = $4; delete $1; }
        | md_ident md_label     cable_marker ',' md_cnct_other { $5->_loc = $1->_loc; $5->set_cnctr($1->_id); $5->set_label($2); $5->set_marker($3); $$ = $5; delete $1; }
        | md_ident md_label                                    { $$ = NULL; }
        | md_ident BROKEN                                    { hw_module_part *part = new hw_conn_broken($1->_id); part->_loc = $1->_loc; delete $1; $$ = part; }
	;

md_ident:
	IDENTIFIER ':' { $$ = new md_ident_fl($1,CURR_FILE_LINE); }
	;

md_label:
	LABEL '(' STRING ')'                   { $$ = $3; }
        ;

md_cnct_other:
	  part_rcsu_range '/' IDENTIFIER                    { $$ = new hw_connection($1,$3);                   }
        | part_rcsu_range '(' IDENTIFIER ')' '/' IDENTIFIER { $$ = new hw_connection($1,$6); $$->set_type($3); }
        ;

part_rcsu_range:
	  RCSU                                   { $$ = to_rcsu_range($1); }
	| PARTIAL_RCSU                           { $$ = to_rcsu_range($1); }
	| RCSU_RANGE                             { $$ = $1; }
	| PARTIAL_RCSU_RANGE                     { $$ = $1; }
	;

//cable_marker:
//	  STRING CABLE_TO/*->*/ STRING   { $$ = new cable_marker_to($1,$3); }
//        |        CABLE_TO/*->*/ STRING   { $$ = new cable_marker_to(NULL,$2); }
//        | STRING CABLE_TO/*->*/          { $$ = new cable_marker_to($1,NULL); }
//        | STRING CABLE_FROM/*<-*/ STRING { $$ = new cable_marker_from($1,$3); }
//        |        CABLE_FROM/*<-*/ STRING { $$ = new cable_marker_from(NULL,$2); }
//        | STRING CABLE_FROM/*<-*/        { $$ = new cable_marker_from($1,NULL); }
//	;

cable_marker:
	  cm_string CABLE_TO/*->*/ cm_string   { $$ = new cable_marker_to($1,$3); }
        | cm_string CABLE_FROM/*<-*/ cm_string { $$ = new cable_marker_from($1,$3); }
	;

cm_string:
	  STRING                         { $$ = $1; }
        |                                { $$ = NULL; }
	;



/* A vector of floating point values */
/*
vector:
          '(' vector_items ')'     { $$ = $2; }
	;

vector_items:
	  value                  { $$ = append_vector(NULL,$1); }
        | vector_items ',' value { $$ = append_vector($1,$3); }
        ;
*/
/* Floating point values.  Simple calculations allowed. */
/*
value:
          DOUBLE                  { $$ = $1; }
        | '-' value %prec UMINUS  { $$ = -$2; }
        | value '+' value         { $$ = $1 + $3; }
        | value '-' value         { $$ = $1 - $3; }
        | value '*' value         { $$ = $1 * $3; }
        | value '/' value
          {
	    if (!$3)
#ifdef YYBISON
	      fprintf(stderr,"Warning: Division by zero, l%d,c%d-l%d,c%d",
		      @3.first_line, @3.first_column,
		      @3.last_line,  @3.last_column);
#endif
	    $$ = $1 / $3;
	  }
        | '(' value ')'           { $$ = $2; }
        ;
*/
%%

void yyerror(const char *s) {
  print_lineno(stderr,yylineno);
  fprintf(stderr," %s\n", s);
/*
  Current.first_line   = Rhs[1].first_line;
  Current.first_column = Rhs[1].first_column;
  Current.last_line    = Rhs[N].last_line;
  Current.last_column  = Rhs[N].last_column;
*/
}

bool parse_cables()
{
  yylloc.first_line = yylloc.last_line = 1;
  yylloc.first_column = yylloc.last_column = 0;

  return yyparse() == 0;
}
