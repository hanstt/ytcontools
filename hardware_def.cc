
#include "hardware_def.hh"

#include "str_set.hh"
#include "util.hh"

#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <string>

hardware_attr_list *append_node(hardware_attr_list *v,
				hardware_attr *a)
{
  if (!v)
    v = new hardware_attr_list;

  if (a)
    v->push_back(a);
  return v;
}

hardware_def *new_hardware_def(file_line loc,
			       hardware_type hw_type,
			       const char *name)
{
  hardware_def *def = new hardware_def;

  def->_loc  = loc;
  def->_hw_type = hw_type;
  def->_name = name;
  
  return def;
}

#include <regex.h>

class expand_regexps
{
  public:
    expand_regexps()
    {
      do_regcomp(&_digits,"^(.*[^0-9])([0-9]+)_([0-9]+)$");
      do_regcomp(&_letter,"^(.*[^0-9])([A-Z])_([A-Z])$");
      do_regcomp(&_digits_letter,"^(.*[^0-9])([0-9]+)([a-zA-Z])_([0-9]+)([a-zA-Z])$");
    }
  
    ~expand_regexps()
    {
      regfree(&_digits);
      regfree(&_letter);
      regfree(&_digits_letter);
    }
  
  public:
    void do_regcomp(regex_t *preg, const char *regex)
    {
      int ret = regcomp(preg,regex,REG_EXTENDED);
  
      if (ret != 0)
        {
          int len = regerror(ret,preg,NULL,0);
          char *str = new char [len];
          regerror(ret,preg,str,len);
          fprintf(stderr, "Regex compilation failure!  (Error: %s)  "
              "(Internal shit happened, clean up (the code) :-) )", str);
          delete [] str;
        }
    }

  public:
    regex_t _digits;
    regex_t _letter;
    regex_t _digits_letter;
};

expand_regexps expanders;

#define countof(x) ((sizeof(x))/(sizeof(x[0])))

std::vector<std::string>* expand_string(const char *label,
					bool allow_reversed,
					std::vector<int> *levels)
{
  const char *str = label;
  regmatch_t pe[6];

  int start_digit = -1;
  int end_digit   = -1;

  char start_letter = '\0';
  char end_letter   = '\0';

  if (regexec(&expanders._digits,str,countof(pe),pe,0) == 0)
    {
      start_digit = strtol(str+pe[2].rm_so,NULL,10);
      end_digit   = strtol(str+pe[3].rm_so,NULL,10);
    }
  else if (regexec(&expanders._letter,str,countof(pe),pe,0) == 0)
    {
      start_letter = str[pe[2].rm_so];
      end_letter   = str[pe[3].rm_so];
    }
  else if (regexec(&expanders._digits_letter,str,countof(pe),pe,0) == 0)
    {
      start_digit = strtol(str+pe[2].rm_so,NULL,10);
      end_digit   = strtol(str+pe[4].rm_so,NULL,10);

      start_letter = str[pe[3].rm_so];
      end_letter   = str[pe[5].rm_so];
    }
  else
    return NULL; // no match

  bool digit_reversed = false;

  if (start_digit > end_digit)
    {
      if (allow_reversed)
	{
	  digit_reversed = true;
	  swap(start_digit,end_digit);
	}
      else
	{
	  fprintf (stderr,"Cannot expand %s, reverse order (%d>%d).\n",
		   label,
		   start_digit,end_digit);
	  return NULL;
	}
    }
    
  if (start_letter > end_letter)
    {
      fprintf (stderr,"Cannot expand %s, reverse order (%c>%c).\n",
	       label,
	       start_letter,end_letter);
      return NULL;
    }
    

  // match 0 is the entire string.
  // match 1 is the prefix
  
  std::string prefix(str+pe[1].rm_so,pe[1].rm_eo); 

  // We are to expand
  
  std::vector<std::string> *items = new std::vector<std::string>;
  /*
  printf ("Expanding %s -> %s (%d -> %d) (%c -> %c)\n",
	  label.c_str(),prefix.c_str(),
	  start_digit,end_digit,
	  start_letter,end_letter);
  fflush(stdout);
  */
  if (start_digit == -1)
    {
      if (levels)
	levels->push_back(end_letter-start_letter+1);
      for (char letter = start_letter; letter <= end_letter; letter++)
	items->push_back(prefix + letter);
    }
  else
    {
      if (levels)
	{
	  levels->push_back(end_digit-start_digit+1);
	  if (start_letter != '\0')
	    levels->push_back(end_letter-start_letter+1);
	}
      for (int digit = start_digit; digit <= end_digit; digit++)
	{
	  char digit_str[16];
	  
	  sprintf (digit_str,"%d",
		   digit_reversed ? end_digit - (digit - start_digit) : digit);
	  
	  if (start_letter == '\0')
	    items->push_back(prefix + digit_str);
	  else
	    for (char letter = start_letter; letter <= end_letter; letter++)
	      items->push_back(prefix + digit_str + letter);
	}
    }

  //std::vector<std::string>::iterator i;
  //for (i = items->begin(); i != items->end(); ++i)
  //  printf ("/%s/",i/*->c_str()*/);
  //printf ("\n");

  return items;
}

std::vector<std::string> *expand_string_one(const char *label,
					    bool allow_reversed,
					    std::vector<int> *levels)
{
  std::vector<std::string> *ret = NULL;

  ret = expand_string(label,allow_reversed,levels);

  if (!ret)
    {
      ret = new std::vector<std::string>;
      ret->push_back(label);
    }
  return ret;
}

bool connector_attr::expand(hardware_attr_list* attr,
			    hardware_attr_list::iterator& insert_after)
{
  // If an item is short for several ones, 
  // then expand it.  
  // 
  // in1_5 => in1, in2, in3, in4, in5
  // inA_C => inA, inB, inC
  //
  // Halfway special case:
  //
  // in1a_2b => in1a, in1b, in2a, in2b
  //
  // We take this here, such that all other logic of the
  // program can operate on single connectors.

  std::vector<std::string>* items = expand_string(_label);

  if (!items)
    return false;

  std::vector<std::string>::iterator i;

  hardware_attr_list::iterator insert_before = insert_after;
  ++insert_before;

  for (i = items->begin(); i != items->end(); ++i)
    {
      connector_attr *item = create_copy();

      item->_label = find_str_identifiers(i->c_str());

      // Insert the item after the current position

      //printf ("/%s/",i/*->c_str()*/);

      attr->insert(insert_before,item);
    }

  //printf ("\n");

  delete items;

  return true;
}

void has_setting::dump() 
{
  printf ("  HAS_SETTING(\"%s\" => ",_name/*->c_str()*/); 

  if (_values)
    {
      std::vector<const char *>::iterator i;
      i = _values->begin();
      if (i != _values->end())
	{
	  printf ("\"%s\"",(*i)/*->c_str()*/);
	  ++i;
	}
      for ( ; i != _values->end(); ++i)
	{
	  printf (",\"%s\"",(*i)/*->c_str()*/);
	}
    }
  printf (");\n"); 
}

void ha_model_as::dump()
{
  printf ("  MODEL_AS(\"%s\");\n",_parent->_name/*->c_str()*/); 
}

hardware_def::hardware_def()
{
  _label    = NULL;
  _handler  = NULL;
  _name_lit = NULL;
}
/*
void dump(hardware_attr_list *attr)
{
  hardware_attr_list::iterator i;

  for (i = attr->begin(); i != attr->end(); ++i)
    { 
      hardware_attr *a = *i;
      
      printf ("|");
  
      connector_attr *connector = dynamic_cast<connector_attr*>(a);
      if (connector)
	{
	  printf ("%s",connector->_label.c_str());
	}
    }
	
  printf ("\n");
}
*/
void hardware_def::apply(hardware_attr_list *attr)
{
  hardware_attr_list::iterator i;

  //printf ("Applying %d items.\n",attr->size());
  //::dump(attr);
	    
  for (i = attr->begin(); i != attr->end(); ++i)
    {
      hardware_attr *a = *i;

      // First expand connectors if necessary.
      { 
	connector_attr *connector = dynamic_cast<connector_attr*>(a);                                
	if (connector && connector->expand(attr,i))
	  {
	    //printf ("Expanded -> %d items.\n",attr->size());
	    //::dump(attr);
	    // The expanded entries were added to the list,
	    // delete this one, as it will not be added.
	    delete connector;
	    continue;
	  }
      }

      // TODO: check that connector / setting does not collide with anything
      // in model_as

      APPLY_MULTIPLE_INSTANCE(connector_attr,connector,_label,_connectors,connector_attr_map::value_type);
      APPLY_MULTIPLE_INSTANCE(has_setting,setting,_name,_settings,has_settings_map::value_type);

      { 
	ha_control *local = dynamic_cast<ha_control*>(a);                                
	if (local)                                                             
	  {                                                                    
	    _controls.insert(local);             
	    continue;                                                          
	  } 
      }

      { 
	ha_model_as *model_as = dynamic_cast<ha_model_as*>(a);                                

	if (model_as)
	  {
	    if (_hw_type != model_as->_parent->_hw_type &&
		model_as->_parent->_hw_type != HW_DEVICE)
	      {
		_loc.print_lineno(stderr);                     
		fprintf(stderr,"%s not same hardware level as MODEL_AS(%s), "
			"and MODEL_AS not generiv DEVICE.\n",
			_name/*->c_str()*/,
			model_as->_parent->_name/*->c_str()*/);   
		delete model_as;
		continue;
	      }

	    _model_as.push_back(model_as);

	    model_as->_parent->_modelled_as.push_back(this);
	  }
      }

      APPLY_SINGLE_INSTANCE(ha_label,   label,   _label);
      APPLY_SINGLE_INSTANCE(ha_handler, handler, _handler);
      APPLY_SINGLE_INSTANCE(ha_name,    name_lit,_name_lit);
    }

  delete attr; // The objects have all been taken  
}

bool hardware_def::check_rcsu_level(rcsu& rcsu)
{
  if (rcsu._rack == -1)
    return false;
  switch (_hw_type)
    {
    case HW_RACK:
      return rcsu._crate == -1 && rcsu._slot == -1 && rcsu._unit == -1;
    case HW_DET:
      return rcsu._crate == -1 && rcsu._slot == -1 && rcsu._unit == -1;
    case HW_CRATE:
      return rcsu._crate != -1 && rcsu._slot == -1 && rcsu._unit == -1;
    case HW_MODULE:
      return rcsu._crate != -1 && rcsu._slot != -1 && rcsu._unit == -1;
    case HW_UNIT:
      return rcsu._crate != -1 && rcsu._slot != -1 && rcsu._unit != -1;
    default:
      return false;
    }
}

bool hardware_def::has_controls(const char *key)
{
  for (model_as_vector::iterator i = _model_as.begin();
       i != _model_as.end(); ++i)
    if ((*i)->_parent->has_controls(key))
      return true;

  ha_control lookfor(find_str_strings(key),NULL);

  return _controls.find(&lookfor) != _controls.end();
}

std::vector<const char*> *hardware_def::get_controls(const char *key,
						     std::vector<const char*> *vect)
{
  for (model_as_vector::iterator i = _model_as.begin();
       i != _model_as.end(); ++i)
    vect = (*i)->_parent->get_controls(key,vect);

  ha_control lookfor(find_str_strings(key),NULL);
  
  control_multiset::iterator i = _controls.lower_bound(&lookfor);
  control_multiset::iterator j = _controls.upper_bound(&lookfor);

  if (i == j)
    return vect; // none found here

  if (!vect)
    vect = new std::vector<const char*>;

  for ( ; i != j; ++i)
    vect->push_back((*i)->_value);

  return vect;
}

const char *hardware_def::get_control(const char *key)
{
  std::vector<const char*> *vect = get_controls(key);

  if (!vect)
    return NULL;

  if (vect->size() > 1)
    {
      fprintf (stderr,"Getting CONTROL(%s) for %s.  Only one expected, but found several.\n",
	       key,_name);
    }

  const char *ctrl = (*vect)[0];

  delete vect;

  return ctrl;  
}


std::vector<const char *> *append_node(std::vector<const char *> *v,
					     const char *a)
{
  if (!v)
    v = new std::vector<const char *>;

  if (a)
    {
      v->push_back(a);
    }

  return v;
}

/* List of all known hardware.
 */

typedef std::map<const char *,hardware_def*> hardware_map;

hardware_map all_hardware;

void append_hardware(hardware_def *a)
{
  hardware_def *prev = find_hardware_def(a->_name);

  if (prev)
    {
      a->_loc.print_lineno(stderr);
      fprintf (stderr,"Hardware object '%s' redefined.\n",a->_name);
      prev->_loc.print_lineno(stderr);
      fprintf (stderr,"Previously defined here.\n");
      return;

      // Yes, we leak the object
    }

  all_hardware.insert(hardware_map::value_type(a->_name,a));
}

hardware_def *find_hardware_def(const char * model)
{
  hardware_map::iterator i;

  i = all_hardware.find(model);

  if (i != all_hardware.end())
    {
      return i->second;
    }

  return NULL;
}

void hardware_def::dump()
{
  printf ("// ");
  _loc.print_lineno(stdout,false);
  printf ("\n\n");

  const char* type_str = "";

  switch (_hw_type)
    {
    case HW_RACK:   type_str = "RACK"; break;
    case HW_DET:    type_str = "DET"; break;
    case HW_CRATE:  type_str = "CRATE"; break;
    case HW_MODULE: type_str = "MODULE"; break;
    case HW_UNIT:   type_str = "UNIT"; break;
    case HW_DEVICE: type_str = "DEVICE"; break;
    }

  printf ("%s(%s)\n",type_str,_name/*->c_str()*/);
  printf ("{\n");
  for (model_as_vector::iterator i = _model_as.begin();
       i != _model_as.end(); ++i)
    (*i)->dump();
  if (_label)
    _label->dump();
  if (_name_lit)
    _name_lit->dump();
  
  has_settings_map::iterator setting;

  for (setting = _settings.begin(); setting != _settings.end(); ++setting)
    (setting->second)->dump();

  control_multiset::iterator control;

  for (control = _controls.begin(); control != _controls.end(); ++control)
    (*control)->dump();

  connector_attr_map::iterator connector;

  for (connector = _connectors.begin(); connector != _connectors.end(); ++connector)
    (connector->second)->dump();

  printf ("}\n");
}

typedef std::map<const char *,hardware_def*,
		 compare_str_less> hardware_name_map;

void dump_hardware()
{
  const char *marker = "HARDWARE_DUMP";

  print_header(marker,"Dump of all hardware (types)");

  hardware_name_map hw;

  hardware_map::iterator i;

  for (i = all_hardware.begin(); i != all_hardware.end(); ++i)
    hw.insert(hardware_name_map::value_type(i->first,i->second));

  hardware_name_map::iterator j;

  for (j = hw.begin(); j != hw.end(); ++j)
    {
      printf ("\n");
      j->second->dump();
    }

  printf ("\n");
  print_footer(marker);
}

#include <stack>

void hardware_def::get_connectors(std::vector<const char *> &dest)
{
  // We need to dig through all model_as specifications as well

  hardware_def *model;
  std::stack<hardware_def*> models;
  models.push(this);

  for ( ; !models.empty(); )
    {
      model = models.top();
      models.pop();

      connector_attr_map::iterator i;
      
      for (i = model->_connectors.begin();
	   i != model->_connectors.end(); ++i)
	dest.push_back(i->first);
      
      for (model_as_vector::iterator i = model->_model_as.begin();
	   i != model->_model_as.end(); ++i)
	models.push((*i)->_parent);
    }
}



