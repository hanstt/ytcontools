

#ifndef __HW_MODULE_H__
#define __HW_MODULE_H__

#include "file_line.hh"

#include <vector>
#include <list>

#include "rcsu.hh"
#include "cable_node.hh"

#include "hardware_def.hh"

class hw_module_part 
{
public:
  virtual ~hw_module_part() { }

public:
  file_line _loc;

};

// This is a list and not a vector such that we
// can modify it (due to expand) when we are apply()-ing
// it to the hardware module.  (otherwise iterators are
// dead)

typedef std::list<hw_module_part*> hw_module_part_list;

class hw_setting : public hw_module_part
{
public:
  virtual ~hw_setting() { }

public:
  hw_setting(const char *name,const char *value)
  {
    _name  = name;  
    _value = value; 
  }

public:
  const char  *_name;
  const char  *_value;

public:
  void dump() 
  {
    printf("  SETTING(\"%s\" => \"%s\");\n",
	   _name/*->c_str()*/,_value/*->c_str()*/);
  }
};

class cable_marker
{
public:
  cable_marker(const char *to,
	       const char *from)
  {
    _from = from;
    _to   = to;
  }

public:
  virtual ~cable_marker() { }

public:
  const char *_from;
  const char *_to;

  virtual void dump() = 0;
};

class cable_marker_to : public cable_marker
{
public:
  cable_marker_to(const char * from,
		  const char * to) : 
    cable_marker(from,to) { }
  
public:
  virtual ~cable_marker_to() { }

public:
  virtual void dump()
  {
    if (_from)
      printf("\"%s\" ",_from/*->c_str()*/);
    printf("->");
    if (_to)
      printf(" \"%s\"",_to/*->c_str()*/);
  }
};

class cable_marker_from : public cable_marker
{
public:
  cable_marker_from(const char * to,
		    const char * from) : 
    cable_marker(from,to) { }

public:
  virtual ~cable_marker_from() { }

public:
  virtual void dump()
  {
    if (_to)
      printf("\"%s\" ",_to/*->c_str()*/);
    printf("<-");
    if (_from)
      printf(" \"%s\"",_from/*->c_str()*/);
  }
};

class hw_conn : public hw_module_part
{
public:
  hw_conn()
  {
    _connector = NULL;
    _attr = NULL;
  }

public:
  virtual ~hw_conn() { }

public:
  void set_cnctr(const char *connector)
  {
    _connector       = connector; 
  }

  virtual void dump() 
  {
    printf("  %s: ",_connector/*->c_str()*/);
  }

public:
  const char      *_connector;

  connector_attr  *_attr;
};

class hw_connection : public hw_conn
{
public:
  virtual ~hw_connection() { }

public:
  hw_connection() 
  {
    _other_type = NULL; 
    _other_connector = NULL;
    _marker = NULL; 
    _label = NULL; 
    _other_attr = NULL; 
  }

  hw_connection(rcsu_range   other,
		const char *other_connector)
  {
    _other_range     = other;
    _other_type      = NULL;
    _other_connector = other_connector; 
    _marker          = NULL;
    _label           = NULL;

    _other_attr      = NULL;
  }

  void set_label(const char *label)
  {
    _label = label;
  }
  
  void set_marker(cable_marker *marker)
  {
    _marker = marker;
  }

  void set_type(const char *type)
  {
    _other_type = type;
  }
  
  bool expand(hw_module_part_list* parts,
	      hw_module_part_list::iterator &insert_after);

public:
  union
  {
    rcsu        _other;
    rcsu_range  _other_range;
  };
  const char *_other_type;
  const char *_other_connector;
  const char *_label;
  cable_marker      *_marker;

  connector_attr    *_other_attr;

public:
  virtual void dump() 
  {
    hw_conn::dump();

    if (_label)
      printf("LABEL(\"%s\"), ",_label/*->c_str()*/);

    if (_marker)
      {
	_marker->dump();
	printf(", ");
      }

    printf("%s",_other.to_string().c_str());
    if (_other_type)
      printf("(%s)",_other_type/*->c_str()*/);
    printf("/%s;\n",_other_connector/*->c_str()*/);
  }
};
/*
class hw_connection2
{

public:
  struct
  {
    hw_module      *_module;
    connector_attr *_attr;
  } _connections[2];
  
  cable_marker     *_marker;
  const char *_label;
};
*/
class hw_conn_broken : public hw_conn
{
public:
  virtual ~hw_conn_broken() { }

public:
  hw_conn_broken(const char *connector)
  {
    set_cnctr(connector);
  }

  virtual void dump() 
  {
    hw_conn::dump();

    printf("BROKEN;\n");
  }

public:


};


class hw_label : public hw_module_part
{
public:
  virtual ~hw_label() { }

public:
  hw_label(const char *label) { _label = label; };

public:
  const char *_label;

public:
  void dump() { printf ("  LABEL(\"%s\");\n",_label/*->c_str()*/); }
};

class hw_serial : public hw_module_part
{
public:
  virtual ~hw_serial() { }

public:
  hw_serial(const char *serial) { _serial = serial; };

public:
  const char *_serial;

public:
  void dump() { printf ("  SERIAL(\"%s\");\n",_serial/*->c_str()*/); }
};

class hw_broken : public hw_module_part
{
public:
  virtual ~hw_broken() { }

public:
  // We do not really need any members.  Our presence is enough.
  // AARGH.

public:
  void dump() { printf ("  BROKEN;\n"); }
};

class hw_control : public hw_module_part
{
public:
  virtual ~hw_control() { }

public:
  hw_control(const char *key,const char *value) { _key = key; _value = value; };

public:
  const char *_key;
  const char *_value;

public:
  bool operator<(const hw_control &rhs) const
  {
    return _key < rhs._key;
  }

public:
  void dump() { printf ("  CONTROL(\"%s\",\"%s\");\n",_key,_value); }
};

hw_module_part_list *append_node(hw_module_part_list *v,
				 hw_module_part *a);

typedef std::map<const char *,hw_setting*>    settings_map;
typedef std::map<const char *,hw_conn*>       connections_map;

typedef std::multiset<hw_control*,compare_ptr<hw_control> > hw_control_multiset;

typedef std::multimap<const char *,hw_module *> hw_has_control_multimap;

extern hw_has_control_multimap _module_has_control;

class hw_module :
  public cabling_node
{
public:
  virtual ~hw_module() { }

public:
  hw_module(hardware_def *model,
	    rcsu location)
  {
    _model    = model;
    _location = location;

    _label    = NULL;
    _serial   = NULL;
  }

public:
  file_line _loc;

public:
  hardware_def *_model;
  rcsu          _location;

  hw_label     *_label;
  hw_serial    *_serial;

  settings_map    _settings;
  connections_map _connections;

  hw_control_multiset   _controls;

public:
  void apply(hw_module_part_list* parts);

public:
  void dump();
  void short_dump_lengths(int *max_rcsu,int *max_model,int *max_label);
  void short_dump(int max_rcsu,int max_model,int max_label);

public:
  bool check_valid_hw_setting(hw_setting* setting);
  bool check_valid_hw_conn(hw_conn* connection);

public:
  bool is_model_as(const char *name);

public:
  hw_connection* get_connection(const char *name);

public:
  const char* get_setting(const char *name);

public:
  std::vector<const char*> *get_controls(const char *key);
  const char *get_control(const char *key);

  bool has_control(const char *key);

public:
  hw_module *find_parent_module();
};

typedef std::map<rcsu,hw_module*> modules_map;

extern modules_map all_modules;

void dump_modules();

void dump_modules_short();

#endif// __HW_MODULE_H__



