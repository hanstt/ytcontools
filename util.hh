#ifndef __UTIL_HH__
#define __UTIL_HH__

#include <stdlib.h>
#include <string.h>

char *make_message(const char *fmt,...);
void print_header(const char *marker, const char *comment);
void print_footer(const char *marker);

template<typename T>
void swap(T &a, T &b)
{
  T tmp = a;
  a = b;
  b = tmp;
}

#endif//__UTIL_HH__
