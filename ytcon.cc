
#include "hardware_def.hh"
#include "cable_node.hh"

#include "daq_code.hh"

#include "name_map.hh"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern int lexer_read_fd;

bool parse_cables();
/*
static struct option long_options[] = {
  {"daq_code",      no_argument,       0, 0 },
  {"analysis_base", no_argument,       0, 0 },
  {"slow_base",     no_argument,       0, 0 },
  {"old_base",      no_argument,       0, 0 },
  {"trace_cables",  required_argument, 0, 0 },
  {"file", 1, 0, 0},
  {0, 0, 0, 0}
};
*/
void usage()
{
  printf ("ytcon\n"
	  "usage ytcon [--daq_code|--analysis_base|--slow_base]\n");

  printf ("    --daq_code           Generate (parts of) DAQ code.\n"
	  "    --analysis_base      Generate 'database' for analysis.\n"
	  "    --slow_base          Generate 'database' for slow control.\n"
	  "    --old_base           Generate kumacs for land-PAW database.\n"
	  "    --trace_cables=rcsu  Trace cables to/from specified module.\n");

  printf ("    ");
}

int main(int argc,char *argv[])
{
  lexer_read_fd = 0; // read from stdin

  for (int i = 1; i < argc; i++)
    {
      if (strncmp(argv[i],"--ignore-mismatch=",18) == 0)
	ignore_mismatches(argv[i]+18);
      else
	{
	  fprintf (stderr,"Bad argument: '%s'.\n",argv[i]);
	  exit(1);
	}
    }

  if (!parse_cables())
    {
      fprintf (stderr,"%s: Aborting!\n",argv[0]);
      exit(1);
    }

  map_cable_nodes();

  if (!link_cable_connections()) {
    fprintf(stderr, "Failed to link cables, aborting!\n");
    exit(EXIT_FAILURE);
  }

  name_map_dump();
  dump_modules_short();

  dump_hardware();
  dump_modules();

  generate_code_and_tables();

  return 0;
}


/* For generating tables:
 *
 * We can go two ways: either from detectors to TDC/QDCs.  Or trace
 * back from TDC/QDCs and see what detectors are connected.
 *
 * Since channels are named after the LABEL that is closest on the
 * path to the TDC/QDC, it makes most sense to do the assignment from
 * the backwards search.  However, for debugging, it is also of
 * interest to get a list of unconnected detectors, and also of
 * QDC/TDC channels that are unused.
 *
 */





/* A few module names/identifiers are used as hard-coded strings in
 * the program to do the tracing of signals.  Preferably as generic
 * names, and not vendor-specific model identifiers.
 *
 * The intention is that this list should be complete.  Use grep(1)
 * to find the actual occurances.
 *
 * TDC     
 * QDC     
 * ADC
 * SCALER
 *
 * DAQ_EVENTWISE
 * DAQ_MONITOR
 * SLOW_CONTROL
 *         Used as a 'base class' to identify modules that should
 *         by read out by a daq program.
 *
 * CAMAC_CRATE             
 *         Identify CAMAC crates.  Slot 24 (and 25) is searched for a controller)
 *
 * FASTBUS_CRATE
 *         Dito
 *
 * CAMAC_CRATE_CONTROLLER  
 *         Figure out what crate in a CAMAC chain we are.
 *         setting::CRATE_NO
 *
 * PROCESSOR
 *         Find out the hostname of the cpu in charge.
 *         setting::HOSTNAME
 *
 */








