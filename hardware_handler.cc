
#include "hardware_def.hh"
#include "str_set.hh"

#include <stdlib.h>
#include <string.h>

struct match_item
{
public:
  match_item(const char *start,const char *end)
  {
    _length = end - start;

    char *s = new char[_length+1];
    memcpy(s,start,_length);
    s[_length] = '\0';

    _str = s;

    char *p = strchr(s,'%');

    if (p)
      {
	_length = p - s;
	*p = '\0';
	_post = p+1;
      }
    else
      _post = ""; // such that _post never is NULL, but then empty

    // printf ("[%s]",s);
  }

public:
  const char *_str;
  int _length;

  const char *_post;
};

class match_set
{
public:
  std::vector<match_item> _inputs;
  std::vector<match_item> _outputs; 
};

class multi_in_out_handler 
  : public ha_handler
{
public:
  multi_in_out_handler(const char *name,const char *initstring)
    : ha_handler(name,initstring)
  {
    // The initstring has format connectorI1,connectorI2...=connectorO1,connectorO2...
    //
    // Where anything from any I affects all O, and vice versa.  (Any O sees all I)

    parse_set_several(name,initstring);
  }


  void parse_set_several(const char *name,const char *initstring)
  {
    match_set* s;

    for ( ; ; )
      {
	const char *colon  = index(initstring,':');

	if (colon)
	  {
	    s = parse_set(initstring,colon);
	    if (!s)
	      goto malformed;
	    _sets.push_back(s);
	    initstring = colon+1;
	  }
	else 
	  break;
      }
    s = parse_set(initstring,index(initstring,'\0'));   
    if (!s)
      goto malformed;
    _sets.push_back(s);

    return;
  malformed:
    fprintf (stderr,"Malformed initializer for HANDLER(%s,%s)\n",
	     name,initstring);
  }

  match_set* parse_set(const char *str,const char* end)
  {
    match_set* s = new match_set;
    
    const char *equals  = index(str,'=');

    if (!equals || equals >= end-1)
      {
	delete s;
	return NULL;
      }

    if (!parse_ids(str     ,equals,s->_inputs) ||
	!parse_ids(equals+1,end   ,s->_outputs))
      return NULL;
    return s;
  }

  bool parse_ids(const char *str,const char* end,std::vector<match_item> &ids)
  {
    const char *comma = index(str,',');

    while (comma && comma < end)
      {
	if (comma == str)
	  return false;

	//printf ("ID<%s/%s>\n",str,comma);
	ids.push_back(match_item(str,comma));
	str = comma+1;
	comma  = index(str,',');
      }    
    if (comma == str)
      return false;
    
    //printf ("ID<%s/%s>\n",str,end);
    ids.push_back(match_item(str,end));
    return true;
  }

  std::vector<match_set*> _sets;

  //std::vector<match_item> _inputs;
  //std::vector<match_item> _outputs; 

public:
  virtual std::vector<const char *> *get_continuation(const char *conn)
  {
    // Try to find the connector

    std::vector<match_item>::iterator i;
    std::vector<match_item> *other;
    const char *next;
    int index = -1;

    for (std::vector<match_set*>::iterator s = _sets.begin(); s != _sets.end(); ++s)
      {
	match_set* set = *s;
	
	for (i = set->_inputs.begin(); i != set->_inputs.end(); ++i)
	  {
	    //printf ("{%s==%s?}",i->_str,conn);
	    if (strncmp(i->_str,conn,i->_length) == 0)
	      {
		next = conn+i->_length;
		
		if (*next != '\0')
		  {
		    char* endptr;
		    index = strtol(next,&endptr,10);
		    if (strcmp(endptr,i->_post) != 0)
		      continue;
		    index = map_index(index,true);
		  }
		
		// We have a match.
		// Handle continuation.
		other = &set->_outputs;
		goto found;
	      }
	  }
	
	for (i = set->_outputs.begin(); i != set->_outputs.end(); ++i)
	  {
	    //printf ("{%s==%s?}",i->_str,conn);
	    if (strncmp(i->_str,conn,i->_length) == 0)
	      {
		next = conn+i->_length;
		
		if (*next != '\0')
		  {
		    char* endptr;
		    index = strtol(next,&endptr,10);
		    if (strcmp(endptr,i->_post) != 0)
		      continue;
		    index = map_index(index,false);
		  }
		
		// We have a match.
		// Handle continuation.
		other = &set->_inputs;
		goto found;
	      }
	  }
      }
    return NULL;

  found:
    // Either the string is to be empty, or it should have an index

    //printf ("[Match %s / %d",
    //    conn,index);

    std::vector<const char *> *items = new std::vector<const char *>;

    for (i = other->begin(); i != other->end(); ++i)
      {
	if (index == -1)
	  items->push_back(find_str_identifiers(i->_str));
	else
	  {
	    char *digit_str = new char [i->_length + 16];
	    sprintf (digit_str,"%s%d%s", i->_str, index, i->_post);
	    //printf ("=>%s",digit_str);
	    items->push_back(find_str_identifiers(digit_str));
      delete [] digit_str;
	  }
      }    
    //printf ("]");
    return items;
  }

  virtual int map_index(int index,bool)
  {
    return index;
  }

};

class split_handler 
  : public multi_in_out_handler
{
public:
  split_handler(const char *name,const char *initstring)
    : multi_in_out_handler(name,initstring)
  {
    // Check that mapping is 1-to-1

    for (int i = 1; i <= 16; i++)
      if (map_index(map_index(i,true),false) != i ||
	  map_index(map_index(i,false),true) != i)
	fprintf (stderr,"Internal error in split_handler mapping (%d).\n",i);   
  }

public:
  virtual int map_index(int index,bool forward)
  {
    if (index < 1 || index > 16)
      fprintf (stderr,"Bad index(%d) given to split_handler::map_index.\n",index);

    if (forward)
      {
	// 1,2,3,...,8 => 1,3,5,...,15    9,10,11,...,16 => 2,4,6,...,16
	return index <= 8 ? (index-1) * 2 + 1 : (index - 9) * 2 + 2;
      }
    else
      {
	// 1,3,5,...,15 => 1,2,3,...,8    2,4,6,...,16 => 9,10,11,...,16
	return index & 0x01 ? (index - 1) / 2 + 1 : (index - 2) / 2 + 9;
      }
  }
};

ha_handler *new_ha_handler(const char *name,
			   const char *initstring)
{


  if      (strcmp(name,"MULTI_IN_OUT") == 0)  return new multi_in_out_handler(name,initstring);
  else if (strcmp(name,"SPLIT_BOX") == 0)     return new split_handler(name,initstring);

  fprintf (stderr,"Unknown HANDLER(%s)\n",name);

  return NULL;
}
