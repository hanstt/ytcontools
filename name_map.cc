#include "name_map.hh"
#include "util.hh"
#include <cstdio>
#include <map>

namespace {
typedef std::map<std::string, std::string> NameMap;
NameMap g_name_map;
}

void name_map_add(char const *a_from, char const *a_to)
{
	g_name_map.insert(NameMap::value_type(a_from, a_to));
}

void name_map_dump()
{
  const char *marker = "NAME_MAP_DUMP";

  print_header(marker,"Dump of all name mappings");
  printf("\n");
  for (NameMap::iterator it = g_name_map.begin(); g_name_map.end() != it; ++it)
  {
    printf("NAME_MAP(%s,%s)\n", it->first.c_str(), it->second.c_str());
  } 
  printf("\n");
  print_footer(marker);
}
