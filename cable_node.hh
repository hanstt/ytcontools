#ifndef __CABLE_NODE_H__
#define __CABLE_NODE_H__

#include "rcsu.hh"

#include "hardware_def.hh"

#include <vector>
#include <map>

struct cabling_node
{
  public:
    virtual ~cabling_node() { }
};

typedef std::vector<cabling_node*> cabling_node_list;

extern cabling_node_list *all_cable_nodes;

cabling_node_list* append_node(cabling_node_list *v,
			       cabling_node *a);

void append_nodes(cabling_node_list **v,
		  cabling_node_list *a);

cabling_node *node_list_pack(cabling_node_list *v);

#include "hw_module.hh"

void map_cable_nodes();

hw_module *find_module(rcsu &loc);

bool link_cable_connections();

void ignore_mismatches(const char *ignores);

#endif// __CABLE_NODE_H__
