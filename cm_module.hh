

#ifndef __CM_MODULE_H__
#define __CM_MODULE_H__

#include "hw_module.hh"

#include <assert.h>
#include <vector>

#define RM_CAMAC        0x01
#define RM_VME          0x02
#define RM_GTB          0x03
#define RM_FASTBUS      0x04
#define RM_MAINFRAME    0x05
#define RM_SERIAL       0x06

struct reach_module
{
public:
  reach_module()
  {
    _type = 0;
  }

public:
  int _type;

  union
  {
    struct
    {
      int _crate;
      int _slot;
    } _camac;
    struct
    {
      int _sam;
      int _branch;
      int _slave;
    } _gtb;
    struct
    {
      int _crate;
      int _slot;
    } _mfr;
    struct
    {
      int _crate;
      unsigned int _address;
      int _slot;
    } _vme;
    struct
    {
      int _rack;
      int _crate;
      int _slot;
    } _fastbus;
  } _access;

public:
  void camac(int crate,int slot)
  {
    _type = RM_CAMAC;
    _access._camac._crate = crate;
    _access._camac._slot = slot;
  }

  void gtb(int sam,int branch,int slave)
  {
    _type = RM_GTB;
    _access._gtb._sam = sam;
    _access._gtb._branch = branch; // aka gtb
    _access._gtb._slave = slave;
    // branch?
  }

  void mainframe(int crate,int slot)
  {
    _type = RM_MAINFRAME;
    _access._mfr._crate = crate;
    _access._mfr._slot = slot;
  }

  void vme(int crate,unsigned int address,int slot)
  {
    _type = RM_VME;
    _access._vme._crate = crate;
    _access._vme._address = address;
    _access._vme._slot = slot;
  }

  void fastbus(int rack,int crate,int slot)
  {
    _type = RM_FASTBUS;
    _access._fastbus._rack = rack;
    _access._fastbus._crate = crate;
    _access._fastbus._slot = slot;
  }

public:
  void reach_fprintf(FILE* out);
};

typedef std::vector<reach_module> reach_module_vector;

// For flags

#define CM_DAQ_EVENTWISE  0x01
#define CM_DAQ_MONITOR    0x02
#define CM_SLOW_CONTROL   0x04
#define CM_MODULE_TYPE    0x07

#define CM_CAMAC          0x010
#define CM_VME            0x020
#define CM_GTB            0x040
#define CM_FASTBUS        0x080
#define CM_MAINFRAME      0x100
#define CM_CRATE_MASK     0x1f0

struct physical_signals;

struct controllable_module
{
public:
  controllable_module(hw_module *module,int flags)
  {
    _module    = module;
    _flags     = flags;

    _processor = NULL;
    _controller = NULL;

    _name      = NULL;

    _num_use_channels = -1;
  }

public:
  // The module

  hw_module *_module;

  // For what is is used (i.e. in what tables etc should go)

  int _flags;

  // The controlling processor

  hw_module *_processor;

  // The next controllable module in the chain towards the processor

  controllable_module *_controller;
  
  // How to reach it from the processor (or rather, next controllable module)

  reach_module _reach;

  // Physical signals that are associated with it.  (and how they are reached)

  physical_signals *_signals;

  // Variable name in generated code

  const char* _name;

  // If channels at the end are disabled (due to non.use).  -1 => do nothing

  int _num_use_channels;

public:
  reach_module *single_reach(int type);

  void backwards_reach_fprintf(FILE* out);
};

typedef std::vector<controllable_module*> cm_vector;



#endif// __CM_MODULE_H__

