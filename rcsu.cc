
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <map>

#include "rcsu.hh"
#include "str_set.hh"
#include "file_line.hh"

extern int yylineno;

#define LINENO_PRINTF_ERR_RET_VAL(msg) { print_lineno(stderr,yylineno); fprintf(stderr,"rcsu : " msg " in %s\n",src); return val; }

#define RD_STRING_RACK 1
#define RD_STRING_DET  2

struct rack_det_string
{
  const char *_str;
  int _type;

  bool operator<(const rack_det_string &rhs) const
  {
    return strcmp(_str,rhs._str) < 0;
  }
};

typedef std::map<rack_det_string,int> map_rack_det_string_ind;
typedef std::map<int,rack_det_string> map_rack_det_ind_string;

map_rack_det_string_ind _map_rack_det_str_i;
map_rack_det_ind_string _map_rack_det_i_str;

int _next_rack_det_string_index = INT_MAX;

rack_det_string get_rack_det_string(int ind)
{
  map_rack_det_ind_string::iterator is_i = _map_rack_det_i_str.find(ind);

  if (is_i == _map_rack_det_i_str.end())
    {
      rack_det_string unknown = { "UNKNOWN", -1 };
      return unknown;
    }

  return is_i->second;
}

bool compare_rcsu_rd_name::operator()(const rcsu &lhs,const rcsu &rhs) const
{
  if (lhs._rack == rhs._rack)
    {
      return lhs < rhs;
    }

  // rack not equal

  int cmptype_lhs = lhs._rack > _next_rack_det_string_index;
  int cmptype_rhs = rhs._rack > _next_rack_det_string_index;

  if (cmptype_lhs != cmptype_rhs)
    return cmptype_lhs < cmptype_rhs;

  if (cmptype_lhs) // textual
    {
      rack_det_string rds_lhs = get_rack_det_string(lhs._rack);
      rack_det_string rds_rhs = get_rack_det_string(rhs._rack);

      if (rds_lhs._type != rds_rhs._type)
	return rds_lhs._type < rds_rhs._type;

      return strcmp(rds_lhs._str,rds_rhs._str) < 0;
    }

  return lhs._rack < rhs._rack;
}

int rack_det_by_string(const char *src,char **endptr,
		       int type)
{
  /* We handle cases such as rGFIc5...
   * (and are handling the GFI part.  I.e. src points to G,
   * and when we're done, endptr will point to c.
   */

  /* Pure numbers, we in principle could handle, but leave alone.
   * (i.e. return 0).
   */

  /* Since 'c' is the separator, we let any lowercase letter be the end.
   */

  const char *end = src;

  while (isupper(*end))
    end++;

  if (end == src)
    return 0;
 
  while (isupper(*end) || isdigit(*end))
    end++;

  if (end > src + 32)
    {
      // Too long
      print_lineno(stderr,yylineno); 
      fprintf(stderr,"rack string too long in %s\n",src); 
      return 0;
    }

  /* Add the string to our list.
   * (or find it).
   */

  int len = end-src;
  char *str = new char[len+1];

  memcpy(str,src,len);
  str[len] = '\0';

  //fprintf (stderr,"Looking for %s in rack string list...\n",str);

  rack_det_string search;

  search._str  = str;
  search._type = type;

  map_rack_det_string_ind::iterator si_i = _map_rack_det_str_i.find(search);

  *endptr = (char *) end;

  if (si_i == _map_rack_det_str_i.end())
    {
      int ind = _next_rack_det_string_index--;

      _map_rack_det_str_i.insert(map_rack_det_string_ind::value_type(search,
								     ind));
      _map_rack_det_i_str.insert(map_rack_det_ind_string::value_type(ind,
								     search));

      //fprintf (stderr,"Inserted %s into rack string list...\n",str);

      return ind;
    }
  else
    {
      delete[] str;

      return si_i->second;
    }
}

rcsu create_partial_rcsu_same(const char *src)
{
  rcsu val;

  val._rack  = -1;
  val._crate = -1;
  val._slot  = -1;
  val._unit  = -1;

  char *endptr = (char*) src;
  if (*(endptr++) != '.') LINENO_PRINTF_ERR_RET_VAL("no .");
  if (*(endptr) == 'c') { endptr++; val._crate = -2; }
  else if (*(endptr) == 's') { endptr++; val._slot  = -2; }
  else if (*(endptr) == 'u') { endptr++; val._unit  = -2; }

  if (*(endptr++) != '\0') LINENO_PRINTF_ERR_RET_VAL("not ending properly");
  
  return val;
}

rcsu create_partial_rcsu(const char *src,bool partial)
{
  char *endptr = (char*) src;
  rcsu val;

  //fprintf (stderr,"RCSU: '%s'\n",src);
  
  val._rack  = -1;
  val._crate = -1;
  val._slot  = -1;
  val._unit  = -1;

  if (partial)
    {
      if (*(endptr++) != '.') LINENO_PRINTF_ERR_RET_VAL("no .");
      if (0 == strncmp("rack", endptr, 4)) { endptr+=4; goto rack; }
      if (*(endptr) == 'r') { endptr++; goto rack; }
      if (*(endptr) == 'd') { endptr++; goto det; }
      if (*(endptr) == 'c') { endptr++; goto crate; }
      if (*(endptr) == 's') { endptr++; goto slot; }
      if (*(endptr) == 'u') { endptr++; goto unit; }
    }

  if (*endptr == 'd')
    {
      endptr++;
    det:
      if ((val._rack = rack_det_by_string(endptr,&endptr,RD_STRING_DET)) == 0)
	LINENO_PRINTF_ERR_RET_VAL("bad d spec");
      goto check_crate;
    }

  if (0 == strncmp("rack", endptr, 4)) { endptr+=4; }
  else if (*(endptr++) != 'r') LINENO_PRINTF_ERR_RET_VAL("no r");
 rack:
  if ((val._rack = rack_det_by_string(endptr,&endptr,RD_STRING_RACK)) == 0)
    val._rack  = strtol(endptr,&endptr,10);
 check_crate:
  if (*(endptr) == '\0') return val;
  if (*(endptr++) != 'c') LINENO_PRINTF_ERR_RET_VAL("no c");
 crate:
  val._crate = strtol(endptr,&endptr,10);
  if (*(endptr) == '\0') return val;
  if (*(endptr++) != 's') LINENO_PRINTF_ERR_RET_VAL("no s");
 slot:
  val._slot = strtol(endptr,&endptr,10);
  if (*(endptr) == '\0') return val;
  if (*(endptr++) != 'u') LINENO_PRINTF_ERR_RET_VAL("no u");
 unit:
  val._unit = strtol(endptr,&endptr,10);
  if (*(endptr++) != '\0') LINENO_PRINTF_ERR_RET_VAL("not ending properly");
  
  return val;
}

#undef LINENO_PRINTF_ERR_RET_VAL
#define LINENO_PRINTF_ERR_RET_VAL(msg) { print_lineno(stderr,yylineno); fprintf(stderr,"rcsu_range : " msg " in %s\n",src); return val; }

rcsu_range create_partial_rcsu_range(const char *src,bool partial)
{
  char *endptr = (char*) src;
  rcsu_range val;

  //fprintf (stderr,"RCSU_r: '%s'\n",src);
  
  val._first._rack  = val._last._rack  = -1;
  val._first._crate = val._last._crate = -1;
  val._first._slot  = val._last._slot  = -1;
  val._first._unit  = val._last._unit  = -1;

  if (partial)
    {
      if (*(endptr++) != '.') LINENO_PRINTF_ERR_RET_VAL("no .");
      if (0 == strncmp("rack", endptr, 4)) { endptr+=4; goto rack; }
      if (*(endptr) == 'r') { endptr++; goto rack; }
      if (*(endptr) == 'd') { endptr++; goto det; }
      if (*(endptr) == 'c') { endptr++; goto crate; }
      if (*(endptr) == 's') { endptr++; goto slot; }
      if (*(endptr) == 'u') { endptr++; goto unit; }
    }

  if (*endptr == 'd')
    {
      endptr++;
    det:
      if ((val._first._rack = rack_det_by_string(endptr,&endptr,
						 RD_STRING_DET)) == 0)
	LINENO_PRINTF_ERR_RET_VAL("bad d spec");
      val._last._rack = val._first._rack;
      goto check_crate;
    }

  if (0 == strncmp("rack", endptr, 4)) { endptr+=4; goto rack; }
  else if (*(endptr++) != 'r') LINENO_PRINTF_ERR_RET_VAL("no r");
 rack:
  if ((val._first._rack = rack_det_by_string(endptr,&endptr,
					     RD_STRING_RACK)) == 0)
    val._first._rack = strtol(endptr,&endptr,10);
  val._last._rack = val._first._rack;
 check_crate:
  if (*(endptr++) != 'c') LINENO_PRINTF_ERR_RET_VAL("no c");
 crate:
  val._first._crate  = val._last._crate = strtol(endptr,&endptr,10);
  if (*(endptr) == '_')
    {
      val._last._crate = strtol(endptr+1,&endptr,10);
      if (*(endptr++) != '\0') LINENO_PRINTF_ERR_RET_VAL("not ending properly");
      return val;
    }
  if (*(endptr++) != 's') LINENO_PRINTF_ERR_RET_VAL("no s");
 slot:
  val._first._slot  = val._last._slot = strtol(endptr,&endptr,10);
  if (*(endptr) == '_')
    {
      val._last._slot = strtol(endptr+1,&endptr,10);
      if (*(endptr++) != '\0') LINENO_PRINTF_ERR_RET_VAL("not ending properly");
      return val;
    }
  if (*(endptr++) != 'u') LINENO_PRINTF_ERR_RET_VAL("no u");
 unit:
  val._first._unit  = val._last._unit = strtol(endptr,&endptr,10);
  if (*(endptr) == '_')
    {
      val._last._unit = strtol(endptr+1,&endptr,10);
      if (*(endptr++) != '\0') LINENO_PRINTF_ERR_RET_VAL("not ending properly");
      return val;
    }
  else
    LINENO_PRINTF_ERR_RET_VAL("no _");
  
  return val;
}

rcsu_range to_rcsu_range(const rcsu& src)
{
  rcsu_range val;

  val._first = src;
  val._last  = src;

  return val;
}

bool rcsu::make_full(const rcsu &src)
{
  if (_rack == -1)
    {
      if (src._rack == -1)
	return false;
      _rack = src._rack;
    }
  else
    return true;

  if (_crate == -2)
    {
      if (src._crate == -1)
	return false;
      _crate = src._crate;
      return true;
    }
  else if (_crate == -1)
    {
      if (src._crate == -1)
	return false;
      _crate = src._crate;
    }
  else
    return true;

  if (_slot == -2)
    {
      if (src._slot == -1)
	return false;
      _slot = src._slot;
      return true;
    }
  else if (_slot == -1)
    {
      if (src._slot == -1)
	return false;
      _slot = src._slot;
    }

  if (_unit == -2)
    {
      if (src._unit == -1)
	return false;
      _unit = src._unit;
      return true;
    }

  return true;
}

bool rcsu::operator<(const rcsu& rhs) const
{
  if (_rack < rhs._rack)
    return true;
  if (_rack > rhs._rack)
    return false;
  
  if (_crate < rhs._crate)
    return true;
  if (_crate > rhs._crate)
    return false;
  
  if (_slot < rhs._slot)
    return true;
  if (_slot > rhs._slot)
    return false;
  
  if (_unit < rhs._unit)
    return true;
  if (_unit > rhs._unit)
    return false;
  
  return false;
}

bool rcsu::is_less_text(const rcsu &rhs) const
{
  // TODO: if we start to use text-names for RACK (and possibly CRATE),
  // then this one is suppose to take that into account (as opposed to
  // to operator< )

   if (_rack < rhs._rack)
    return true;
  if (_rack > rhs._rack)
    return false;
  
  if (_crate < rhs._crate)
    return true;
  if (_crate > rhs._crate)
    return false;
  
  if (_slot < rhs._slot)
    return true;
  if (_slot > rhs._slot)
    return false;
  
  if (_unit < rhs._unit)
    return true;
  if (_unit > rhs._unit)
    return false;
  
  return false;
}
 
bool rcsu::operator==(const rcsu& rhs) const
{
  return 
    _rack  == rhs._rack  &&
    _crate == rhs._crate &&
    _slot  == rhs._slot  &&
    _unit  == rhs._unit;
}

bool rcsu::operator!=(const rcsu& rhs) const
{
  // The gcc 2.95 could do without this function.
  // 3.3.3 could not. What is the standard?

  return !(*this == rhs);
}

std::string rcsu::to_string() const
{
  char buf[128];
  char *p_rack;
  int len_rack;

  if (_rack > _next_rack_det_string_index)
    {
      rack_det_string rds = get_rack_det_string(_rack);
      
      len_rack = sprintf (buf,"%c%s",
			  rds._type == RD_STRING_RACK ? 'r' :
			  rds._type == RD_STRING_DET ? 'd' : 'u',
			  rds._str);
    }
  else
    len_rack = sprintf (buf,"r%d",_rack);

  p_rack = buf + len_rack;

  if (_crate == -1)
    ;
  else if (_slot == -1)
    sprintf (p_rack,"c%d",_crate);
  else if (_unit == -1)
    sprintf (p_rack,"c%ds%d",_crate,_slot);
  else
    sprintf (p_rack,"c%ds%du%d",_crate,_slot,_unit);

  return std::string(buf);
}

bool rcsu_range::is_range()
{
  return 
    _first._rack  != _last._rack  ||
    _first._crate != _last._crate ||
    _first._slot  != _last._slot  ||
    _first._unit  != _last._unit;
}

int rcsu_range::num_range()
{
  return 
    (_last._rack  - _first._rack  + 1) *
    (_last._crate - _first._crate + 1) *
    (_last._slot  - _first._slot  + 1) *
    (_last._unit  - _first._unit  + 1);
}

rcsu rcsu_range::next_other(const rcsu &prev)
{
  rcsu val = prev;

  if (val._unit  < _last._unit)  { val._unit ++; return val; } else { val._unit  = _first._unit ; }
  if (val._slot  < _last._slot)  { val._slot ++; return val; } else { val._slot  = _first._slot ; }
  if (val._crate < _last._crate) { val._crate++; return val; } else { val._crate = _first._crate; }
  if (val._rack  < _last._rack)  { val._rack ++; return val; } else { fprintf (stderr,"Internal error in rcsu_range::next_other()"); exit(1); /* Not really here, but called too many times. */ }
}
