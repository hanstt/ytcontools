

#ifndef __ENUMERATE_H__
#define __ENUMERATE_H__

#include "hardware_def.hh"

void enumerate_modules(module_vector &modules,
		       const char *name,
		       const char *control_override);

void enumarate_modules_control(module_vector &modules,
			       const char *name,
			       const char *control_override);

#endif// __ENUMERATE_H__

