#include "hw_module.hh"

#include "enumerate.hh"

#include "str_set.hh"

#include <assert.h>
#include <stack>

void enumerate_modules(module_vector &modules,
		       const char *name,
		       const char *control_override)
{
  // find the hardware module with the given name

  std::stack<hardware_def*> models;

  hardware_def *model = find_hardware_def(find_str_identifiers(name));

  if (!model)
    {
      // Warning message?
      return;
    }

  models.push(model);
  
  while (!models.empty())
    {
      model = models.top();
      models.pop();

      // also include instances of hardware modelled as this one
      // (e.g. catching all TDCs)

      for (modelled_as_vector::iterator i = model->_modelled_as.begin();
	   i != model->_modelled_as.end(); ++i)
	models.push(*i);

      for (module_vector::iterator i = model->_instances.begin();
	   i != model->_instances.end(); ++i)
	{
	  if (!((*i)->has_control(control_override)))
	    modules.push_back(*i);
	}
    }
}

// hw_has_control_multimap _module_has_control;

void enumarate_modules_control(module_vector &modules,
			       const char *name,
			       const char *control_override)
{
  // typedef std::vector<hw_module*>               module_vector;

  printf ("Searching for control: %s/%s (%d)\n",
	  control_override,name,
	  (int)(_module_has_control.size()));

  for (hw_has_control_multimap::iterator i = _module_has_control.begin();
       i != _module_has_control.end(); ++i)
    {
      printf ("Checking for control: %s ... %s:%s\n",
	      control_override,
	      i->first,
	      i->second->_location.to_string().c_str());

    }

  const char *lookfor_ctrl = find_str_strings(control_override);
  const char *lookfor_name = find_str_strings(name);

  hw_has_control_multimap::iterator i = _module_has_control.lower_bound(lookfor_ctrl);
  hw_has_control_multimap::iterator j = _module_has_control.upper_bound(lookfor_ctrl);

  for ( ; i != j; ++i)
    {
      assert(i->second->has_control(control_override));

      const char *n = i->second->get_control(control_override);

      printf ("Searching for control: %s ... %s , found: %s(%p) (%p)\n",
	      control_override,
	      i->second->_location.to_string().c_str(),
	      n,n,lookfor_name);

      if (n == lookfor_name)
	modules.push_back(i->second);
    }
}
