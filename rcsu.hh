
#ifndef __RCSU_HH__
#define __RCSU_HH__

#include <string>

/* Rack, crate, slot unit.
 *
 * Undefined items are -1.  Note that only items from the end may be
 * undefined (except when being partial).  A partial item of -2 means:
 * stop copying at this item, i.e. strip later slot / unit.
 */

class rcsu
{
public:
  int _rack;
  int _crate;
  int _slot;
  int _unit;

public:
  bool operator<(const rcsu& rhs) const;

  bool operator==(const rcsu& rhs) const;
  bool operator!=(const rcsu& rhs) const;

public:
  int is_partial() { return _rack == -1; }

  bool make_full(const rcsu &src);

  bool is_less_text(const rcsu &rhs) const;

public:
  std::string to_string() const;
};

struct compare_rcsu_rd_name
{
  bool operator()(const rcsu &lhs,const rcsu &rhs) const;
};

class rcsu_range
{
public:
  rcsu _first;
  rcsu _last;

public:
  bool is_range();

  int num_range();

  rcsu next_other(const rcsu &prev);

};

rcsu create_partial_rcsu(const char *src,bool partial = true);

rcsu create_partial_rcsu_same(const char *src);

inline rcsu create_rcsu(const char *src) { return create_partial_rcsu(src,false); }

rcsu_range create_partial_rcsu_range(const char *src,bool partial = true);

inline rcsu_range create_rcsu_range(const char *src) { return create_partial_rcsu_range(src,false); }

rcsu_range to_rcsu_range(const rcsu& src);

#endif// __RCSU_HH__
