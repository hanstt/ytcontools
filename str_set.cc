
#include "str_set.hh"

#include <set>

/* We keep a set of all strings that are in use.  This because they are
 * duplicated at so many places.  We do not have to constantly allocate
 * them.  We only have to store pointers.  Which in turn leads to less
 * memory use, better cache hit rate, etc...
 *
 * The set as such need to store pointers to the strings, as otherwise
 * the set may reallocate itself moving the strings.
 */

typedef std::set<const char*,
                 compare_str_less > set_strings;

/* If you want to compare two strings for equality, as long as they
 * are in the same set, a simple pointer comparison is enough, as
 * those are unique.  This however perhaps is an invitation to
 * trouble...  So beware.q */

/* We keep two sets, one for IDENTIFIERS and one for STRINGS.  (see lexer.lex) */

set_strings _str_set_identifiers;
set_strings _str_set_strings;


const char *find_str_identifiers(const char *str)
{
  set_strings::iterator i = _str_set_identifiers.lower_bound(str);

  if (i == _str_set_identifiers.end() ||
      strcmp(*i,str) != 0)
    {
      // We need to be inserted.

      size_t len = strlen (str);

      char *s = new char[len+1];
      
      memcpy(s,str,len);
      s[len] = '\0';

      _str_set_identifiers.insert(i,s);

      return s;
    }
  else
    {
      // We were already there

      return *i;
    }
}

const char *find_str_strings(const char *str,int len)
{
  if (len == -1)
    len = strlen(str);

  char *s = new char[len+1];

  memcpy(s,str,len);
  s[len] = '\0';

  std::pair<set_strings::iterator,bool> result = _str_set_strings.insert(s);

  if (result.second == false)
    {
      // We were already there

      delete[] s;
    }
  else
    {
      // We were newly inserted.

      
    }

  return *result.first;
}






