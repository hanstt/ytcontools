#include "cable_node.hh"

#include <string.h>
#include <stdlib.h>

cabling_node_list *all_cable_nodes;

cabling_node_list* append_node(cabling_node_list *v,
			       cabling_node *a)
{
  if (!v)
    v = new cabling_node_list;

  if (a)
    v->push_back(a);
  return v;
}

void append_nodes(cabling_node_list **v,
		  cabling_node_list *a)
{
  if (!*v)
    {
      *v =  a;
      return;
    }

  (*v)->insert((*v)->end(),
	       a->begin(),a->end());

  delete a;
}

// cabling_node *node_list_pack(cabling_node_list *v);

////////////////////////////////////////////////////////////////////////
//
// Create a map with key rcsu and pointer to the module 
//

modules_map all_modules;

void map_cable_nodes()
{
  if (!all_cable_nodes)
    return;

  cabling_node_list::iterator i;

  for (i = all_cable_nodes->begin(); i != all_cable_nodes->end(); ++i)
    {
      cabling_node* node = *i;

      hw_module *module = dynamic_cast<hw_module*>(node);

      if (module)
	{
	  modules_map::const_iterator other;

	  if ((other = all_modules.find(module->_location)) != 
	      all_modules.end())
	    {
	      module->_loc.print_lineno(stderr);
	      fprintf (stderr,"Several modules with rcsu: %s\n",
		       module->_location.to_string().c_str());
	      other->second->_loc.print_lineno(stderr);
	      fprintf (stderr,"Also defined here.\n");
	      
	    }

	  all_modules.insert(modules_map::value_type(module->_location,
						     module));
	}
    }
}

hw_module *find_module(rcsu &loc)
{
  modules_map::iterator i = all_modules.find(loc);

  if (i == all_modules.end())
    return NULL;

  return i->second;
}

////////////////////////////////////////////////////////////////////////
//
// For all connections, find the other module, and
// it's corresponding connection.
//

typedef std::set<rcsu> set_rcsu;

set_rcsu _ignore_mismatches;

bool ignore_link_error(rcsu &loc)
{
  set_rcsu::iterator lower;

  lower = _ignore_mismatches.upper_bound(loc);

  if (lower == _ignore_mismatches.begin())
    return false;

  lower--;
  /*
  fprintf (stderr,"Ignoring?: %s  (lower: %s)\n",
	   loc.to_string().c_str(),
	   lower->to_string().c_str());
  */
#define IGNORE_LEVEL(level)		\
  if (lower->level == -1)		\
    return true;			\
  if (lower->level != loc.level)	\
    return false;

  IGNORE_LEVEL(_rack);
  IGNORE_LEVEL(_crate);
  IGNORE_LEVEL(_slot);
  IGNORE_LEVEL(_unit);

  return false;
}

#define IGNORE_MISMATCH(rcsu)	 \
  if (ignore_link_error(rcsu)) { \
    ignored_mismatch_errors++;	 \
    continue;                    \
}

bool link_cable_connections()
{
  if (!all_cable_nodes)
    return true;

  int ignored_mismatch_errors = 0;

  modules_map::iterator i;

  int is_ok = true;
  for (i = all_modules.begin(); i != all_modules.end(); ++i)
    {
      hw_module *module = i->second;

      connections_map::iterator j;
      
      for (j = module->_connections.begin(); j != module->_connections.end(); j++)
	{
	  hw_conn *conn = j->second;
	  
	  // We have a connection.  
	  
	  // rcsu          _other;
	  // std::string   _other_type;
	  // std::string   _other_connector;
	  // std::string   _label;
	  // cable_marker *_marker;

	  hw_connection *connection = dynamic_cast<hw_connection*>(conn);

	  if (connection)
	    {	  
	      modules_map::iterator i_other = all_modules.find(connection->_other);
	      
	      if (i_other == all_modules.end())
		{
		  IGNORE_MISMATCH(module->_location);
		  IGNORE_MISMATCH(connection->_other);

		  connection->_loc.print_lineno(stderr);

		  fprintf (stderr,
			   "'%s/%s' cannot find other module with rcsu: %s\n",
			   i->first.to_string().c_str(),
			   connection->_connector/*->c_str()*/,
			   connection->_other.to_string().c_str());
		  is_ok = false;
		  continue;
		}

	      // Aha, module found.  Can we find a connector on the
	      // other module with this name?

	      hw_module *other = i_other->second;

	      connections_map::iterator i_back = 
		other->_connections.find(connection->_other_connector);

	      if (i_back == other->_connections.end())
		{
		  IGNORE_MISMATCH(module->_location);
		  IGNORE_MISMATCH(connection->_other);

		  connection->_loc.print_lineno(stderr);

		  fprintf (stderr,
			   "'%s/%s' cannot find return connector: %s/%s\n",
			   i->first.to_string().c_str(),
			   connection->_connector/*->c_str()*/,
			   connection->_other.to_string().c_str(),
			   connection->_other_connector/*->c_str()*/);

		  other->_loc.print_lineno(stderr);
		  fprintf (stderr,"other module was defined here.\n");
		  is_ok = false;
		  continue;
		}

	      // Ok, so we have return connection.  Does it lead to us?

	      hw_connection *back = dynamic_cast<hw_connection*>(i_back->second);

	      if (!back)
		{
		  IGNORE_MISMATCH(module->_location);
		  IGNORE_MISMATCH(connection->_other);

		  connection->_loc.print_lineno(stderr);
		  fprintf (stderr,
			   "'%s/%s' return connector no good: %s/%s\n",
			   i->first.to_string().c_str(),
			   connection->_connector/*->c_str()*/,
			   connection->_other.to_string().c_str(),
			   connection->_other_connector/*->c_str()*/);

		  back->_loc.print_lineno(stderr);
		  fprintf (stderr,"return connector defined here.\n");
		  is_ok = false;
		  continue;
		}

	      if (back->_other != i->first)
		{
		  IGNORE_MISMATCH(module->_location);
		  IGNORE_MISMATCH(connection->_other);
		  IGNORE_MISMATCH(back->_other);

		  connection->_loc.print_lineno(stderr);
		  fprintf (stderr,
			   "'%s/%s' return connector not leading back to us: "
			   "%s/%s (module)\n",
			   i->first.to_string().c_str(),
			   connection->_connector/*->c_str()*/,
			   connection->_other.to_string().c_str(),
			   connection->_other_connector/*->c_str()*/);

		  back->_loc.print_lineno(stderr);
		  fprintf (stderr,
			   "return connector defined here, goes to: %s/%s.\n",
			   back->_other.to_string().c_str(),
			   back->_other_connector/*->c_str()*/);
		  is_ok = false;
		  continue;
		}

	      if (back->_other_connector != connection->_connector)
		{
		  IGNORE_MISMATCH(module->_location);
		  IGNORE_MISMATCH(connection->_other);
		  IGNORE_MISMATCH(back->_other);

		  connection->_loc.print_lineno(stderr);
		  fprintf (stderr,
			   "'%s/%s' return connector not leading back to us: "
			   "%s/%s (connector)\n",
			   i->first.to_string().c_str(),
			   connection->_connector/*->c_str()*/,
			   connection->_other.to_string().c_str(),
			   connection->_other_connector/*->c_str()*/);

		  back->_loc.print_lineno(stderr);
		  fprintf (stderr,
			   "return connector defined here, goes to: %s/%s.\n",
			   back->_other.to_string().c_str(),
			   back->_other_connector/*->c_str()*/);
		  is_ok = false;
		  continue;
		}

	      // All right!  Connection is good.  Let's use it.

	      // Mark up the return connector for us.

	      connection->_other_attr = back->_attr;
	    }
	}
    }

  if (ignored_mismatch_errors)
    {
      fprintf (stderr,"Warning: %d cable mismatch(es) ignored.\n",
	       ignored_mismatch_errors);
    }

  return is_ok;
}

void ignore_mismatches(const char *ignores)
{
  if (!*ignores)
    return;

  for ( ; ; )
    {
      const char *comma = strchr(ignores,',');

      char *ignore;

      if (comma)
	ignore = strndup(ignores,comma-ignores);
      else
	ignore = strdup(ignores);

      const char *underscore = strchr(ignore,'_');

      if (underscore)
	{
	  rcsu_range ign_range = create_rcsu_range(ignore);

	  int num = ign_range.num_range();

	  rcsu ign = ign_range._first;

	  for (int i = 0; i < num; i++)
	    {
	      _ignore_mismatches.insert(ign);

	      printf ("Ignoring mismatches: %s\n",ign.to_string().c_str());

	      ign = ign_range.next_other(ign);
	    }
	}
      else
	{
	  rcsu ign = create_rcsu(ignore);

	  _ignore_mismatches.insert(ign);

	  printf ("Ignoring mismatches: %s\n",ign.to_string().c_str());
	}

      free (ignore);

      if (!comma)
	break;
      ignores = comma + 1;
    }
}
