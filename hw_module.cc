
#include "hw_module.hh"
#include "str_set.hh"
#include "util.hh"

#include <stack>

#include <stdlib.h>

#include <regex.h>

hw_module_part_list *append_node(hw_module_part_list *v,
				 hw_module_part *a)
{
  if (!v)
    v = new hw_module_part_list;

  if (a)
    v->push_back(a);
  return v;
}

hw_has_control_multimap _module_has_control;

void hw_module::apply(hw_module_part_list* parts)
{
  hw_module_part_list::iterator i;

  for (i = parts->begin(); i != parts->end(); ++i)
    {
      hw_module_part *a = *i;

      // First expand connectors if necessary.
      { 
	hw_connection *connector = dynamic_cast<hw_connection*>(a);                                
	/*
	if (connector)
	  printf("Investigating connector %08x.\n",(int) connector);
	fflush(stdout);
	*/
	if (connector)
	  {
	    if (connector->expand(parts,i))
	      {
		// The expanded entries were added to the list,
		// delete this one, as it will not be added.
		delete connector;
		continue;
	      }
	    
	    if (connector->_other.is_partial())
	      {
		// The rcsu of other was partial, i.e.  .s11,
		// fill in our values

		if (!connector->_other.make_full(_location))
		  {
		    fprintf (stderr,"Partial rcsu could not be made full.\n");
		  }
	      }
	  }
      }

      APPLY_MULTIPLE_INSTANCE_CHECK(hw_conn,connection,_connector,_connections,
                                    connections_map::value_type);
      APPLY_MULTIPLE_INSTANCE_CHECK(hw_setting,setting,_name,_settings,
                                    settings_map::value_type);

      APPLY_SINGLE_INSTANCE(hw_label,  label,  _label);
      APPLY_SINGLE_INSTANCE(hw_serial, serial, _serial);

       { 
	hw_control *local = dynamic_cast<hw_control*>(a);
	if (local)                                                             
	  {                                                                    
	    _controls.insert(local);   
	    /*
	    printf ("Inserting: %s %d\n",
		    local->_key,
		    _module_has_control.size());
	    */
	    _module_has_control.insert(hw_has_control_multimap::value_type(local->_key,this));
	    continue;                                                          
	  } 
      }
   }
}

void hw_module::dump()
{
  printf ("// ");
  _loc.print_lineno(stdout,false);
  printf ("\n\n");
  printf ("%s(%s)\n",
	  _model->_name/*->c_str()*/,
	  _location.to_string().c_str());
  printf ("{\n");
  
  if (_label)
    _label->dump();
  if (_serial)
    _serial->dump();

  settings_map::iterator setting;

  for (setting = _settings.begin(); setting != _settings.end(); ++setting)
    (setting->second)->dump();

  connections_map::iterator connection;

  for (connection = _connections.begin(); connection != _connections.end(); connection++)
    (connection->second)->dump();

  printf ("}\n");
}

void hw_module::short_dump_lengths(int *max_rcsu,int *max_model,int *max_label)
{
  int indent;

  if (_location._crate == -1) indent = 0;
  else if (_location._slot == -1) indent = 1;
  else if (_location._unit == -1) indent = 2;
  else indent = 3;

  int len_rcsu = indent + strlen(_location.to_string().c_str());
  if (len_rcsu >= *max_rcsu)
    *max_rcsu = len_rcsu;
  int len_model = strlen(_model->_name);
  if (len_model >= *max_model)
    *max_model = len_model;
  int len_label = _label ? strlen(_label->_label) : 0;
  if (len_label >= *max_label)
    *max_label = len_label;
}

void hw_module::short_dump(int max_rcsu,int max_model,int max_label)
{
  int indent;

  if (_location._crate == -1) indent = 0;
  else if (_location._slot == -1) indent = 1;
  else if (_location._unit == -1) indent = 2;
  else indent = 3;

  printf ("%*s%-*s  %-*s  %-*s",
	  indent,"",max_rcsu-indent,
	  _location.to_string().c_str(),
	  max_model,_model->_name,
	  max_label,_label ? _label->_label : "");

  printf ("  // ");
  _loc.print_lineno(stdout,false);

  printf ("\n");
}

////////////////////////////////////////////////////////////////////////

void dump_modules()
{
  const char *marker = "MODULE_DUMP";

  print_header(marker,"Dump of all modules (instances)");

  modules_map::iterator i;

  for (i = all_modules.begin(); i != all_modules.end(); ++i)
    {
      printf ("\n");
      i->second->dump();
    }

  printf ("\n");
  print_footer(marker);
}

typedef std::map<rcsu,hw_module*,
		 compare_rcsu_rd_name> hw_module_rd_name_map;

void dump_modules_short()
{
  const char *marker = "SHORT_MODULE_DUMP";

  print_header(marker,"Short dump of all modules (instances)");

  hw_module_rd_name_map rd_name_mod;  

  modules_map::iterator i;

  int max_rcsu = 0;
  int max_model = 0;
  int max_label = 0;
  
  for (i = all_modules.begin(); i != all_modules.end(); ++i)
    {
      hw_module* module = i->second;
      
      rd_name_mod.insert(hw_module_rd_name_map::value_type(i->first,
							   i->second));

      module->short_dump_lengths(&max_rcsu,&max_model,&max_label);
    }

  rcsu prev = { -1, -1, -1, -1 };

  hw_module_rd_name_map::iterator j;

  for (j = rd_name_mod.begin(); j != rd_name_mod.end(); ++j)
    {
      hw_module* module = j->second;

      if (prev._rack != module->_location._rack)
	printf ("\n");
    
      prev = module->_location;

      module->short_dump(max_rcsu,max_model,max_label);
    }

  printf ("\n");
  print_footer(marker);
}

////////////////////////////////////////////////////////////////////////

#define countof(x) ((sizeof(x))/(sizeof(x[0])))

bool hw_module::check_valid_hw_setting(hw_setting* setting)
{
  // The model module should have such a setting.
  // And it should be a valid value...

  has_settings_map::iterator i;

  // We need to dig through all model_as specifications as well

  hardware_def *model;
  std::stack<hardware_def*> models;
  models.push(_model);

  for ( ; ; )
    {
      model = models.top();
      models.pop();

      i = model->_settings.find(setting->_name);

      if (i != model->_settings.end())
	break; // We found the setting!

      for (model_as_vector::iterator i = model->_model_as.begin();
	   i != model->_model_as.end(); ++i)
	models.push((*i)->_parent);
      
      if (models.empty())
	{
	  setting->_loc.print_lineno(stderr);
	  fprintf(stderr," could not find setting '%s'\n",
	      setting->_name/*->c_str()*/);
	  _model->_loc.print_lineno(stderr);
	  fprintf(stderr," model was defined here\n");
	  return false;
	}
    }

  // Setting exists.  Is it a valid value?

  has_setting *set_type = i->second;
  if (set_type->_values)
    {
      std::vector<const char *>::iterator j;
      
      for (j = set_type->_values->begin(); j != set_type->_values->end(); ++j)
	{
	  if (*j == setting->_value)
	    return true;
	}
    }

  // If there is only one option, it may be for a value?
  
  if (set_type->_values->size() == 1)
    {
      const char *val = (*set_type->_values)[0];

      if (strlen(val) >= 2 &&
	  val[0] == '%' && val[1] == 'f')
	{
	  const char *p = setting->_value/*->c_str()*/;
	  char* endptr;

	  /*double float_val =*/ strtod(p,&endptr);

	  for ( ; ; )
	    {
	      if (endptr > p &&
		  strcmp(endptr,val/*->c_str()*/+2) == 0)
		return true;

	      // Allow space at end of number

	      if (*endptr != ' ')
		break;
	      endptr++;
	    }	  
	}

      // or we treat it as a regular expression

      // This is the last chance.  Complain
      // if it doesn't compile

      regex_t reg;

      const char *regex = val/*->c_str()*/;

      int ret = regcomp(&reg,regex,REG_EXTENDED);

      if (ret != 0)
	{
	  int len = regerror(ret,&reg,NULL,0);
	  char *str = new char [len];
	  regerror(ret,&reg,str,len);
          fprintf(stderr, "Regex (%s) compilation failure!  (Error: %s)  ",
		  regex,str);
          delete [] str;
	}
      else
	{
	  // It compiled.  Does it match? (we require full match)
	  
	  const char *p = setting->_value/*->c_str()*/;
	  regmatch_t pe[1];
	  
	  int ret = regexec(&reg,p,countof(pe),pe,0);
	  
	  regfree(&reg);
	  /*	  
	  fprintf(stderr,
		  "Regex (%s) match (%s)?  (%d %d %d)\n",
		  regex,p,ret,pe[0].rm_so,pe[0].rm_eo);
	  */
	  if (ret == 0 &&
	      pe[0].rm_so == 0 &&
	      pe[0].rm_eo == (int) strlen(p))
	    {
	      return true;
	    }
	}
    }

  setting->_loc.print_lineno(stderr);
  fprintf(stderr," invalid setting value '%s' for setting '%s'\n",
	  setting->_value/*->c_str()*/,
	  setting->_name/*->c_str()*/);
  model->_loc.print_lineno(stderr);
  fprintf(stderr," model was defined here\n");
  return false;
}

bool hw_module::check_valid_hw_conn(hw_conn* connection)
{
  // We can here check that there actually is an connector with this name...

  connector_attr_map::iterator i;

  // We need to dig through all model_as specifications as well

  hardware_def *model;
  std::stack<hardware_def*> models;
  models.push(_model);

  for ( ; ; )
    {
      model = models.top();
      models.pop();

      i = model->_connectors.find(connection->_connector);

      if (i != model->_connectors.end())
	break; // We found the connection!

      for (model_as_vector::iterator i = model->_model_as.begin();
	   i != model->_model_as.end(); ++i)
	models.push((*i)->_parent);

      if (models.empty())
	{
	  connection->_loc.print_lineno(stderr);
	  fprintf(stderr," unknown connector '%s'\n",connection->_connector/*->c_str()*/);
	  _model->_loc.print_lineno(stderr);
	  fprintf(stderr," model was defined here\n");
	  return false;
	}
    }

  connection->_attr = i->second;

  // If the connector also has markers, then we could also check that it is of correct
  // input / output type

  return true;
}

bool hw_module::is_model_as(const char *name)
{
  hardware_def *model;

  std::stack<hardware_def*> models;
  models.push(_model);

  while (!models.empty())
    {
      model = models.top();
      models.pop();

      if (strcmp(model->_name,name) == 0)
	return true;

      for (model_as_vector::iterator i = model->_model_as.begin();
	   i != model->_model_as.end(); ++i)
	models.push((*i)->_parent);
    }

  return false;
}

bool hw_connection::expand(hw_module_part_list* parts,
			   hw_module_part_list::iterator &insert_after)
{
  // Both our connector and the connector at the other end must expand
  // to the same amount of single connections.  Otherwise expansion
  // cannot happen.

  std::vector<std::string>* items_conn = expand_string(_connector);
  std::vector<std::string>* items_other_conn = expand_string(_other_connector,true);

  int num_other_range = _other_range.num_range();

  if (!items_conn && !items_other_conn && num_other_range == 1)
    return false;

  int num_items       = items_conn       ? items_conn->size()       : 1;
  int num_other_items = items_other_conn ? items_other_conn->size() : 1;

  if (num_items != num_other_range * num_other_items)
    {
      _loc.print_lineno(stderr);
      fprintf(stderr,
	      "cannot expand since "
	      "connector '%s' and other connector '%s' "
	      "has different number of items %d vs %d*%d.\n",
	      _connector/*->c_str()*/,_other_connector/*->c_str()*/,
	      (int)(items_conn ? items_conn->size() : 0),
	      (int)(items_other_conn ? items_other_conn->size() : 0),
	      num_other_range);

      delete items_conn;
      delete items_other_conn;

      return false;
    }

  if (!items_other_conn)
    {
      items_other_conn = new std::vector<std::string>;
      items_other_conn->push_back(_other_connector);
    }

  std::vector<std::string>::iterator i_conn       = items_conn->begin();
  std::vector<std::string>::iterator i_other_conn = items_other_conn->begin();

  hw_module_part_list::iterator insert_before = insert_after;
  ++insert_before;

  rcsu other = _other_range._first;

  for ( ; i_conn != items_conn->end(); ++i_conn, ++i_other_conn)
    {
      if (i_other_conn == items_other_conn->end())
	{
	  other = _other_range.next_other(other);
	  i_other_conn = items_other_conn->begin();
	}

      hw_connection *item = new hw_connection();

      item->_loc             = _loc;
      item->_connector       = find_str_identifiers(i_conn->c_str());
      item->_other_range._first = other;
      item->_other_range._last  = other;
      item->_other_type      = _other_type;
      item->_other_connector = find_str_identifiers(i_other_conn->c_str());
      item->_label           = _label;
      item->_marker          = _marker;
      /*
      printf ("Expanding to: %s %s (%08x)\n",
	      item->_connector.c_str(),
	      item->_other_connector.c_str(),
	      (int) item);
      fflush (stdout);
      */
      // Insert the item after the current position

      parts->insert(insert_before,item);
    }

  delete items_conn;
  delete items_other_conn;

  return true;
}

hw_connection* hw_module::get_connection(const char* name)
{
  connections_map::iterator i = 
    _connections.find(find_str_identifiers(name));

  if (i == _connections.end())
    return NULL;

  return dynamic_cast<hw_connection*>(i->second);
}

const char* hw_module::get_setting(const char *name)
{
  settings_map::iterator i = 
    _settings.find(find_str_strings(name));
  
  if (i == _settings.end())
    return NULL;
  
  return i->second->_value;
}

std::vector<const char*> *hw_module::get_controls(const char *key)
{
  std::vector<const char*> *vect = NULL;

  hw_control lookfor(find_str_strings(key),NULL);
  
  hw_control_multiset::iterator i = _controls.lower_bound(&lookfor);
  hw_control_multiset::iterator j = _controls.upper_bound(&lookfor);

  if (i == j)
    return vect; // none found here

  if (!vect)
    vect = new std::vector<const char*>;

  for ( ; i != j; ++i)
    vect->push_back((*i)->_value);

  return vect;
}

const char *hw_module::get_control(const char *key)
{
  std::vector<const char*> *vect = get_controls(key);

  if (!vect)
    return NULL;

  if (vect->size() > 1)
    {
      fprintf (stderr,"Getting CONTROL(%s) for %s.  Only one expected, but found several.\n",
	       key,_location.to_string().c_str());
    }

  const char *ctrl = (*vect)[0];

  delete vect;

  return ctrl;  
}

bool hw_module::has_control(const char *key)
{
  hw_control lookfor(find_str_strings(key),NULL);

  return _controls.find(&lookfor) != _controls.end();
}

hw_module *hw_module::find_parent_module()
{
  // Find the enclosing module.  If we are a unit, find a slot, if a slot,
  // find a crate, if a crate, find a rack

  rcsu rc_parent = _location;

  if (rc_parent._unit != -1)
    rc_parent._unit = -1;
  else if (rc_parent._slot != -1)
    rc_parent._slot = -1;
  else if (rc_parent._crate != -1)
    rc_parent._crate = -1;
  else
    return NULL;
  
  modules_map::iterator i = all_modules.find(rc_parent);

  if (i == all_modules.end())
    return NULL;

  return i->second;
}
